<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://wordpress.org/support/article/editing-wp-config-php/

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', 'bitnami_wordpress' );


/** MySQL database username */

define( 'DB_USER', 'bn_wordpress' );


/** MySQL database password */

define( 'DB_PASSWORD', '8afa47ef082a0b48144a92c6cbf73eb32feba06fe9d2fff259d2683055bdd00a' );


/** MySQL hostname */

define( 'DB_HOST', 'localhost:3306' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         'ru9NwVv^/-10g7wb/4I`]!J[Eo_C4mnK(M/b?LFx{CJXJ~D)OZR#y^3vo=f0OsuC' );

define( 'SECURE_AUTH_KEY',  '>q%D]:0_H^q[%w]a9I7_94Q7y9ZuPpLw5|@W{tdKrmZ7E[vb`r?opSig<^Tr0p*i' );

define( 'LOGGED_IN_KEY',    'kHr^>v(]%VRip#oJ;)VZ+8@Ua,L!EGA@</^]&1m/7`zNBHYX852lj`REN.3MuH6_' );

define( 'NONCE_KEY',        'GmBSas[~@VbOp:22i<2A<hHFH_Q@nqq%&dXsqfR5 KtJ4C2adcI3*QI0V!j0`.]1' );

define( 'AUTH_SALT',        'C}g&HMdz_7,<ir6 i18}.p:yKLMrt{oO`bqdLyvxGT hCICcf@o18zIg~U}asjoP' );

define( 'SECURE_AUTH_SALT', 'cgD)y#*qjmGINZ,;%mN}*vCj4ll:4Wbf&_zA?{>Kf2]PYHM-osOQvadt1h7bKbsr' );

define( 'LOGGED_IN_SALT',   ',1ZnzgV]|RSKvy_E)Wlr|f18tt:k`wIc(2$Su{wt|;4GE,xH#jq`*uxR1Bk/8 Mu' );

define( 'NONCE_SALT',       'T_R6+93ow6o9>Z6<ZJp( vbY#L;QGe}j{W9dXP()PRN`w%2om<R@(;Qvf|z8#_JK' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';

define('ALLOW_UNFILTERED_UPLOADS', true);
/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the documentation.

 *

 * @link https://wordpress.org/support/article/debugging-in-wordpress/

 */

define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', true );
define( 'FS_METHOD', 'direct' );
define( 'WP_AUTO_UPDATE_CORE', 'minor' );
define('FORCE_SSL_ADMIN', true);
define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', false );
$base = '/';
define( 'DOMAIN_CURRENT_SITE', 'dev.chotu.shop' );
define( 'PATH_CURRENT_SITE', '/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );
// define('AMBASSDOR_API_URL', 'http://65.1.88.93:5000/orderdump/add');
define('AMBASSDOR_API_URL', 'http://13.232.244.116:5000/orderdump/add');
define('AMBASSDOR_API_USERNAME_PASSWORD', 'mern_api123:kangaroo');
define('WP_MEMORY_LIMIT', '256M');
set_time_limit(300);
define('JWT_AUTH_CORS_ENABLE', true);
define('JWT_AUTH_SECRET_KEY', 'o|SVir#Q&GQxlF[&c{Aq1i_fn.Vi~j]kRoFY!?@;$.-Y+e}-,x=b9J+[[<5VW2dn');
/* That's all, stop editing! Happy publishing. */
define('AUTOPTIMIZE_CRITICALCSS_API_KEY', 'eyJhbGmorestringsherexHa7MkOQFtDFkZgLmBLe-LpcHx4');
define('WP_CACHE', true);
/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', __DIR__ . '/' );

}
set_time_limit(0);
ini_set('memory_limit', '20000M');

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
//require_once WPMU_PLUGIN_DIR . '/load.php';
/**
 * Disable pingback.ping xmlrpc method to prevent WordPress from participating in DDoS attacks
 * More info at: https://docs.bitnami.com/general/apps/wordpress/troubleshooting/xmlrpc-and-pingback/
 */
if ( !defined( 'WP_CLI' ) ) {
	// remove x-pingback HTTP header
	add_filter("wp_headers", function($headers) {
		unset($headers["X-Pingback"]);
		return $headers;
	});
	// disable pingbacks
	add_filter( "xmlrpc_methods", function( $methods ) {
		unset( $methods["pingback.ping"] );
		return $methods;
	});
}
