<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head();?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800" rel="stylesheet">
  <?php
    $localize = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'redirecturl' => '',
            'UserId' => 0,
            'loadingmessage' => __('please wait...', ''),
        );
      ?>
      <style>
        .home_bg_img{
          background-image: url('<?php echo wp_get_attachment_image_url(get_option('options_home_page_banner_pic'),'banner-image');?>');
          aspect-ratio: 1618/1000;
          object-fit: cover;
          
        }
      </style>
</head>
<body>
  <section class="mobileView m-auto">
  <?php
  	do_action('chotu_before_header');
  ?>

<header class="sticky-top header_panel">
    <div class="container">

    <nav class="navbar navbar-dark row">
          <!-- toggler button -->

         <div class="col-3 t_left h-100"> 
        
          <button class="navbar-toggler p-0 h-100 align-middle" type="button" data-toggle="collapse" data-target="#Toggler"
            aria-controls="Toggler" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon ">
              <div class="nav_line"></div>
              <div class="nav_line"></div>
              <div class="nav_line"></div>
            </span>
          </button>
       
         </div>

        <div class="col-6 h-100">
          <?php 
          the_custom_logo();
          ?>
          </div>
          <div class="col-3 t_right h-100">
            <div class="h-100 align-middle">
                <?php
                if (!is_user_logged_in()) {
                ?>

                <span href="?login=true" onclick="jQuery('this').digits_login_modal(jQuery(this));return false;" attr-disclick="1" class="digits-login-modal" type="1"><img class="filter_beige h-100 align-middle" src="<?php echo get_template_directory_uri();?>/images/account_circle_black.svg" style="width: 30px; height: 30px;border-radius: 50%;"></span>
                <?php 
                  }else{
                    //echo '<a href="javascript:;">Login</a>';
                    $balance = get_user_meta(get_current_user_id(),'mycred_default',true);
                    if($balance == '' || $balance == 0){
                      $balance = 0;
                    }
                    echo '<div class="balance_header h-100"><a class="text-decoration-none" href="'.home_url().'/my-balance">₹'.$balance.'</a></div>';
                  }
                ?>
            </div>
       </div>
     </nav>
    </div>
    <div class="collapse bg-white navbar-collapse" id="Toggler">
        <?php
          wp_nav_menu(
            array(
              'theme_location' => 'menu-1',
              'menu_id'        => 'primary-menu',
              'menu_class' => 'navbar-nav mx-auto',    
            )
          );
          ?>

      </div>

</header>
      
    

   

