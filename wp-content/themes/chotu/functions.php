<?php
/**
 * chotu functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package chotu
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'chotu_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function chotu_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on chotu, use a find and replace
		 * to change 'chotu' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'chotu', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		add_post_type_support( 'page', 'excerpt' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'chotu' ),
				'footer_menu' => 'Footer Menus',
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'chotu_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 145,
				'width'       => 333,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);
		add_theme_support(
			'footer-logo',
			array(
				'height'      => 100,
				'width'       => 100,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'chotu_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function chotu_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'chotu_content_width', 640 );
}
add_action( 'after_setup_theme', 'chotu_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function chotu_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'chotu' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'chotu' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);/*
	 register_sidebar( array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>', 
        'id'            => 'home-page-1',      
        'name'=>__( 'Home Page 1', 'textdomain' ),  
    )
    );  
    register_sidebar( array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
        'id'            => 'home-page-2',        
        'name'=>__( 'Home Page 2', 'textdomain' ),  
    )
    );
    register_sidebar( array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
        'id'            => 'home-page-3',   
        'name'=>__( 'Home Page 3', 'textdomain' ),  
    )
    );
    register_sidebar( array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
        'id'            => 'home-page-4',       
        'name'=>__( 'Home Page 4', 'textdomain' ),  
    )
    );
    */
    register_sidebar( array(
    	'name'=>esc_html__( 'Footer copy right', 'chotu' ), 
    	'id'            => 'footer',         
    )
    );
}
add_action( 'widgets_init', 'chotu_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function chotu_scripts() {
	
	wp_enqueue_style( 'bootstrap.min.css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.1', 'all');
	wp_enqueue_style( 'chotu-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'chotu-style', 'rtl', 'replace' );
	//wp_enqueue_style( 'font-awesomes', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css', array(), '1.1', 'all');
	wp_enqueue_script( 'chotu-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'iconify', get_template_directory_uri() . '/js/iconify.min.js', array ( 'jquery' ), 1.1, true);
	//wp_enqueue_script( 'jquery-3.3.1.slim', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'bootstrap.bundle.min', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'bootstrap.min.js', get_template_directory_uri() . '/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'footer.js', get_template_directory_uri() . '/js/footer.js', array ( 'jquery' ), 1.1, true);



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'chotu_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function chotu_add_first_and_last($items) {
  $items[1]->classes[] = 'd-flex  border-bottom';
  return $items;
}

add_filter('wp_nav_menu_objects', 'chotu_add_first_and_last');
function chotu_add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-link text-dark"', $ulclass);
}
add_filter('wp_nav_menu','chotu_add_menuclass');
function get_breadcrumb() {
	if(!is_front_page()){
		echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo "&nbsp;";
                the_title();
            }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
	}
    
}
add_action( 'rest_api_init', 'create_api_posts_meta_field' );

function create_api_posts_meta_field() {

 // register_rest_field ( 'name-of-post-type', 'name-of-field-to-return', array-of-callbacks-and-schema() )
 register_rest_field( 'locality', 'metaval', array(
 'get_callback' => 'get_post_meta_for_api',
 'schema' => null,
 )
 );
}

function get_post_meta_for_api( $object ) {
 //get the id of the post object array
 $post_id = $object['id'];
 $locality_status = get_post_meta($post_id, 'locality_status', true);
$image = wp_get_attachment_image_url(get_post_meta($post_id,'locality_banner_pic',true),'banner_pic');
if(empty($image) && $image == ''){
  $image = wp_get_attachment_image_url(get_option('options_default_locality_pic'),'banner-image');

    //$banner_image = plugins_url().'/chotu_work/assets/images/default_locality.webp';
  }
  if($locality_status =='coming_soon' && wp_get_attachment_image_url(get_post_meta($post_id,'locality_banner_pic',true),'banner-image') == ''){
    //$banner_image = plugins_url().'/chotu_work/assets/images/coming_soon_default_locality.webp';
    $image = wp_get_attachment_image_url(get_option('options_coming_soon_default_locality_pic'),'banner-image');
  }
  $data = get_post_meta( $post_id );
  $data['post_attachement'] = $image;
 //return the post meta
 return $data;
}

/* restrict access for non admin user and unauthenticated user */
/*
add_filter( 'rest_authentication_errors', function($result) {
  if ( ! empty( $result ) ) {
    return $result;
  }
  $request_url = strtok($_SERVER["REQUEST_URI"], '?');
  $allowedAddress =array('/wp-json/jwt-auth/v1/token','/wp-json/v1/get_gated_locality');
  $current_route = isset($request_url) ? $request_url : '/';
  if( in_array( $current_route, $allowedAddress ) ){
  	return $result;
  }else{
  	 if( !is_user_logged_in() ) {
	    return new WP_Error( 'rest_cannot_access', __( 'Only authenticated users can access this endpoint.', 'disable-json-api' ), array( 'status' => rest_authorization_required_code() ) );
	  }elseif (!is_admin()) {
	  	return new WP_Error( 'rest_cannot_access', __( 'Only authenticated users can access this endpoint.', 'disable-json-api' ), array( 'status' => rest_authorization_required_code() ) );
	  }
	  //User is logged in, approve all
	  else {
	    return $result;
	  }
  }
  
 
 
});
*/