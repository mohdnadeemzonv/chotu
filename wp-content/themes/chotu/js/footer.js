var x = document.getElementById("demo");
	var latitude;
	var longitude;
	var allowGeoRecall = true;
  var countLocationAttempts = 0;
	function getLocation() {
		console.log(navigator.geolocation);
	  if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(showPosition,showError);
	  } else { 
	    x.innerHTML = "Geolocation is not supported by this browser.";
	  }
	}

	function showPosition(position) {
	console.log(position);
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	get_details_by_lat_long(latitude,longitude);
	allowGeoRecall = false;
	}

	function get_details_by_lat_long(latitude,longitude){
		jQuery.ajax({
             type : "POST",
             url : window.ASL.ajaxurl,
             data : {'latitude':latitude,'longitude':longitude,action: "get_object_by_actual_location"},
             success: function(response) {
             	location.reload();
             	console.log(response);
            }
        });
	}
	function showError(error) {
	if(allowGeoRecall) getLocation();
  switch(error.code) {
    case error.PERMISSION_DENIED:
      x.innerHTML = '<br>we need your location to show localities near you to shop from. Kindly <a class="allow" href="javascript:void(0)" onclick="getLocation()">allow</a> location access..'
      break;
    case error.POSITION_UNAVAILABLE:
      x.innerHTML = "Location information is unavailable."
      break;
    case error.TIMEOUT:
      x.innerHTML = "The request to get user location timed out."
      break;
    case error.UNKNOWN_ERROR:
      x.innerHTML = "An unknown error occurred."
      break;
  }
}