<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chotu
 */
do_action('chotu_before_footer');
?>

<footer class="py-4" style="background-color: #f9f9f9">
      <div class="container sticky-bottom">
        <nav class="d-flex justify-content-center flex-column align-items-center pt-2">
          <!-- <img src="<?php echo plugins_url()?>/chotu_work/assets/images/chotu.svg" alt="Logo" /> -->
           <div class="mt-3">
           	<p class=" text-color-dark justify-content-center wrap_footer_link">
	          <?php
	          $menu_list='';
	          $locations = get_nav_menu_locations();
	          $menu_items = wp_get_nav_menu_items($locations['footer_menu']);
	          if(!empty($menu_items)){
	          	$lastElement = end($menu_items);
	          	foreach ($menu_items as $key => $menu_item) {
				$title = $menu_item->title;
				$url = $menu_item->url;
				if($menu_item != $lastElement) {
					$menu_list .= "\t\t\t\t\t". '<a class="text-reset caption" href="'. $url .'">&nbsp;'. $title .'&nbsp;</a>|' ."\n";
				}else{
					$menu_list .= "\t\t\t\t\t". '<a class="text-reset caption" href="'. $url .'">&nbsp;'. $title .'&nbsp;</a>' ."\n";
				}
				 
				}
				echo $menu_list;
	          }
	          
	          ?>
      	</p>
          </div>
        </nav>
      </div>
   </footer>
    
    <!-- BOTTOM COPYRIGHT FOOTER -->
    <nav class="navbar navbar-expand-lg" style="background-color: #0e4c75">
      <div class="container-fluid justify-content-center">
        <?php if ( is_active_sidebar( 'footer' ) ) {
         dynamic_sidebar( 'footer' );
       }?>
      </div>
    </nav>
    
  </section>
  <?php wp_footer()?>
 
  <!-- <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script> -->

<script type="text/javascript">
  
  </script>
</body>

</html>
