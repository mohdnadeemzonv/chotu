<?php
/*
 * Template Name: Main Site Copy Page 
 * description: >-
  Page template without sidebar
 */
?>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
  
    <?php wp_head();?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800" rel="stylesheet">
  <?php
    $localize = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'redirecturl' => '',
            'UserId' => 0,
            'loadingmessage' => __('please wait...', ''),
        );
      ?>
</head>
<body>
  <section class="mobileView m-auto">
  <?php
    do_action('chotu_before_header');
  ?>

<header class="sticky-top header_panel">
    <div class="container">

    <nav class="navbar navbar-dark row">
          <!-- toggler button -->

         <div class="col-3 t_left h-100"> 
          <button class="navbar-toggler p-0 h-100 align-middle" type="button" data-toggle="collapse" data-target="#Toggler"
            aria-controls="Toggler" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon "><img class="filter_beige" src="<?php echo get_template_directory_uri();?>/images/menu_black.svg" alt="" /></span>
          </button>
         </div>

     <div class="col-6 h-100">
          <?php 
          the_custom_logo();
          ?>
          </div>
          <div class="col-3 t_right h-100">
            <div class="h-100 align-middle">
          <?php
          if (!is_user_logged_in()) {
          ?>
          <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="h-100 login font-family-inter font-weight-normal"><img class="filter_beige h-100 align-middle" src="<?php echo get_template_directory_uri();?>/images/account_circle_black.svg" style="width: 30px; height: 30px;border-radius: 50%;"></a>
          <?php 
            }else{
              //echo '<a href="javascript:;">Login</a>';
              $balance = get_user_meta(get_current_user_id(),'mycred_default',true);
              if($balance == '' || $balance == 0){
                $balance = 0;
              }
              echo '<div class="balance_header h-100"><a class="text-decoration-none" href="'.home_url().'/my-balance">₹'.$balance.'</a></div>';
            }
          ?>
</div>
    </div>
     
    </div>
    <div class="collapse bg-white navbar-collapse" id="Toggler">
        <?php
          wp_nav_menu(
            array(
              'theme_location' => 'menu-1',
              'menu_id'        => 'primary-menu',
              'menu_class' => 'navbar-nav mx-auto',    
            )
          );
          ?>
      </div>
</header>


<section class="slider_main mb-4"> 
   <div id="GFG" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
               <li data-target="#GFG" data-slide-to="0" class="active"></li>
               <li data-target="#GFG" data-slide-to="1"></li>
                <li data-target="#GFG" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img src="https://dev.chotu.shop/wp-content/uploads/2021/10/FoodBackground-1024x768-1.jpg" alt="GFG" />
               </div>
                
               <div class="carousel-item">
                  <img src="https://dev.chotu.shop/wp-content/uploads/2021/07/my-home-jewel.jpg" alt="GFG" />
               </div>

                <div class="carousel-item">
                  <img src="https://dev.chotu.shop/wp-content/uploads/2021/09/lodha-bellezza.jpg" alt="GFG" />
               </div>
            </div>
         </div>

<div class="home_page_baner">
     <h4><i>pakka</i> <b>fresh</b></h4><br>
<h6>buy as neighbors, get the freshest</h6><br>

 <?php
    echo do_shortcode('[wpdreams_ajaxsearchlite]');
  ?>
</div>

</section>


     
      
    </nav>
    <!-- Heading Description -->
    <!-- Heading Description -->
  
    <?php
    
    do_action('chotu_after_main_content');


    
echo '<link rel="manifest" href="'.get_template_directory_uri().'/manifest.json">';
$page_id = get_the_ID();
global $wpdb;
$add_captain_image = wp_get_attachment_url(get_option('options_add_captain_image'));
$add_captain_heading = get_option('options_add_captain_heading');
$add_captain_description = get_option('options_add_captain_description');

 if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
    $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
        
        $lat = $location['lat'];
        $lon = $location['lon'];
    }else{
        $lat = $_COOKIE['lat'];
        $lon = $_COOKIE['lon'];
    }
    $bg_color = '#FAFFDC';
    // $lat_long = $lat.','.$lon;
    $lat_long = '17.45602,78.448551';
    echo '<div class="container"><h5>My Favourites</h5>';
    $favourites = get_user_favorites($user_id = null, $site_id = 0, $filters = null);
    if(is_user_logged_in()){
        if(!empty($favourites)){
        $box_start = '<div class="carousel overflow-hidden ">
            <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 " style="background: '.$bg_color.'; width:100%; "></div>';// '';
             $box_end = '</div></div>';
        foreach ($favourites as $key => $locality_id) {
        $s_locality = get_post($locality_id);
        $footer_text = '<p class=""><small class="caption">'. get_custom_post_meta($locality_id,'locality_description').'</small></p>';
        $button_text = 'View';
        $attachment_id = get_custom_post_meta($locality_id,'locality_banner_pic');
        $banner_image = wp_get_attachment_image_url($attachment_id,'banner-image');
        $favorite = get_favorites_button($locality_id, 0);
        $share_text = chotu_work_card_whatsapp_share('locality',get_permalink( $locality_id));
        echo $box_start;
        echo chotu_work_make_card($banner_image,$s_locality->post_title,$footer_text,$button_text,get_permalink( $locality_id),'',$share_text,$favorite);
        echo $box_end;
        }
    }else{
        echo '<p class="caption text-center">Add favorites by clicking on &#9733;.</p><hr>';
    }
    }else{
        echo '<p class="caption text-center">Please <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="h-100 login font-family-inter font-weight-normal">login</a> to add favorites.</p><hr>';
    }  
    
    echo '</div>';
    echo '<p class="caption text-center">
        for better results, please <a class="allow" href="javascript:void(0)" onclick="getLocation()">allow</a> location access
        <span id="demo"></span>
        <div class="container"><div id="locality_list"></div> <a href="#brands_list" class="pull-right caption text-decoration-none text-primary">What can I buy?</a>';

    echo do_shortcode('[show_localities title="Where can I buy from?" brand_id="" captain_id="" lat_long="'.$lat_long.'"]');


echo '<div id="brands_list"></div><a href="#locality_list"  class="pull-right caption text-decoration-none text-primary">Where can I buy from?</a>';
echo do_shortcode('[show_brands title="What can I buy?" locality_id="" captain_id="" lat_long="'.$lat_long.'"]');
$link = home_url().'/join';
$share_text = chotu_work_card_whatsapp_share('captain',$link);
$footer_text = '<p class=""><small class="caption">'.$add_captain_description.'</small></p>';
echo '<h5>Become a captain</h5>';
echo chotu_work_make_card($add_captain_image,$add_captain_heading,$footer_text,'JOIN',$link,'',$share_text,'');
// echo $html;
the_content();
do_action('chotu_work_home_after_content');
get_footer();?>




<!-- <script src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script> -->
<!-- <script type="text/javascript">
   jQuery(".carousel").swipe({
      swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
          if (direction == 'left') jQuery(this).carousel('next');
          if (direction == 'right') jQuery(this).carousel('prev');
      },
      allowPageScroll: "vertical" 
  });
</script> -->
<script type="text/javascript">
  // $(document).ready(function(){
  //    jQuery('.carousel').carousel('pause');
  //    jQuery(".carousel-inner > .carousel-item:first").addClass("active");
  //    jQuery(".carousel-inner-brands > .carousel-item:first").addClass("active");
  // });


</script>