<style>
.navbar-dark .navbar-toggler {
    background-color: white !important;
        padding: 0;
}
.container{    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;}

	.header_panel {
	    background: #fff;
	}
	.header_panel .container {
	    width: 96%!important;
	}
	.header_panel .navbar {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
}
.header_panel .row {
    --bs-gutter-x: 1.5rem;
    --bs-gutter-y: 0;
    display: flex;
    flex-wrap: wrap;
       margin-right: -15px;
    margin-left: -15px;
}
.h-100 {
    height: 100%!important;
}
.col-3 {
    flex: 0 0 auto;
    width: 25%;
    padding-left: 3%;
    padding-right: 3%;
}
.col-6 {
    flex: 0 0 auto;
    width: 50%;
}
.navbar-dark .navbar-toggler {
    border-color: transparent;
}
.navbar-dark .navbar-toggler {
    color: rgba(255,255,255,.55);
    border-color: rgba(255,255,255,.1);
}
.navbar-dark .navbar-toggler-icon {
    background-image: initial!important;
}
.navbar-toggler-icon {
    width: auto!important;
    height: auto!important;
}
.navbar-toggler-icon {
    display: inline-block;
    width: 1.5em;
    height: 1.5em;
    vertical-align: middle;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100%;
}
.nav_line {
    height: 2px;
    width: 18px;
    background-color: #fbc899;
    margin: 3px 0;
}
.custom-logo-link img {
    width: 100%!important;
    height: 100%!important;
    object-fit: contain;
}
.t_right {
    text-align: right;
}
.align-middle {
    vertical-align: middle!important;
}
.filter_beige {
    filter: invert(97%) sepia(99%) saturate(858%) hue-rotate(303deg) brightness(95%) contrast(108%);
}
div#Toggler {
    position: absolute;
    left: 0;
    right: 0;
    background-color: #fff;
    z-index: 999999;
}
.hide{
	display: none;
}
.show{
	display: block;
}

.t_right {
    text-align: right;
}

.digits-login-modal img {
    margin: 0 auto;
    margin-right: 0;
}


.header_panel .navbar {
    padding-top: 0;
    padding-bottom: 0;
}

.navbar-dark .navbar-toggler:focus{    outline-color: transparent !important;}

.logo_customise{}
.logo_customise img {
    margin: 0 auto;
}

#primary-menu li {
    border-bottom: 0.063rem solid #dee2e6!important;
    padding: 5px;
}

#primary-menu li a {
    padding: 0.5rem 0.938rem;
        font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
}

.navbar-nav {
    display: flex;
    flex-direction: column;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}

#primary-menu ul {
    margin: 0;
}
.balance_header a{
    position: absolute;
    top: 50%;
    right: 31px;
    transform: translate(0%, -50%);

}
.header_second{    text-align: center;
    padding: 8px;
    position: sticky;
    background-color: #fff;
    margin-bottom: 5px;
    z-index: 99; top: -3px;}
.header_second h3{
    font-size: 18px !important;
    /*font-weight: 500 !important;*/
    padding-top: 21px;
}
    .floating_icon img{      
    position: fixed;
    /* left: 0; */
    right: 0;
    bottom: 50px;
    padding: 40px;
    width: 130px;
     display: block !important;
}
  
}


</style>
<header class="sticky-top header_panel">
    <div class="container">

        <nav class="navbar navbar-dark row">
            <!-- toggler button -->
                
               
            <div class="col-3 t_left h-100"> 
                <button class="navbar-toggler p-0 h-100 align-middle" type="button" data-toggle="collapse" data-target="#Toggler"
                aria-controls="Toggler" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon ">
                    <div class="nav_line"></div>
                    <div class="nav_line"></div>
                    <div class="nav_line"></div>
                </span>
            </button>

        </div>

        <div class="col-6 h-100">
            <a href="https://chotu.com/" class="logo_customise">
            <img src="https://chotu.com/wp-content/uploads/2021/10/chotu-dark.webp">
            </a>
        </div>
        <div class="col-3 t_right h-100">
            <div class="h-100 align-middle">
                <?php
                if (!is_user_logged_in()) {
                    if(!is_multisite()){
                    ?>
                     <a href="<?php echo home_url('/').'my-account';?>" class="digits-login-modal" type="1"><img class="filter_beige h-100 align-middle" src="https://dev.chotu.shop/wp-content/themes/chotu/images/account_circle_black.svg" style="width: 30px; height: 30px;border-radius: 50%;"></a>
                    <?php
                    }else{?>
                         <span href="?login=true" onclick="jQuery('this').digits_login_modal(jQuery(this));return false;" attr-disclick="1" class="digits-login-modal" type="1"><img class="filter_beige h-100 align-middle" src="https://dev.chotu.shop/wp-content/themes/chotu/images/account_circle_black.svg" style="width: 30px; height: 30px;border-radius: 50%;"></span>
                    <?php }
                }else{
                    //echo '<a href="javascript:;">Login</a>';
                    $balance = get_user_meta(get_current_user_id(),'mycred_default',true);
                    if($balance == '' || $balance == 0){
                        $balance = 0;
                    }
                    echo '<div class="balance_header h-100"><a class="text-decoration-none" href="'.home_url().'/my-balance">₹'.$balance.'</a></div>';
                }
                ?>
            </div>
        </div>
        
    </nav>
</div>
<div class="collapse bg-white navbar-collapse hide" id="Toggler">
    <?php
    wp_nav_menu(
        array(
            'theme_location' => 'menu-1',
            'menu_id'        => 'primary-menu',
            'menu_class' => 'navbar-nav mx-auto',    
        )
    );
    ?>

</div>

</header>
<script>
    jQuery(document).ready(function() {
    jQuery('.navbar-toggler').click(function() {
        jQuery('.navbar-collapse').toggleClass("show");
      });
    });
</script>