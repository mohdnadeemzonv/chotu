<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */


add_action('init', function(){
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
    add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
});

if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    } 
}

if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {   
    function woocommerce_get_product_thumbnail( $size = 'woocommerce_thumbnail' ) {
        global $post, $woocommerce;
        $output = '<div class="thumb-image-renderd">';

        if ( has_post_thumbnail() ) {               
            $output .= get_the_post_thumbnail( $post->ID, $size );
        } else {
             $output .= wc_placeholder_img( $size );
             // Or alternatively setting yours width and height shop_catalog dimensions.
             // $output .= '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="300px" height="300px" />';
        }                       
        $output .= '</div>';
        return $output;
    }
} 

/* restrict access for non admin user and unauthenticated user */
/*
add_filter( 'rest_authentication_errors', function($result) {
  if ( ! empty( $result ) ) {
    return $result;
  }
  $request_url = strtok($_SERVER["REQUEST_URI"], '?');
  $allowedAddress =array('/wp-json/jwt-auth/v1/token','/wp-json/v1/get_gated_locality');
  $current_route = isset($request_url) ? $request_url : '/';
  if( in_array( $current_route, $allowedAddress ) ){
    return $result;
  }else{
     if( !is_user_logged_in() ) {
        return new WP_Error( 'rest_cannot_access', __( 'Only authenticated users can access this endpoint.', 'disable-json-api' ), array( 'status' => rest_authorization_required_code() ) );
      }elseif (!is_admin()) {
        return new WP_Error( 'rest_cannot_access', __( 'Only authenticated users can access this endpoint.', 'disable-json-api' ), array( 'status' => rest_authorization_required_code() ) );
      }
       elseif ( ! current_user_can( 'administrator' ) ) {
        return new WP_Error( 'rest_not_admin', 'You are not an administrator.', array( 'status' => 401 ) );
      }
      //User is logged in, approve all
      else {
        return $result;
      }
  }
  
 
 
});
*/