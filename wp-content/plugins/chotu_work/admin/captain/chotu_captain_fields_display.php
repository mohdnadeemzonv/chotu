<?php 
/*
  Add meta fields to captain user profile
*/
if ( in_array( 'captain',$profileuser->roles ) ) {
    // $geography = get_the_author_meta( 'geography', $profileuser->ID );
    $captain_locality = array_column(get_captain_locality($profileuser->ID), 'locality_id');

    $captain_brand = array_column(get_captain_brand($profileuser->ID), 'brand_id');
    $geography = array_column(get_user_meta_geography($profileuser->ID),'cat');//get_location_object_data_by_type($profileuser->ID,'captain');
    // $captain_brand = json_decode(get_user_meta($profileuser->ID, 'captain_brand', 'single' ),true);
    // $captain_locality = json_decode(get_user_meta($profileuser->ID, 'captain_locality', 'single'),true);?>
<table class="form-table">
   <tr>
    <th><label for="geography"><?php _e("Captain Geography"); ?></label></th>
    <td>
        <div id="geographydiv" class="postboxs" bis_skin_checked="1">
           <div class="inside" bis_skin_checked="1">
              <div id="taxonomy-geography" class="categorydiv" bis_skin_checked="1">
                 <ul id="geography-tabs" class="category-tabs" style="display:none;">
                    <li class="tabs"><a href="#geography-all" style="display:none;">All Geography</a></li>
                    <li class="hide-if-no-js"><a href="#geography-search" class="search-tab" tabindex="4" id="geography-search-tab">Search</a></li>
                 </ul>
                 <div id="geography-all" class="tabs-panel" bis_skin_checked="1" <?php echo empty($geography) ? 'style="display:none;"':'';?>>
                    <input type="hidden" name="tax_input[geography][]" value="0">           
                    <?php $id ='id="geographychecklist"';
                        hierarchical_category_tree($geography, 'geography', 0,'categorychecklist form-no-clear',$id ); ?>
                 </div>
                 <div id="geography-search" class="tabs-panels">
                    <input type="text" name="geography-search-field" id="geography-search-field" class="meta-box-search-field"><button type="button" id="geography-search-button" class="meta-box-search-button" onclick="showTaxonomyList('geography-all')">Geography</button>
                    <ul id="geography-search-results" class="meta-box-search-results"></ul>
                 </div>
                
              </div>
           </div>
        </div>
        <!-- <select class="" name="geography" id="geography"> -->
            <?php 
            /*
            $dropdown = wp_dropdown_categories(array('taxonomy' => 'geography', 'name' => 'brand_category', 'echo' => 0, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => $geography, 'hide_empty' => 0, 'orderby' => 'name', 'hierarchical' => 1));
                         // Hackily add in the data link parameter.
                     $dropdown = str_replace('&nbsp;', '-', $dropdown);
                     $dropdown = preg_replace( 
                        '^' . preg_quote( '<select ' ) . '^', 
                        '<select name="geography[]" multiple="multiple"', 
                        $dropdown
                    );
                    echo $dropdown;
             //echo chotu_work_geography_dropdown(array('selected' => $geography));
            //$locality_geography = chotu_get_taxonomy_options('geography',$geography);
            */
            ?>
        <!-- </select> -->
    </td>
</tr>
<tr>
    <th><label for="captain_brand"><?php _e("Brands"); ?></label></th>
    <td>
        <select class="captain-brand-ajax" name="captain_brand[]" multiple="multiple">
            <?php
            if(!empty($captain_brand) && $captain_brand !=''){
            // if(!empty($captain_locality) && $captain_locality !=''){
            $sites = get_sites(array('site__in'=> $captain_brand,'public'  => 1));
            if(!empty($sites)){
             foreach($sites as $site){
               if($site->blog_id !=1 ){
                   $blog_details = get_blog_details($site->blog_id);
                   $term_id = get_site_meta($site->blog_id,'brand_category_subcategory','single');
                   $term = get_term( $term_id, 'brand_category' );
                   $term_name='';
                   if(isset($term->name)){
                    $term_name = $term->name.' > ';
                   }
                   if(in_array($site->blog_id, $captain_brand)){
                      echo '<option value="'.$site->blog_id.'" selected>'.$term_name.$blog_details->blogname.'</option>';
                  }
            }
        }
     }
    } ?>
</select>
</td>
</tr>
<tr>
    <th><label for="captain_locality"><?php _e("Locality"); ?></label></th>
    <td>
        <select class="captain-locality-ajax" name="captain_locality[]" multiple="multiple">
            <?php 
            // $localities = get_localities('locality');
            if(!empty($captain_locality) && $captain_locality !=''){
               $args = array('post_type'=>'locality','post__in' => $captain_locality,'post_status'=>'publish','posts_per_page'=> -1);
                $localities = get_posts($args);
                if(!empty($localities)){
                    $cat_name='';
                    foreach ($localities as $key => $locality) {
                        $term_list = wp_get_object_terms( $locality->ID, 'geography', array( 'fields' => 'names' ));
                        if(!empty($term_list)){
                            $cat_name = $term_list[0];
                        }
                         if(in_array($locality->ID, $captain_locality)){
                            echo '<option value="'.$locality->ID.'" selected>'.$cat_name.' &gt; '.$locality->post_title.'</option>';
                          }
                    }
                }  
            }
           
            } ?>
        </select>
    </td>
</tr>
<script type="text/javascript">
        jQuery( document ).ready(function( $ ){
            $( '.user-last-name-wrap').remove();
        } );
    </script>
</table>
