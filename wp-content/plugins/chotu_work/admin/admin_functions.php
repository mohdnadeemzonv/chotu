<?php
/* Add banner pic to the brand website*/
function chotu_work_customize_register( $wp_customize ) {
 // Add Section
$wp_customize->add_section('brand_banner_pic', array(
      'title'             => __('Brand Banner Pic', 'name-theme'), 
      'priority'          => 70,
  )); 
$wp_customize->add_setting( 'banner_pic', array(
        'default' => get_bloginfo('template_directory') . '/images/logo.png',
    ) );
$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'logo',
           array(
               'label'      => __( 'Upload a Banner Pic', 'theme_name' ),
               'section'    => 'brand_banner_pic',
               'settings'   => 'banner_pic',
               'height'=>1000, // cropper Height
               'width'=>1618, // Cropper Width
               'flex_width'=>true, //Flexible Width
               'flex_height'=>true, //
               'context'    => 'your_setting_context'
           )
       )
   );
}
add_action('customize_register', 'chotu_work_customize_register');


/* As super-admin, when you add a user as network-admin, this will replace the default username/email ID with just phone number. Phonenumber@chotu.app is the default email ID and username=phone-number.*/
/*
add_action('network_user_new_form','network_user_new_form_fields');
function network_user_new_form_fields(){?>
  <style type="text/css">
    #adduser .form-table{
      display: none;
    }
  </style>
  <table class="form-tables" role="presentation">
    <tbody><tr class="form-field form-required">
      <th scope="row"><label for="username">Phone Number</label></th>
      <td><input type="text" class="regular-text" name="user[phone_number]" id="phone_number" autocapitalize="none" autocorrect="off" maxlength="60"></td>
    </tr>
  </tbody></table>
  <tr class="form-field">
      <td colspan="2" class="td-full">Please enter the user email ID as 10-digit Indian phone number @chotu.com. Example: if phone number is +91 98765 43210, enter it as 9876543210@chotu.com</td>
    </tr>
    <script type="text/javascript">
            jQuery(document).on('click','#add-user',function(e){
            e.preventDefault();
            jQuery('#username').val(jQuery('#phone_number').val());
            jQuery('#email').val(jQuery('#phone_number').val()+'@chotu.com');
            jQuery('#adduser').submit();
            });
    </script>
<?php }
*/


/**
 * Allow very short user names.
 *
 * @wp-hook wpmu_validate_user_signup
 * @param   array $result
 * @return  array
 */
/* Remove the errors while adding users.*/
add_filter('wpmu_validate_user_signup', 'chotu_remove_user_name_error' );
function chotu_remove_user_name_error( $result )
{
    $error_name = $result[ 'errors' ]->get_error_message( 'user_name' );
    if ( empty ( $error_name ))
    {
        return $result;
    }
    unset ( $result[ 'errors' ]->errors[ 'user_name' ] );
    return $result;
}

/*Rename the admin menu from Sites to Brands*/
add_action("network_admin_menu", function () {
    foreach ($GLOBALS["menu"] as $position => $tab) {
        if ("Sites" === $tab["0"]) {
            $GLOBALS["menu"][$position][0] = "Brands";
            break;
        }
    }
});


/* we replace email with phone number completely. The unique identifier is phone number instead of email. A user who enters 9866011883 as a phone number, we create a dummy email called 9866011883@chotu.com. For this dummy email, we should not verify by email or not send any notifications. The following code does this.

skip email notification to new user */
add_filter( 'wpmu_signup_user_notification', '__return_false' ); // Disable confirmation email
add_filter( 'wpmu_welcome_user_notification', '__return_false' );
add_action( 'user_new_form', 'chotu_work_admin_registration_form' );
function chotu_work_admin_registration_form( $operation ) {
  if ( 'add-new-user' !== $operation ) {
    // $operation may also be 'add-existing-user'
    return;
  }

  $phone_number = ! empty( $_POST['phone_number'] ) ? intval( $_POST['phone_number'] ) : '';

  ?>
  <h3><?php esc_html_e( 'Personal Information', 'crf' ); ?></h3>

  <table class="form-table">
    <tr class="form-field form-required">
      <th><label for="phone_number"><?php esc_html_e( 'Phone Number', 'crf' ); ?></label> <span class="description"><?php esc_html_e( '(required)', 'crf' ); ?></span></th>
      <td>
        <input type="number"min="10" max="10"step="1"id="phone_number"name="phone_number"value="<?php echo $phone_number;?>"class="regular-text"/>
      </td>
    </tr>
  </table>
  <?php
}

add_action('user_new_form', 'chotu_work_user_new_form', 10, 1);
add_action('show_user_profile', 'chotu_work_user_new_form', 10, 1);
add_action('edit_user_profile', 'chotu_work_user_new_form', 10, 1);

/*creates the email and removes the notification*/
function chotu_work_user_new_form($form_type) {
    ?>
    <script type="text/javascript">
        jQuery('#email').closest('tr').removeClass('form-required').find('.description').remove();
        if(jQuery('#phone_number').length < 10){
          jQuery('#phone_number').focus();
          //return confirm("Please enter the 10-digit phone number");
        }
        // Uncheck send new user email option by default
        <?php if (isset($form_type) && $form_type === 'add-new-user') : ?>
            jQuery('#send_user_notification').removeAttr('checked');
            jQuery(document).on('click','#createusersub',function(e){
            event.preventDefault();
            jQuery('#email').val(jQuery('#phone_number').val()+'@chotu.com');
            jQuery("#noconfirmation").attr('checked',true);
            jQuery('#createuser').submit();
              
            });
        <?php endif; ?>
    </script>
    
    <?php
}

/*updates all custom meta fields*/
add_action( 'personal_options_update', 'save_extra_profile_fields');
add_action( 'edit_user_profile_update', 'save_extra_profile_fields');
add_action( 'user_register', 'save_extra_profile_fields' );
function save_extra_profile_fields( $user_id ) {
  if (!current_user_can('manage_options'))
      return false;
    if(isset($_POST['phone_number'])){
      update_user_meta($user_id, 'phone_number', $_POST['phone_number']);
    }
  //
    if(isset($_POST['field_61079f717fcaa'])){
      $email = esc_attr( $_POST['field_61079f717fcaa'] ).'@chotu.com';
          $args = array(
          'ID'         => $user_id,
          'user_email' => $email
      );
      wp_update_user( $args );
    }
    if(isset($_POST['brand_geography'])){
      update_user_meta($user_id, 'brand_geography', $_POST['brand_geography']);
    }
}

/* updates location data (lat_long, pincode, address & geo_breadcrumb) for any object in location custom table*/
function chotu_update_location_data($reference_id,$reference_type,$lat_long,$pincode,$full_address,$geography_breadcrumb){
  global $wpdb;
  $chotu_master_locations = 'chotu_master_locations';
  // $lat_long = str_replace(" ", '', $lat_long);
  if($lat_long == ''){
    $lat_long = '0,0';
  }
  $lat_long = str_replace(",", ' ', $lat_long);
  if(chotu_checkExist($reference_id,$reference_type) > 0){
    if(isset($lat_long)){
      $wpdb->query("UPDATE $chotu_master_locations SET lat_long=ST_GeomFromText('POINT($lat_long)'), pincode='$pincode', full_address='$full_address' WHERE reference_id=$reference_id AND reference_type='$reference_type'");
     
    }else{
     
      // $wpdb->query("UPDATE $chotu_master_locations SET geography_breadcrumb='$geography_breadcrumb' WHERE reference_id='$reference_id' AND reference_type='$reference_type'" );
    }
  }else{
    if(isset($lat_long)){
      $wpdb->query("INSERT INTO $chotu_master_locations (`reference_id`, `reference_type`, `lat_long`,`pincode`,`full_address`) VALUES ('$reference_id', '$reference_type', ST_GeomFromText('POINT($lat_long)'),'$pincode','$full_address')" );
    }
  }
  return true;
}

/* to fetch the location data for a given object with type and id. (locality, captain, brand)*/
function get_location_object_data_by_type($reference_id,$reference_type){
  global $wpdb;
  $chotu_master_locations = 'chotu_master_locations';
  return $wpdb->get_row("SELECT location_ID,reference_ID,reference_type,pincode,full_address, geography_breadcrumb, GROUP_CONCAT(ST_X(`lat_long`),',',ST_Y(`lat_long`)) AS lat_long FROM $chotu_master_locations WHERE reference_id = '$reference_id' AND reference_type='$reference_type'");
}

/* Add custom image size of 600px by 200px - banner pic*/
add_action( 'after_setup_theme', 'chotu_theme_setup' );
function chotu_theme_setup() {
    add_image_size( 'banner_pic', 1618,1000,true ); // 300 pixels wide (and unlimited height)
    // add_image_size( 'homepage-thumb', 100, 300, true ); // (cropped)
}

/* We made geography as category. category by default is mult-select. the following function restricts this to a radio button */

/* commented by nadeem for making multiselect as per Ravi's instruction.*/
//add_filter( 'wp_terms_checklist_args', 'chotu_work_term_radio_checklist' );
function chotu_work_term_radio_checklist( $args ) {
    if ( ! empty( $args['taxonomy'] ) && $args['taxonomy'] === 'geography' /* <== Change to your required taxonomy */ ) {
        if ( empty( $args['walker'] ) || is_a( $args['walker'], 'Walker' ) ) { // Don't override 3rd party walkers.
            if ( ! class_exists( 'Chotu_work_Walker_Category_Radio_Checklist' ) ) {
                /**
                 * Custom walker for switching checkbox inputs to radio.
                 *
                 * @see Walker_Category_Checklist
                 */
                class Chotu_work_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
                    function walk( $elements, $max_depth, ...$args ) {
                        $output = parent::walk( $elements, $max_depth, ...$args );
                        $output = str_replace(
                            array( 'type="checkbox"', "type='checkbox'" ),
                            array( 'type="radio"', "type='radio'" ),
                            $output
                        );

                        return $output;
                    }
                }
            }

            $args['walker'] = new Chotu_work_Walker_Category_Radio_Checklist;
        }
    }

    return $args;
}


/*creates a delivery cycle for a brand basis the input in Brand Settings. This is the production cycle of a brand.*/
function create_brand_order_cycle($blog_id,$number_of_time){
  global $wpdb;
  $days = get_all_available_weekday($blog_id);
  $brand_order_cycle = get_site_meta($blog_id,'brand_order_cycle','single');
  $output_dates = array();
  foreach ($days as $key => $value) {
    if($value->meta_value == 1){
      $dayname = getDay($key);
      $output_dates[] = date('Y-m-d', strtotime("next ".$dayname.""));
      if(empty($brand_order_cycle)){
        add_site_meta($blog_id,'brand_order_cycle',date('Y-m-d', strtotime("next ".$dayname."")));
      }
    }else{
      $output_dates[] = '';
    }
  }
  $week_after_date = date('Y-m-d',strtotime("+7 day"));
  
  $data = $wpdb->get_results("DELETE FROM wp_blogmeta WHERE meta_value > '$week_after_date' AND blog_id=$blog_id AND meta_key='brand_order_cycle'");
  $counter = 7;
  if(!empty($output_dates)){
    for ($i = 1; $i < $number_of_time; $i++){
      foreach ($output_dates as $key => $output_date) {
       if($output_date !='' && isset($output_date)){
        add_site_meta($blog_id,'brand_order_cycle',date('Y-m-d',strtotime($output_date." +".$counter." day")));
       }
      }
      $counter += 7;
    }
  }
  return true;
}

/*
creates a dropdown for geo_breadcrumb
*/
function chotu_work_geography_dropdown($geography) { 

$dropdown = wp_dropdown_categories(array('taxonomy' => 'geography', 'name' => 'geography', 'echo' => 0, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => $geography, 'hide_empty' => 0, 'orderby' => 'name', 'hierarchical' => 1));
     // Hackily add in the data link parameter.
 $dropdown = str_replace('&nbsp;', '-', $dropdown);
 
echo $dropdown;
}

/*prints the error notices*/
function chotu_work_admin_notices(){
  //if ( ! session_id() ) {
   // session_start();
  //}
 
  if(isset($_SESSION['my_admin_notices'])){
    
    echo $_SESSION['my_admin_notices'];
    unset ($_SESSION['my_admin_notices']);
  }
  
}
add_action( 'admin_notices', 'chotu_work_admin_notices' );
add_action( 'network_admin_notices', 'chotu_work_admin_notices' );

/*renames first name in a user_profile as shop name*/
add_filter( 'gettext', 'chotu_work_rename_first_name_from_user_page', 10, 2 );
function chotu_work_rename_first_name_from_user_page( $translation, $original )
{
    if ( 'First Name' == $original ) {
        return 'Shop Name';
    }
    return $translation;
}

/* fetch localities by the captain ID*/
function get_captain_locality($captain_id){
global $wpdb;
return $wpdb->get_results("SELECT locality_id FROM `chotu_captain_locality` WHERE `captain_ID` =$captain_id");
}

/* fetch brands by the captain ID*/
function get_captain_brand($captain_id){
global $wpdb;
return $wpdb->get_results("SELECT brand_id FROM `chotu_captain_brand` WHERE `captain_ID` = $captain_id");
}

/* REF786 add 2 new menu itmes - brand and captain in the central menu - redirection issues - COMMENTED now*/
if(is_main_site(0)){
  //add_action('admin_menu','chotu_add_admin_menu_brand_locality');
}

function chotu_add_admin_menu_brand_locality(){

  add_menu_page(
      'brands',
      'Brands',
      'manage_options',
      'brands',
      'show_page_brand',
      'dashicons-store',
      8
    );
  add_menu_page(
      'captains',
      'Captains',
      'manage_options',
      'captains',
      'show_page_captain',
      'dashicons-buddicons-groups',
      7
    );
   $current_user_id = get_current_user_id();
   if($current_user_id){
      $author = get_user_by( 'ID', $current_user_id );

      //if(in_array('administrator',$author->roles)){ }
   }
      
}

function show_page_brand(){
  ob_end_clean();
  ob_start();
  return wp_redirect(home_url().'/wp-admin/network/sites.php');
}
function show_page_captain(){
  ob_end_clean();
  ob_start();
   return wp_redirect(home_url().'/wp-admin/users.php?role=captain');
}

/*REF786 COMMENTED till here*/

/* Adds filter in the localities page under wp-admin */
add_action('restrict_manage_posts', 'chotu_work_filter_post_type_by_geography');
function chotu_work_filter_post_type_by_geography() {
  global $typenow;
  $post_type = 'locality'; // change to your post type
  $taxonomy  = 'geography'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => sprintf( __( 'Show all %s', 'textdomain' ), $info_taxonomy->label ),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
    ));
  };
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mohd Nadeem
 * @link 
 */
add_filter('parse_query', 'chotu_work_convert_id_to_term_in_query');
function chotu_work_convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'locality'; // change to your post type
  $taxonomy  = 'geography'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}

add_action('restrict_manage_posts', 'chotu_work_filter_post_type_by_locality_category');
function chotu_work_filter_post_type_by_locality_category() {
  global $typenow;
  $post_type = 'locality'; // change to your post type
  $taxonomy  = 'locality_category'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => sprintf( __( 'Show all %s', 'textdomain' ), $info_taxonomy->label ),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
    ));
  };
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mohd Nadeem
 * @link 
 */
add_filter('parse_query', 'chotu_work_convert_id_to_term_in_query_locality_category');
function chotu_work_convert_id_to_term_in_query_locality_category($query) {
  global $pagenow;
  $post_type = 'locality'; // change to your post type
  $taxonomy  = 'locality_category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}


/**************** brand table add colum for brand category and brand geography */
// define the manage_sites_custom_column callback 


/**
* To add a columns to the sites columns
*
* @param array
*
* @return array
*/
add_filter( 'wpmu_blogs_columns', 'chotu_work_blogs_table_columns',999,1 );
function chotu_work_blogs_table_columns($sites_columns){
  unset($sites_columns['users']);
  unset($sites_columns['mycred']);
  unset($sites_columns['lastupdated']);
  unset($sites_columns['registered']);

  $sites_columns['brands_geography'] = 'Brand Geography';
  $sites_columns['brands_category'] = 'Brand Category';
  $sites_columns['pincode'] = 'Pincode';
  return $sites_columns;
 
}
// Hook to manage column data on network sites listing

 
/**
* Show page post count
*
* @param string
* @param integer
*
* @return void
*/
add_action( 'manage_sites_custom_column', 'chotu_work_sites_table_row', 10, 2 );
function chotu_work_sites_table_row($column_name, $blog_id)
{
    $location_table_data = get_location_object_data_by_type($blog_id,'brand');
    $blog_detail = get_blog_option( $blog_id, 'blogname' );
    
    if ( $column_name == 'brands_geography' ) {
        switch_to_blog($blog_id);
        restore_current_blog();
    
    if (!empty($location_table_data->geography_breadcrumb)){
      $term = get_term_by('id', $location_table_data->geography_breadcrumb, 'geography');
        echo  $term->name;
    }
        echo  '';
    }
    if($column_name == 'pincode'){
      if (!empty($location_table_data->pincode)){
        echo  $location_table_data->pincode;
      }
       echo  '';
      }

    if ( $column_name == 'brands_category' ) {
        switch_to_blog($blog_id);//not aware of a function that'll do this based on blog ID so we switch instead
        $brand_category = get_site_meta( $blog_id, 'brand_category','single');
        restore_current_blog();
    
    if (!empty($brand_category)){
      $term = get_term_by('id', $brand_category, 'brand_category');
        echo  $term->name;
    }
     echo  '';
    }
}


/*REF787 code commented*/
/*********** user page add column also filter functions */
//Add column to Network Admin User panel list page
function new_contact_methods( $contactmethods ) {
    $contactmethods['geography'] = 'Geography';
    return $contactmethods;
}
// add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );
/*REF787*/

/*wp-admin for users - adds a few columns and unsets a few*/
function new_modify_user_table_column( $column ) {
    unset($column['activation_code']);
    unset($column['posts']);
    unset($column['email']);
    unset($column['wfls_last_login']);
    $column['geographies'] = __('Geography','');
    $column['pincode'] = __('Pincode','');
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table_column' );

/*wp-admin for users - adds data to the above new columns*/

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    $location_table_data = get_location_object_data_by_type($user_id,'captain');
     switch ($column_name) {
        case 'pincode' :
            return isset($location_table_data->pincode) ? $location_table_data->pincode : '';
        case 'geographies' :
          $term = get_term_by('id', $location_table_data->geography_breadcrumb, 'geography');
          return isset($term->name) ? $term->name : '';
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );


function chotu_saved_post( $post_id, $xml_node, $is_update ) {
 // $record = json_decode( json_encode( ( array ) $xml_node ), 1 );
  $locality_lat_long = get_post_meta($post_id,'locality_lat_long',true);
  $locality_pincode = get_post_meta($post_id,'locality_pincode',true);
  $locality_address = get_post_meta($post_id,'locality_address',true);
  $geography = get_the_category( $post_id )[0]->name;
 chotu_update_location_data($post_id,'locality',$locality_lat_long,$locality_pincode,$locality_address,$geography);


}
add_action( 'pmxi_saved_post', 'chotu_saved_post', 10, 3 );

add_action('wp_ajax_nopriv_get_captain_brand','chotu_work_get_captain_brand');
add_action('wp_ajax_get_captain_brand','chotu_work_get_captain_brand');
function chotu_work_get_captain_brand(){
  global $wpdb;
  
    if(isset($_GET['type']) && $_GET['type'] == 'captain_brand'){
      $search =$_GET['search'];
      if(strlen($_GET['type']) > 2){
      $data = $wpdb->get_results("SELECT `blog_id` as id, UCASE(REPLACE(`path`,'/','')) as text FROM `wp_blogs` WHERE `blog_id` !=1 AND `path` LIKE '%$search%'");
        $captain_brand = array (
          'results' => $data      
        );
      echo json_encode($captain_brand);
      wp_die();
    }
  }
  if(isset($_GET['type']) && $_GET['type'] == 'captain_locality'){
    $search =$_GET['search'];
    if(strlen($search) > 2){
      $data = $wpdb->get_results("SELECT p.`ID` as id, p.`post_title` as text FROM `wp_posts`as p LEFT JOIN wp_postmeta ON wp_postmeta.post_id = p.ID WHERE p.post_type = 'locality' AND p.post_title LIKE '%$search%' AND wp_postmeta.meta_key = 'locality_status' AND wp_postmeta.meta_value = 'active'");
      $localities = array (
        'results' => $data      
      );
    echo json_encode($localities);
    wp_die();
    }
    
  }

}
add_filter( 'wp_dropdown_cats', 'wp_dropdown_cats_multiple', 10, 2 );
function wp_dropdown_cats_multiple( $output, $r ) {
  $selected = is_array($r['selected']) ? $r['selected'] : explode( ",", $r['selected'] );
    if( is_array($selected ) ) {
        foreach ( array_map( 'trim', $selected ) as $value )
            $output = str_replace( "value=\"{$value}\"", "value=\"{$value}\" selected", $output );

    }
    return $output;
}
function hierarchical_category_tree($selected = array(), $taxonomy, $cat,$class,$id) {
   echo '<ul class="'.$class.'" '.$id.'>';
  $next = get_terms(array('taxonomy'=>$taxonomy,'orderby' => 'name','order' => 'ASC','parent'=>$cat,'fields' => 'all','hide_empty'=> 0) );
  if( $next ) :
    foreach( $next as $cat ) :
      $disabled = '';
      $childs = get_terms(array('taxonomy'=>$taxonomy,'orderby' => 'name','order' => 'ASC','parent'=>$cat->term_id,'fields' => 'all','hide_empty'=> 0) );
      if(!empty($childs)){
        $disabled = 'disabled';
      }
      if(in_array($cat->term_id,$selected)){
        echo '<li id="'.$taxonomy.'-'.$cat->term_id.'" class="popular-category"><label class="selectit"><input value="'.$cat->term_id.'" type="checkbox" checked name="'.$taxonomy.'[]" id="in-'.$taxonomy.'-'.$cat->term_id.'"  '.$disabled.'>' . $cat->name . '</label>';
      }else{
        echo '<li id="'.$taxonomy.'-'.$cat->term_id.'" class="popular-category"><label class="selectit"><input value="'.$cat->term_id.'" type="checkbox" name="'.$taxonomy.'[]" id="in-'.$taxonomy.'-'.$cat->term_id.'"  '.$disabled.'>' . $cat->name . '</label>';
      }
    
    hierarchical_category_tree($selected,$taxonomy, $cat->term_id,'children','');
   // dd($next);
    endforeach;    
  endif;

  echo '</li></ul>'; echo "\n";
}  
add_action( 'admin_footer-post.php', 'chotu_work_remove_top_categories_checkbox' );
add_action( 'admin_footer-post-new.php', 'chotu_work_remove_top_categories_checkbox' );

function chotu_work_remove_top_categories_checkbox()
{
    global $post_type;

    if ( 'locality' != $post_type )
        return;
    ?>
        <script type="text/javascript">
            jQuery("#geography-all li label input").each(function(){
              if (jQuery(this).parent().next('ul').hasClass('children')) {
                  jQuery(this).attr('disabled',true);
              }
          });
        </script>
    <?php
}