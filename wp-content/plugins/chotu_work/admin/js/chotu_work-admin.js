(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 /*converts the dropdown into search-select thru select2 library*/
	 $(document).ready(function() {
	 	$('#menu-site .wp-first-item > a').text('All Brands');
	 	$("#gated_locality_ID").select2( {
	 		placeholder: "Select locality",
	 		allowClear: true
	 	} );
	 	$("#brand_id").select2( {
	 		placeholder: "Select brand",
	 		allowClear: true
	 	} );
	 	$("#team_ids").select2( {
	 		placeholder: "Select locality",
	 		allowClear: true
	 	} );
	 	$("#geography").select2( {
	 		placeholder: "Select geography",
	 		dropdownCssClass : 'bigdrop',
	 		allowClear: true
	 	} );
	 	$("#brand_category").select2({
	 		dropdownCssClass : 'bigdrop',
	 		dropdownAutoWidth : true,
	 		multiple : true
	 	} 
	 	);

	 	$("#brand_geography").select2( {
	 		placeholder: "Select geography",
	 		dropdownCssClass : 'bigdrop',
	 		allowClear: true
	 	} );
	 	$("#captain_locality").select2( {
	 		placeholder: "Select Locality",
	 		allowClear: true
	 	} );
	 	$('.captain-brand-ajax').select2({
	 		minimumInputLength: 3,
	 		ajax: {
	 			url : ajaxurl,
	 			dataType: 'json',
	 			 data: function (params) {
			      var query = {
			        search: params.term,
			        type: 'captain_brand',
			        // captain_ID: $('#user_id').val(),
			        action:"get_captain_brand"
			      }
			      return query;
			    },
	 		}
		});
	 	//var captain_ID = $('#user_id').val();
	 	$('.captain-locality-ajax').select2({
	 		minimumInputLength: 3,
	 		ajax: {
	 			url : ajaxurl,
	 			dataType: 'json',
	 			data: function (params) {
			      var query = {
			        search: params.term,
			        type: 'captain_locality',
			        // captain_ID: $('#user_id').val(),
			        action:"get_captain_brand"
			      }
			      return query;
			    },  // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
				}
			});

			 //  jQuery('#lead_time').on('change',function(){
			 // 	console.log(jQuery(this).val());
			 // 	if(jQuery(this).val() == 'other'){
			 // 		jQuery('.lead_time_custom').removeClass('hidden');
			 // 	}else{
			 // 		jQuery('.lead_time_custom').addClass('hidden')
			 // 	}
			 // });

			 jQuery('#btnRight_code').on('click',function (e) {
			 	jQuery(this).prev('select').find('option:selected').remove().appendTo('#isselect_code');
			 });
			 jQuery('#btnLeft_code').on('click',function (e) {
			 	jQuery(this).next('select').find('option:selected').remove().appendTo('#canselect_code');
			 })
			 	//var captain_ID = $('#user_id').val();

				 // });
			});
	
	 /*to validate phone number in user_creation module*/
	 jQuery('#phone_number').on('keypress', function(e) {
	 	var $this = jQuery(this);
	 	var regex = new RegExp("^[0-9\b]+$");
	 	var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	     // for 10 digit number only
	     if ($this.val().length > 9) {
	     	e.preventDefault();
	     	return false;
	     }
	     if (e.charCode < 54 && e.charCode > 47) {
	     	if ($this.val().length == 0) {
	     		e.preventDefault();
	     		return false;
	     	} else {
	     		return true;
	     	}
	     }
	     if (regex.test(str)) {
	     	return true;
	     }
	     e.preventDefault();
	     return false;
	 });

	 /*check the 6-digits in brand_pincode. The same is auto-checked by ACF for locality & captain*/	
	 $(document).on('click', 'form input[type=submit]', function(e) {
	 	if($('#brand_pincode').val()){
	 		var isValid = $('#brand_pincode').val().length;
	 		if(isValid !=6 ) {
		      e.preventDefault(); //prevent the default action
		      $('#brand_pincode').focus();
		      return false;
		  }
		}

	});

		

	 // function validate_pincode(pincode){
	 	
	 // 	var isValid = $('#'+pincode).val().length;
	 //    if(isValid < 6) {
	 //      e.preventDefault(); //prevent the default action
	 //      $('#'+pincode).focus();
	 //      return false;
		// }
	 // }

	 jQuery(document).ready(function(){
	adminCategorySearch.init();
});

var adminCategorySearch = {
	init : function(){
		jQuery.expr[':'].Contains = function(a, i, m) {
		  return jQuery(a).text().toUpperCase()
			  .indexOf(m[3].toUpperCase()) >= 0;
		};

		jQuery('body').on('change keyup', '.meta-box-search-field', function(){
			console.log('nd');
			var s = jQuery(this).val();
			if ( jQuery.trim(s) == "" )
			{
				jQuery(this).parents('.inside').find('.categorydiv').first().find('.categorychecklist li').show();
			}
			else
			{
				var result = jQuery(this).parents('.inside').find('.categorydiv').first().find('.categorychecklist li:Contains("'+s+'")');
				console.log(s);
				jQuery(this).parents('.inside').find('.categorydiv').first().find('.categorychecklist li').hide();
				result.each(function(){
					jQuery(this).show();
					if (adminCategorySearch.showSubcategories) {
						jQuery(this).find('ul li').show();
					}
				});
			}
		});

		jQuery('body').on('click', '.clear-meta-box-search-field', function(e){
			e.preventDefault();
			console.log('Clcik happens');
			jQuery(this).parents('.hide-if-no-js').find('.meta-box-search-field').val('');
			jQuery(this).parents('.hide-if-no-js').find('.meta-box-search-field').trigger('keyup');
		});

		jQuery('body').on('click', '.custom-meta-box-show-all-link', function(e){
			e.preventDefault();
			jQuery('#geography-all').css('display','none');
			// jQuery(this).parents('.categorydiv').first().find('.categorychecklist li').show();
			// jQuery(this).parents('.categorydiv').find('.meta-box-search-field').val('');
		});

		jQuery('.categorydiv').each(function(){
			jQuery(this).append('<p><a href="javascript:void(0);" class="custom-meta-box-show-all-link">Reset Search Results</a></p>');
		});

		jQuery('body').on('change keyup', '.postbox-search-field', function(){
			var s = jQuery(this).val();
			if ( jQuery.trim(s) == "" )
			{
				jQuery(this).parents('.inside').find('select').find('option').show();
			}
			else
			{
				var result = jQuery(this).parents('.inside').find('select option:Contains("'+s+'")');
		
				jQuery(this).parents('.inside').find('select option').hide();
				result.each(function(){
					jQuery(this).show();
					if (adminCategorySearch.showSubcategories) {
						jQuery(this).find('ul li').show();
					}
				});
			}
		});

		// jQuery('#geography-search-button').on('click',function(){
		// 	jQuery('#geography-all').css('display','block');
		// });
		// jQuery('#brand_category-search-button').on('click',function(){
		// 	jQuery('#brand_category-all').css('display','block');
		// });
	}
};
	 
	 
	})( jQuery );
function showTaxonomyList(taxonomy){
	jQuery('#'+taxonomy).css('display','block');
}