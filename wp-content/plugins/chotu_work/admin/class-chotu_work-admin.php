<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/admin
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_work_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_work_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_work_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_work-admin.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'chotu_work_c', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );	

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_work_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_work_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'chotu_work-select2', plugin_dir_url( __FILE__ ) . 'js/select2.full.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_work-admin.js', array( 'jquery' ), $this->version, false );

	}
	/*
	Add addres syntax to locality	
	*/


	public function chotu_meta_box_for_locality( $post ){
		    add_meta_box( 'address_syntax', __( 'Gated Locality Address Syntax', 'textdomain' ), array( $this, 'chotu_meta_box_for_locality_meta_box_html_output' ), 'locality' );
		}

	public function chotu_meta_box_for_locality_meta_box_html_output( $post ) {
		    wp_nonce_field( basename( __FILE__ ), 'meta_box_for_locality_meta_box_nonce' ); //used later for 
		    require_once plugin_dir_path( __FILE__ ) . 'locality/chotu_work_locality_address_syntax_fields.php';
		}

	/*
	Save addres syntax to locality	
	*/
		
	public function chotu_meta_box_for_locality_meta_box_save( $post_id ){
		    // check for nonce to top xss

		    if ( !isset( $_POST['meta_box_for_locality_meta_box_nonce'] ) ){
		        return;
		    }

		    //check if the entered pincode exists in the 19000+ list we already have.

		    if(empty(term_exists( $_POST['acf']['field_60f54fb4f09d9'], 'pincode' ))){
		  //   	if ( ! session_id() ) {
				// 	session_start(['read_and_close' => true,]);
				// }
		    	
		    	//$errors->add("locality_pincode","Pincode not exist.");
		    	 $_SESSION['my_admin_notices'] = '<div class="error"><p>The entered pincode does not exist. Please check.</p></div>';
		    	 update_post_meta($post_id,'locality_pincode','');
		    	 header('Location: '.get_edit_post_link($post_id, 'redirect'));
            	exit;
		    }
		    // check for correct user capabilities - stop internal xss from customers
		    // can the user edit the post or not. LocalityAT
		    if ( ! current_user_can( 'edit_post', $post_id ) ){
		        return;
		    }
		    
		    //convert address_syntax of 3 levels (name, level, type, options) into a JSON and saves it.
		     $address_syntax = array();
			   for ($i=1; $i < 4; $i++) {
			        if(isset($_REQUEST['level_'.$i.'_name']) && $_REQUEST['level_'.$i.'_name'] !='' && $_REQUEST['level_'.$i.'_type'] !=''){
			           $address_syntax['level_'.$i]['level'] = $_REQUEST['level_'.$i.'_name'];
			           $address_syntax['level_'.$i]['type'] = $_REQUEST['level_'.$i.'_type'];
			           $address_syntax['level_'.$i]['options'] = $_REQUEST['level_'.$i.'_values'];
			        }
			    }
			update_post_meta($post_id,'locality_address_syntax',json_encode($address_syntax));    	
		   
		   //	 locality_geography 
		    
		   $geography ='';
		   if(isset($_REQUEST['acf']['field_60f55058876b7'])){
		   	if(isset($_POST['tax_input']['locality_geography'][1])){
		   		$geography = $_POST['tax_input']['locality_geography'][1];
		   	}

		   //updates the custom table chotu_master_locations with the data: reference_type, lat_long, pincode, full_address & locality_geography.
		   chotu_update_location_data($post_id,'locality',$_POST['acf']['field_60f55058876b7'],$_POST['acf']['field_60f54fb4f09d9'],$_POST['acf']['field_60f54f7eea1bf'],$geography);
		   	//chotu_update_location_data($post_id,$_REQUEST['acf']['field_60f55058876b7'],'gated_locality');
		   }
			
		}

	
	
		// new site to have default time-zone as Asia/Kolkata.
	public function chotu_blog_update_option($data) {
		$blog_id = $data->blog_id;
		 global $wpdb;
	    switch_to_blog($blog_id);
	    $new_field_value = '';
	    update_option('gmt_offset','Asia/Kolkata');
	    update_option('timezone_string','Asia/Kolkata');
	    restore_current_blog();
	}

	
	/*
		Add custom profile fields to captain
	*/
    public function chotu_work_user_profile_field($profileuser) {
    	require_once plugin_dir_path( __FILE__ ) . 'captain/chotu_captain_fields_display.php';
       // echo '<script>document.getElementById("email").value = "user_'.$profileuser->ID.'@chotu.app";</script>';
    }

    //add extra fields to captain
    public function chotu_save_extra_user_profile_fields($user_id)
    {
    	$geography  = array_column(get_user_meta_geography($user_id,'brand_geography_'),'cat');
    	if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
    		return;
    	}

    	if ( !current_user_can( 'edit_user', $user_id ) ) { 
    		return false; 
    	}
    	if ( !empty( $_POST['phone_number'] ) ) {
	    	update_user_meta( $user_id, 'phone_number', $_POST['phone_number'] );
	    }
	    // if ( !empty( $_POST['geography'] ) ) {
	    // 	update_user_meta( $user_id, 'geography', $_POST['geography'] );
	    // }
		$user = new WP_user($user_id);
		if ( in_array( 'captain',$user->roles ) ) {

			// if ( ! session_id() ) {
			// 			   session_start(['read_and_close' => true,]);
			// 	}
				if(empty(term_exists( $_POST['acf']['field_610a2a6f1f561'], 'pincode' ))){
				$_SESSION['my_admin_notices'] = '<div class="error"><p>The entered pincode does not exist. Please check.</p></div>';
					update_user_meta($user_id,'captain_pincode','');
					session_write_close();
					header('Location: '.get_edit_user_link($user_id, 'redirect'));
					exit;
				return false; 
			}
		}
	    //update the table chotu_captain_brand
	    if(!empty($_POST['captain_brand'])){
	    	update_captain_brand_data($user_id,$_POST['captain_brand']);
	    	//update_user_meta( $user_id, 'captain_brand', json_encode($_POST['captain_brand']) );
	    }else{
	    	//update_user_meta( $user_id, 'captain_brand','' );
	    	delete_captain_data($user_id,'brand');
	    }

	    //update the table chotu_captain_locality
	    if(!empty($_POST['captain_locality'])){
	    	update_captain_locality_data($user_id,$_POST['captain_locality']);
	    	//update_user_meta( $user_id, 'captain_locality', json_encode($_POST['captain_locality']) );
	    }else{
	    	//update_user_meta( $user_id, 'captain_locality','' );
	    	delete_captain_data($user_id,'locality');
	    }
	   
	    if(isset($_POST['geography'])){
	    	$geography_post = $_POST['geography'];
	    	if(is_array($_POST['geography'])){
	    		foreach($_POST['geography'] as $cat){
	    			update_user_meta( $user_id, 'geography_'.$cat,$cat);
	    		}
	    		$diff = array_diff($geography, $_POST['geography']);
	    		// dd($diff);
	    		if(!empty($diff)){
	    			delete_user_meta_geography($user_id,$diff);
	    		}
	    	}
	    }else{
	    	//delete_user_meta( $user_id, 'geography_'.$cat);
	    }

	    //update the table chotu_master_locations
	    chotu_update_location_data($user_id,'captain',$_POST['acf']['field_610a2a871f562'],$_POST['acf']['field_610a2a6f1f561'],$_POST['acf']['field_60febafb71302'],'');

    }


    public function get_central_site_product_categories($search){
	  global $wpdb;
	  $wp_terms = "wp_terms";
	  $selected='';
	  //$data=array();
	  $i ='';
	  $search = htmlentities($search, ENT_HTML5  , 'UTF-8');;
	  $wp_term_taxonomy = "wp_term_taxonomy";
	  $sql = "SELECT * from (SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND name like '%$search%') as temp";
	  $brand_category = $wpdb->get_results($sql);
	  if(!empty($brand_category)){
   //       $tree_string=array();
	        foreach ($brand_category as $key => $value) {
	        	$temp = [];
	        	if($value->parent == 0){
	        		$temp['id'] = $value->term_id;
	        		$temp['text'] = $value->name;
	        		array_push($this->data,$temp);
	        	}else{
	        		$this->getParents($value->name,$value->parent, 0,$value->term_id);
	        	}
	        	if(!in_array($value->term_id,$this->visited)){
		        	$this->getChildrens($this->data[0]['text'],$value->term_id, 0);
		        }
	
	        }
	    }
	    
	    return $this->data;
	    //echo $html;
	}

	public function getParents($name,$parent_id, $level,$term_id) {
		global $wpdb;
		$wp_terms = "wp_terms";
		$wp_term_taxonomy = "wp_term_taxonomy";
		;
		if($parent_id ==0) return $data;
		$brand_category = $wpdb->get_results("SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND wp_term_taxonomy.term_id ='$parent_id'");
		$level++;
		$temp = [];
		if(!empty($brand_category)){
			// $this->data[0]['text'] = $name;
   //          $this->data[0]['id'] = $parent_id;
			foreach ($brand_category as $key => $value) {
				$this->data[$key]['id'] = $term_id;
         		$this->data[$key]['text'] = $value->name.' > '.$name;
         		// array_push($this->data,$temp);
         		if($value->parent !=0){
         			$this->getParents($value->name.' > '.$name,$value->parent,$level,$term_id);
         		}
         		
            	
			}
			//dd($this->data);
			//return $this->data; 
		}                             
	}

	public function getChildrens($name,$parent_id, $level) {
		global $wpdb;
		$wp_terms = "wp_terms";
		$wp_term_taxonomy = "wp_term_taxonomy";
		$brand_category = $wpdb->get_results("SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND  wp_term_taxonomy.parent = $parent_id");
		//dd($brand_category);
		// if($level == 1){
		// }
		$level++;
		$temp = [];
		if(!empty($brand_category)){
			// $data[0]['text'] = $name;
   //          $data[0]['id'] = $parent_id;
			foreach ($brand_category as $key => $value) {
				//$key++;
				array_push($this->visited,$value->term_id);
         		$temp['id'] = $value->term_id;
         		$temp['text'] = $name.' > '.$value->name;
         		array_push($this->data,$temp);
            	$this->getChildrens($name.' > '.$value->name,$value->term_id,$level);
			}
		}
		return true;
		
		//$this->data['visisted']=$visited;
		//return $temp;                            
	}

    public function get_global_product_category(){
    	if(isset($_GET['search'])){
    		$cat = $this->get_central_site_product_categories($_GET['search']);
    		$data = array (
	        'results' => $cat      
	      );
    		echo json_encode($data);
    	}
    	 
    
     wp_die();	
    }

    
    /*
		Create locality_concat = geo_breadcrumb + '>' + locality name
    */
	// public function chotu_set_post_default_category( $post_id, $post, $update ) {
	// 	global $post;
	// 	$locality_geography = wp_get_object_terms( $post_id, 'geography');
	// 	if(!empty($locality_geography)){
	// 		foreach ($locality_geography as $key => $value) {
	// 			update_post_meta($post_id,'geo_locality',$value->name.' > '.$post->post_title);
	// 			$term_taxonomy_ids = wp_set_object_terms( $post_id, $value->name.' > '.$post->post_title, 'geo_locality_concat');
	// 			$suggestion_term = $value->name.' > '.$post->post_title;

	// 				  $taxonomy = 'geo_locality_concat'; // The name of the taxonomy the term belongs in
	// 				  wp_set_post_terms( $post_id, array($suggestion_term), $taxonomy );
	// 		}
			
	// 	}
		
	// }

}