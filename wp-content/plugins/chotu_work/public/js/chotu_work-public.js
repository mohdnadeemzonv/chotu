(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	
	/* 
	 On captain page, on clicking the locality, the view button on brand gets enabled.
	 */

	 $( document ).on( 'change', 'input[name="locality_id"]', update_locality_id_to_url_param );
	  $( document ).on( 'click', 'input[name="locality_id"]',function(){
	  	console.log('click');
	  	$('.carousel').removeClass('hide');
	  });
	 function update_locality_id_to_url_param(locality_ids,locality_this){
		$('.carousel').removeClass('hide');
	 	console.log('locality_ids'+locality_ids);
	 		
	 	var object = (locality_this) ? locality_this : $(this);

	 	let title = $(object).attr('data-title');
	 	let address = $(object).attr('data-address');
	 	let phone_number = $('#phone_number').val();
	 	let a = "https://api.whatsapp.com/send/?phone="+phone_number+'&text=*'+title+'* %0a'+address+'%0aOrder Details: %0a';
	 	$('.whatsapp_link').attr('href',a);
	 	var locality_id = ($(this).val()) ? $(this).val() : locality_ids;
	 	
	 	$.each($('.url_param'), function(index, value) {
	 		$('.carousel').removeClass('hide');
	 		console.log($(value).attr('href'));
	 		if ($(value).attr('href').match(/\?./)) {
	 			const url =  new URL($(value).attr('href'));
				url.searchParams.set('l', locality_id);
				$(value).attr('href',url.href);
				$(value).removeClass('disabled');
				

	 		}
		});
	 }
	/*
	On captain page, when the locality_ID comes attached in the URL, keeps the locality checked.
	*/	 
	 $(document).ready(function(){

	 	if(location.search.split('l=')[1]){
	 		$('.carousel').removeClass('hide');
	 		var locality_id = location.search.split('l=')[1];
	 		$.each($('input[name="locality_id"]'), function(index, value) {
	 			console.log($(value).val());
	 			if($(value).val() == locality_id){
	 				$(this).attr('checked',true);
	 				var locality_this = $(this);
	 				
	 				update_locality_id_to_url_param(locality_id,locality_this);
	 			}
			});
	 	}else{
	 		$('.checkbox_input').prop('checked', false);
	 		// $('.carousel').removeClass('hide');
	 	}


	/************* start timer for order before time******************/
	jQuery(".order_before_time_card").each(function(index,value){
		var last_date_of_order = jQuery('.order_before_time_card').eq(index).attr('data-card');
		console.log('last_date_of_order'+last_date_of_order)
		if(last_date_of_order != ''){
			// var countDownDate = new Date("10/26/2020 12:29:29").getTime();
			var countDownDate=Date.parse(parseDateString(last_date_of_order));
          // Update the count down every 1 second
          var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();
              
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
              // console.log('countDownDate '+countDownDate);
              // console.log('now '+now  );
              // console.log('distance'+distance);
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
              
            // Output the result in an element with id="demo"
            jQuery('.timer').eq(index).html(' '+ days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ");
              
            // If the count down is over, write some text 
            if (distance < 0) {
              clearInterval(x);
              jQuery('.timer').eq(index).html(": Expired");
            }
          }, 1000);
      }else{
      	jQuery('.timer').eq(index).html(": N/A");
      }
            
	});

	$('#join_my_whatsapp').on('click',function(){
		$('.wpforms-submit').trigger('click');
		$('.wpforms-submit').removeClass('hide');
	});
	$(document).on('click','#load_more',function(){
		$('.carousel').removeClass('hide');
		$('.load_more').addClass('hide');
	});

});

	 function parseDateString (dateString) {
            var matchers = [];
            matchers.push(/^[0-9]*$/.source);
            matchers.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
            matchers.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
            matchers = new RegExp(matchers.join("|"));
            if (dateString instanceof Date) {
                return dateString;
            }
            if (String(dateString).match(matchers)) {
                if (String(dateString).match(/^[0-9]*$/)) {
                    dateString = Number(dateString);
                }
                if (String(dateString).match(/\-/)) {
                    dateString = String(dateString).replace(/\-/g, "/");
                }
                return new Date(dateString);
            } else {
                throw new Error("Couldn't cast `" + dateString + "` to a date object.");
            }
        }
	
})( jQuery );
