<?php 
global $blog_id, $wpdb;
$blog_id = get_current_blog_id();

$wpdb->set_blog_id( $blog_id );
//wp_get_attachment_image( absint( $vendor_data['logo'] ), 'full' );

/*
Retrieve post meta of central site from a network site (brand) for a custom post type (locality)
*/
function get_custom_post_meta($post_id,$meta_key){
  global $wpdb;
  $wp_postmeta = "wp_postmeta";
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND post_id = '".$post_id ."')"); 
}
/*
Retrieve post meta of central site from a network site (brand) for a user (captain or buyer)
*/
function get_custom_user_meta($user_id,$meta_key){
  global $wpdb;
  $wp_postmeta = "wp_usermeta";
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND user_id = '".$user_id ."')"); 
}
/*
Retrieve custom taxonomy of central site from a network site (brand) for any term
*/
function get_custom_term($term_id){
  global $wpdb;
  $wp_terms = "wp_terms";
  return $wpdb->get_var("SELECT name FROM $wp_terms WHERE (term_id = '".$term_id ."')");
}

/*
Retrieve the URL of any post-attachment (image) in central site from network site (brand).
$size param can be: 'large', 'medium', 'thumbnail' (square-pic),'full','banner-pic'
*/
function get_custom_attachment_image($post_attach_id,$size=''){
  global $wpdb;
  $postmeta = "wp_postmeta";
  $post_metadatas = $post_attach_id ? maybe_unserialize( $wpdb->get_var( "SELECT meta_value FROM $postmeta WHERE meta_key = '_wp_attachment_metadata' AND post_id = {$post_attach_id}" ) ) : NULL;
  if(isset($post_metadatas)){
    // $image = ($post_metadatas[ 'sizes' ][$size]['file'] ? $post_metadatas[ 'sizes' ][$size]['file']: '');
   return get_site_url(0) . '/wp-content/uploads/' . $post_metadatas[ 'file' ];
 }
 return '';
}

/*
Retrieve the post_attach_ID for a given post.
this post_attach_ID is called in above function to get the image.
*/
function get_the_post_thumb_id($post_id){
  global $wpdb;
  $wp_postmeta = "wp_postmeta";
  return $post_thumbnail_id = $wpdb->get_var( "SELECT meta_value FROM  $wp_postmeta WHERE meta_key = '_thumbnail_id' AND post_id = $post_id" );
}


/*
FINAL FRONTIER: Attach URL parameters to the brand button on home page, captain page and locality page
*/
function display_brands_with_url_param($brand_id,$locality_id,$captain_id){
  $blog_details = get_blog_details($brand_id);
  $delivery_date='';
  if(!empty($blog_details)){
    $delivery_order_cycle = get_delivery_date_and_cut_off_time($brand_id,$locality_id,$captain_id);
    if(!empty($delivery_order_cycle)){
      $delivery_date = $delivery_order_cycle->delivery_date;
    }
    return $blog_details->siteurl.'/?l='.$locality_id.'&c='.$captain_id.'&d='.date('Ymd', strtotime($delivery_date));
  }
}

/*REF788: CAN REMOVE Retrieve all brands coutn handled by a locality*/
function get_brands_count_by_locality($locality_id){
  global $wpdb;
  $chotu_captain_brand = 'chotu_captain_brand';
  return $wpdb->get_row("select COUNT(DISTINCT(c.brand_id)) as brand from chotu_captain_brand as c INNER JOIN wp_blogs AS b ON c.brand_id = b.blog_id WHERE c.captain_ID IN(select captain_ID from chotu_captain_locality WHERE locality_id=$locality_id) AND b.deleted = 0");
}

/*Retrieve all brands handled by a locality*/
function get_brands_by_localities($localities){
  global $wpdb;
  $captain_locality = 'chotu_captain_locality';
  if(is_array($localities)){
    $localities = implode(",", array_column($localities, 'ID'));
  }
  $captains = $wpdb->get_results("SELECT captain_ID FROM $captain_locality WHERE locality_id IN($localities)");
  if(!empty($captains)){
    return get_brands_by_captains($captains);
  }

}
/*Retrieve all localities handled by a brands*/
function get_localities_by_brand($brands){
  global $wpdb;
  if(is_array($brands)){
    $brands = implode(",", array_column($brands, 'ID'));
  }
  $captains = get_captain_by_brand($brands);
  if(!empty($captains)){
    return get_locality_by_captain($captains);
  }
}

/*Retrieve all localities handled by a captain*/
function get_locality_by_captain($captains){
  global $wpdb;
  $captain_locality = 'chotu_captain_locality';
  if(is_array($captains)){
    $captains = implode(",", array_column($captains, 'ID'));
  }
  return $wpdb->get_results("SELECT locality_id from $captain_locality WHERE captain_ID IN($captains)");
}

/*Retrieve all captains in a locality_id*/
/**
 * @param $locality array()
 * */
function get_captains_by_localities($localities){
  global $wpdb;
  $captain_locality = 'chotu_captain_locality';
  if(is_array($localities)){
    $localities = implode(",", array_column($localities, 'ID'));
  }
  return $wpdb->get_results("SELECT captain_ID FROM $captain_locality as cl INNER JOIN wp_users as u ON cl.captain_ID=u.ID WHERE cl.locality_id IN($localities)");
}

/*Retrieve all captain handled by a brand*/
function get_captain_by_brand($brands){
  global $wpdb;
  $captain_locality = 'chotu_captain_brand';
  if(is_array($brands)){
    $brands = implode(",", array_column($brands, 'ID'));
  }
  return $wpdb->get_results("SELECT captain_ID from $captain_locality as cb INNER JOIN wp_users as u ON cb.captain_ID = u.ID WHERE cb.brand_id IN($brands)");
}

/*Retrieve all brands handled by a captain_id*/
/**
 * @param  $captain_id 
 * type int
 * */
function get_brands_by_captains($captains){
  global $wpdb;
  $captain_brand = 'chotu_captain_brand';
  if(is_array($captains)){
    $captains = implode(",", array_column($captains, 'captain_ID'));
  }
  return $wpdb->get_results("SELECT DISTINCT(brand_id) from $captain_brand as cb INNER JOIN wp_usermeta as um ON cb.captain_ID = um.user_id WHERE cb.captain_ID IN($captains) AND um.meta_key = 'wp_capabilities'AND um.meta_value LIKE '%captain%'"); 
}

/*RED 790: CAN REMOVE Select count of order done by captain*/
function get_order_count_by_captain($captain_ID){
  global $wpdb;
  $chotu_orders = 'chotu_orders'; 
  return $wpdb->get_row("SELECT COUNT(DISTINCT(order_ID)) as count FROM $chotu_orders WHERE captain_ID=$captain_ID order by count desc");
}
/* Retrieve the weekday from the index */
function getDay($number){
  $weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
  $day_numeric = date($number);
  return $weekdays[$day_numeric];
}

/*Retrieve all the order cycles available for a brand*/
function get_all_available_weekday($brand_id){
  global $wpdb;
  $blogmeta = $wpdb->base_prefix.'blogmeta';
  return $wpdb->get_results("SELECT meta_value FROM $blogmeta WHERE `meta_key` LIKE '%brand_weekday%' AND blog_id =$brand_id");
  
}
/*Retrieve the delivery date and cut off time for a given brand_ID. Locality_ID and Captain_ID are here as placeholders, maybe required in future. Right now, only brand_ID suffices.*/
function get_delivery_date_and_cut_off_time($brand_id,$locality_id,$captain_id){//$brand_id,$locality_id,$captain_id
  global $wpdb;
  $blogmeta = $wpdb->base_prefix.'blogmeta';
  $date_time_now = date('Y-m-d H:i');
  $order_before_time_sql = "(SELECT CONCAT(meta_value,' ',(SELECT meta_value from wp_blogmeta WHERE  meta_key = 'brand_cut_off_time' AND  `blog_id` = '$brand_id')) as date_and_time from wp_blogmeta WHERE meta_key = 'brand_order_cycle' AND  `blog_id` = '$brand_id' HAVING date_and_time >= '$date_time_now' order by date_and_time ASC limit 1)";
  return $wpdb->get_row("SELECT DATE_ADD($order_before_time_sql, INTERVAL (SELECT meta_value from wp_blogmeta WHERE  meta_key = 'brand_lead_time' AND  `blog_id` = '$brand_id') DAY) as delivery_date,$order_before_time_sql as order_before_time");
}

/* check coming delivery date exist in database or not*/
function check_delivery_date_existing($blog_id,$date_of_delivery){
  global $wpdb;
  $blogmeta = $wpdb->base_prefix.'blogmeta';
  return $wpdb->get_row("SELECT * FROM `wp_blogmeta` WHERE `blog_id` = $blog_id AND `meta_key`= 'brand_order_cycle' AND `meta_value`= $date_of_delivery");
}

add_action('init', 'chotu_work_set_cookie_by_geo_ip');
// my_setcookie() set the cookie on the domain and directory WP is installed on
function chotu_work_set_cookie_by_geo_ip(){
  if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
    $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
    if(isset($location['lat']) && isset($location['lon'])){
      setcookie('lat', $location['lat'], time() + (31536000));
      setcookie('lon', $location['lon'], time() + (31536000));
    }
    
  }

}
/* return all geography with all childrens by lcoality */
function get_geography_by_locality($locality_ids){
  $geographies = wp_get_object_terms (array_column($locality_ids,'locality_id'), 'geography');
  $childrens = array();
  global $wpdb;
  $brands= '';
  if(!empty($geographies)){
    return get_childrens_of_geography(array_column($geographies, 'term_id'));
  }
}
/* return all brand by geography with all childrens*/
function get_brand_by_geography($geographies){
  global $wpdb;
  //$geographies = array_merge($geographies,$geographies);
  $brands= '';
  if(!empty($geographies)){
    if (array_key_exists("term_id",$geographies)){
      $geographies = array_column($geographies, 'term_id');
    }
    $all_geography = get_childrens_of_geography($geographies);
    $geography_ids_list = implode(",",$all_geography);
    $brands = $wpdb->get_results("SELECT DISTINCT(blog_id) as brand_id FROM `wp_blogmeta` WHERE `meta_key` LIKE '%brand_geography%' AND meta_value IN($geography_ids_list)");
  }
  return $brands;
}

/* return all locality by geography with all childrens*/
function get_locality_by_geography($geographies){
  global $wpdb;
  if (array_key_exists("term_id",$geographies)){
    $geographies = array_column($geographies, 'term_id');
  }
  $all_geography = get_childrens_of_geography($geographies);
  $geography_ids_list = implode(",",$all_geography);
  if(!empty($geographies)){
    $SQLquery = "SELECT wp_posts.ID FROM wp_posts LEFT JOIN wp_term_relationships ON ( wp_posts.ID = wp_term_relationships.object_id  )  WHERE wp_term_relationships.term_taxonomy_id IN ( $geography_ids_list ) AND wp_posts.post_type = 'locality' AND ( ( wp_posts.post_status = 'publish' ) ) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC";
    $localities = $wpdb->get_results($SQLquery);
  //   $localities = get_posts( array(
  //     'post_type' => 'locality',
  //     'tax_query' => array(
  //       array(
  //         'taxonomy' => 'geography',
  //         'field' => 'term_id',
  //         'terms' => $all_geography
  //       )
  //     )
  //   ) 
  // );
  }
  return $localities;
}
/* return all geography with all childrens by brand*/
function get_geography_by_brand($brand_ids){
  global $wpdb;
  //$geographies = array_merge($geographies,$childrens);
  if(!empty($brand_ids) || $brand_ids !=''){
    //$meta_value = implode(",",array_column($childrens, 'term_id'));
    $geographies = $wpdb->get_results("SELECT DISTINCT(meta_value) as geography FROM `wp_blogmeta` WHERE `meta_key` LIKE '%brand_geography%' AND blog_id IN($brand_ids)");
    if(!empty($geographies)){
      return  get_childrens_of_geography(array_column($geographies, 'geography'));
    }
  }
}
/* commented by nadeem no use of this funtion at yet
if(!function_exists('category_has_children')){
  function category_has_children( $term_id = 0, $taxonomy = 'geography' ) {
    $children = get_categories( array( 
      'child_of'      => $term_id,
      'taxonomy'      => $taxonomy,
      'hide_empty'    => false,
    ) );
    if(!empty($children)){
      return array_column($children,'term_id');
    }
  }
}
*/

/* this function return all geography with their children end level*/
if(!function_exists('get_childrens_of_geography')){
  function get_childrens_of_geography($geographies){
    $childrens = array();
    foreach ($geographies as $key => $geography) {
      if(!empty(getChildren( $geography,'geography' ))){
        $children = getChildren( $geography,'geography' );
        $childrens = array_merge($geographies,$children);
      }
    }
    if(empty($childrens)){
      return $geographies;
    }
    return $childrens;
  }
}
if(!function_exists('getChildren')){
  function getChildren($parent_id,$geography) {
    $tree = array();
    if (!empty($parent_id)) {
      $tree = getOneLevel($parent_id,$geography);
      foreach ($tree as $key => $val) {
        $ids = getChildren($val,$geography);
        $tree = array_merge($tree, $ids);
      }
    }
    return $tree;
  }
}
if(!function_exists('getOneLevel')){
  function getOneLevel($catId,$geography){
    global $wpdb;
    $wp_terms = "wp_terms";
    $wp_term_taxonomy = "wp_term_taxonomy";
    $brand_category = $wpdb->get_results("SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'geography' AND  WHERE parent='".$catId."'");
    $cat_id=array();
    if(!empty($brand_category)){
      foreach ($brand_category as $key => $value) {
        $cat_id[] = $value->term_id;
      }
    }   
    return $cat_id;
  }
}



