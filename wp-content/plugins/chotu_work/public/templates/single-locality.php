<?php
//This is the display page for a single locality. The locality ID is the key for this page.
get_header();
echo '<link rel="manifest" href="'.get_template_directory_uri().'/manifest.json">';
//echo do_shortcode('[all_captains]');
  $locality_id = get_the_ID();
  $lat_long= '';
  $title = get_the_title();
  $locality_address = get_post_meta($locality_id,'locality_address',true);
  $locality_marquee = get_post_meta($locality_id,'locality_marquee',true);
  $attachment_id = get_custom_post_meta($locality_id,'locality_banner_pic');
  $banner_image = get_custom_attachment_image($attachment_id,'');
  $locality_data = get_location_object_data_by_type($locality_id,'locality');
  $captains = get_captains_by_localities($locality_id);
  if(isset($locality_data->lat_long)){
    $lat_long = $locality_data->lat_long;
  }
  $add_captain_description = get_option('options_add_captain_description');
  $locality_page_form_shortcode = get_option('options_locality_page_form_shortcode');
  $locality_status = get_custom_post_meta($locality_id,'locality_status');

  if(empty($banner_image) && $banner_image == ''){
    //$banner_image = plugins_url().'/chotu_work/assets/images/default_locality.webp';
    $banner_image = wp_get_attachment_image_url(get_option('options_default_locality_pic'),'banner-image');
  }
  if($locality_status =='coming_soon' && get_custom_attachment_image($attachment_id,'') == ''){
    //$banner_image = plugins_url().'/chotu_work/assets/images/coming_soon_default_locality.webp';
    $banner_image = wp_get_attachment_image_url(get_option('options_coming_soon_default_locality_pic'),'banner-image');
  }
  $favorite = get_favorites_button($locality_id, 0);

$join_as_captain_image = wp_get_attachment_url(get_option('options_add_captain_image'));
$join_as_captain_heading = get_option('options_add_captain_heading');
$locality_share_text = get_option('options_locality_share_text');

//if there are no captains and hence no brands, the locality page displays brands within the vicinity of the locality's lat_long. Hence, we check the lat_long.
  ?>
</style>
  <!-- <div >  </div>  -->
    <div class="position-relative w-100 locality_single_page">
     <?php echo $favorite;?>
      <div type="button"><a href="https://api.whatsapp.com/send/?phone&text=<?php echo $locality_share_text;?>: <?php echo get_permalink($locality_id);?>" target="_blank"><img src="<?php echo plugins_url()?>/chotu_work/assets/images/share_black.svg"class="position-absolute share-button-banner end-0 border-0 filter_white p-3" alt=""></a></div>

      <img class="card-img-top" style="filter: brightness(0.5);" src="<?php echo $banner_image; ?>">
      <div class="caption_banner text-white"><p><?php echo strtoupper($title);?></p><small><?php echo bloginfo('description')?></small></div>

        <div class="inner_page_caption">
          <img src="<?php echo plugins_url()?>/chotu_work/assets/images/location.svg" style="width:1.5rem;" class="filter_white" alt="">
          <span class="caption"><?php echo $locality_address;?></span>
        </div> 
    </div>
<div class="container">
    <div class="text-center pt-3 pb-3">
      <div style="" class="caption text-center mt-3 m-auto mb-3">
        <?php if(isset($locality_marquee) && $locality_marquee !=''){?>
         <marquee class="caption" width="100%" direction="left"><?php echo $locality_marquee ?></marquee>
         
       <?php  } ?>
       <br >
       <?php 
       $locaity_shortcode='';
        echo  get_post_meta($locality_id,'locality_description',true);?>
          <br >
          <br >
          <?php if(empty($captains) || $locality_status =='coming_soon'){
            $desc = get_option('options_coming_soon_locality_description');
            $locaity_shortcode = do_shortcode($locality_page_form_shortcode);
          }else{
            $desc = get_option('options_locality_page_description');
            // $desc = get_post_meta($locality_id,'options_locality_page_description',true);
          }
          ?>
          <?php echo $desc?>
      </div>
    </div>
    <!-- <div class="sm-3"> -->
     <?php echo $locaity_shortcode;
    
   $brands = array();

    if(!empty($captains)){
      $brands = get_brands_by_captains($captains);
    }
    ?>
     <div id="captain_list"></div> <a href="#brands_list" class="pull-right caption text-decoration-none text-primary">What can we buy?</a>
    <?php
   if(!empty($captains) && $locality_status !='coming_soon'){
    echo do_shortcode('[show_captains title="Which captain can I buy from?" locality_id="'.$locality_id.'" brand_id="" lat_long=""]');
      echo '<div id="brands_list"></div> <a href="#captain_list" class="pull-right caption text-decoration-none text-primary">Which captain can I buy from?</a>';
      if(!empty($brands)){
        echo do_shortcode('[show_brands title="What can we buy?" locality_id="'.$locality_id.'" captain_id="" lat_long=""]');
      }else{
        echo do_shortcode('[show_brands title="What can we buy?" locality_id="" captain_id="" lat_long="'.$lat_long.'"]');
      }
    }else{
      
      //the locality has no captain, and hence no brands
      echo do_shortcode('[show_brands title="What can we buy?" locality_id="" captain_id="" lat_long="'.$lat_long.'"]');
      $link = home_url().'/join';
      $footer_text = '<p class=""><small class="caption">'.$add_captain_description.'</small></p>';
      $share_text = chotu_work_card_whatsapp_share('captain',$link);
      echo '<h5>Become a captain</h5>';
      echo chotu_work_make_card($join_as_captain_image,$join_as_captain_heading,$footer_text,'JOIN',$link,'',$share_text,'');
    }
?>
</div>
<?php
get_footer();