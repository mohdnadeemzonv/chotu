<?php 
/* Template Name: My Order History
*/
if(!is_user_logged_in()){
	wp_redirect(home_url());
}
get_header();
global $wpdb;

## ==> Define HERE the customer ID
$customer_user_id = get_current_user_id(); // current user ID here for example
$orders = $wpdb->get_results("SELECT * FROM chotu_orders WHERE customer_ID='$customer_user_id' order by order_date desc");
?>

<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<div class="container">
	<?php the_title( '<h5 class="entry-title">', '</h5>' ); ?>
	<div class="table-responsive">	
	<table id="customer_orders" class="table table-striped">
		<thead>
			<tr>
				<!-- <th> Order ID </th> -->
				<!-- <th> Order Date </th> -->
				<!-- <th> Logo </th> -->
				<th>Brand</th>
				<th>Date</th>
				<!-- <th> Mode Of Payment</th> -->
				<th style="text-align: right;">Value</th>
				<th style="text-align: center;">Bill</th>
				<!-- <th> Actions</th> -->
			</tr>
		</thead>
		<tbody>
			<?php 
			if(!empty($orders)){
				foreach ($orders as $key => $order) {
					// $url = site_url().'/'.trim($order->brand_name).'/my-account/view-order/'.$order->order_id;
					$blog_id =  $order->brand_ID;
					$blog_details = get_blog_details($blog_id);
					$pdf_url = wp_nonce_url($blog_details->siteurl.$blog_details->path.'wp-admin/admin-ajax.php?action=generate_wpo_wcpdf&document_type=invoice&order_ids=' . $order->order_ID . '&my-account', 'generate_wpo_wcpdf' );
						
						$brand_name = get_brand_option($blog_id,'blogname');

					?>
					<tr>
						<td> <?php echo $brand_name;?> </td>
						<td> <?php echo date('d-M',strtotime(get_brand_post_meta($order->order_ID,'date_of_delivery',$blog_details->blog_id)))?> </td>
						<td style="text-align: right;"> <?php echo '₹ '.number_format($order->order_total, 0, ',', ',');?> </td>
						<td style="text-align: center;"><a href="<?php echo esc_attr($pdf_url); ?>" target="_blank"><img src="<?php echo plugins_url()?>/chotu_work/assets/images/pdf-icon.svg"class="order-pdf-icon filter_red" alt=""></a></td>
					</tr>
				<?php }
			}

			?>
		</tbody>
	</tbody>
</table>
</div>
<div id="myModals" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="order_detail">
      
    </div>

  </div>
</div>
</div>
<?php
get_footer();?>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#customer_orders').DataTable(
    { language: {
        searchPlaceholder: "Search orders",
        search: "",
      }
    })
     document.getElementById("customer_orders").deleteTFoot();
     jQuery('#customer_orders_previous a').text('Prev');
		// $('#customer_order').DataTable();

		 // $( document ).on( 'click', '#manage_gallery', upload_gallery_button );
	} );


</script>