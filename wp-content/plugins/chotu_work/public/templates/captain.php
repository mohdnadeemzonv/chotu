<?php
/*
Author Page (CAPTAIN) front-end
*/
    global $wp;
    $current_url = home_url( add_query_arg( array(), $wp->request ) );
    get_header();
    echo '<link rel="manifest" href="'.get_template_directory_uri().'/manifest.json">';
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    if(in_array('captain',$curauth->roles)){
      $phone_number = get_the_author_meta( 'phone_number', $curauth->ID );
    $num_length = strlen($phone_number);
    if($num_length == 10) {
        $phone_number = '91'.$phone_number;
    }
    
    $captain_marquee = get_the_author_meta( 'captain_marquee', $curauth->ID );
    $captain_number_privacy = get_the_author_meta( 'captain_number_privacy', $curauth->ID );
    $captain_address = get_the_author_meta( 'captain_address', $curauth->ID );
    $attachment_id = get_the_author_meta('captain_banner_pic',$curauth->ID );
    $captain_square_pic = get_custom_attachment_image(get_the_author_meta('captain_square_pic',$curauth->ID ));
    $banner_image = get_custom_attachment_image($attachment_id);
    $options_join_my_wa_group = get_option('options_join_my_wa_group');
    if(empty($banner_image) && $banner_image == ''){
      //$banner_image = plugins_url().'/chotu_work/assets/images/teamscreen.png';
      $banner_image = wp_get_attachment_image_url(get_option('options_default_captain_pic'),'banner-image');

    }
    if($captain_square_pic == ''){
      $captain_square_pic = plugins_url().'/chotu_work/assets/images/chotu_logo.png';
    }
  $brands = get_brands_by_captains($curauth->ID);
  $localities = get_locality_by_captain($curauth->ID);
?>
    <!-- <div >  </div>  -->
    <div class="position-relative w-100 captain_single_page">
      <span type="button" class="wrap_share_btn"><a href="https://api.whatsapp.com/send/?phone&text=<?php echo $current_url;?>" target="_blank"><img src="<?php echo plugins_url()?>/chotu_work/assets/images/share_black.svg"class="filter_white position-absolute end-0 border-0 share-button-banner p-3" alt=""></a></span>
      <input type="hidden" name="phone_number" value="<?php echo $phone_number;?>" id="phone_number"> 
      <img class="card-img-top" style="filter: brightness(0.5);" src="<?php echo $banner_image; ?>">
      
      

      <div class="profile_pic">
        <img class="img-circle d-flex m-auto" src="<?php echo $captain_square_pic?>" alt="">
      </div>
      <div class="caption_banner">
        <p><?php echo strtoupper($curauth->nickname)?></p>
            <div class="text-center mgrn_b_cl">    
     
      <div class="caption">  
          <svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" fill="currentColor" class="bi bi-geo-alt-fill"
            viewBox="0 0 16 16">
            <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z" />
          </svg>
          <span class="ml-3"><?php echo $captain_address;?></span>
        </div>
    </div>
      </div>
    </div>


    <div class="container">
    
       <div style="" class=" text-center mt-1 m-auto mb-1 caption captain_dis">
        <?php 
              if(isset($captain_marquee) && $captain_marquee !=''){
                  echo '<marquee class="caption" width="100%" direction="left">'.$captain_marquee.'</marquee>';
              }?>
                <?php echo $curauth->user_description; ?>
                <?php echo do_shortcode($options_join_my_wa_group);?>
      </div>

      
      <div class="all_radio_data_wrap" id="all_radio_data_wrap">
        <div class="row">
         <?php if( is_user_logged_in() ) {
                if($captain_number_privacy){
                    ?>
                  <div class="col-12">
                  <a href="javascript:void(0)" class="caption" id="join_my_whatsapp" style="display: table;text-decoration: none; margin: 1em auto; color: #fff; padding: 0.5em 2em 0.5em 3.5em; border-radius: 2em; background: #25d366 url('https://tochat.be/click-to-chat-directory/css/whatsapp.svg') no-repeat 1.5em center; background-size: 1.6em;">Join my whatsapp group</a>
                </div>

                <?php 
                  }else{?>
                    <div class="col-6 pull-left d-flex">
                    <a href="tel:<?php echo $phone_number;?>" class="caption" style="text-decoration: none; margin: 1em auto; color: #fff; padding: 0.5em 2em 0.5em 3.5em; border-radius: 2em; background: #0e4c75 url('<?php echo plugins_url()?>/chotu_work/assets/images/download.svg') no-repeat 1.5em center; background-size: 1.6em;">me</a>
                  </div>
                   <div class="col-6 pull-right d-flex">
                    <a href="https://wa.me/<?php echo $phone_number;?>/" class="caption" style="text-decoration: none; margin: 1em auto; color: #fff; padding: 0.5em 2em 0.5em 3.5em; border-radius: 2em; background: #25d366 url('https://tochat.be/click-to-chat-directory/css/whatsapp.svg') no-repeat 1.5em center; background-size: 1.6em;">me</a>
                  </div>
                  <?php }
            } else { ?>
                <div class="col-12">
                  <a href="javascript:;" onclick="jQuery('this').digits_login_modal(jQuery(this));return false;" class="login font-family-inter font-weight-normal caption"style="display: table; text-decoration: none; margin: 1em auto; color: #fff; padding: 0.5em 2em 0.5em 3.5em; border-radius: 2em; background: #25d366 url('https://tochat.be/click-to-chat-directory/css/whatsapp.svg') no-repeat 1.5em center; background-size: 1.6em;">Join my whatsapp group</a>
                </div>

           <?php }?>
         </div>
        <p class="text-secondary text-center caption">Please choose a locality to shop from</p>
        <div>
 
<div class="cont-bg">

  <div class="cont-main">
    <div class="row">
      <?php
        if(!empty($localities)){
          $add='';
          // dd($localities);
          $i=0;
            foreach ($localities as $key => $locality) {
              $add='';
              $s_locality = get_post($locality->locality_id);
              if(!empty($s_locality)){
                $locality_status = get_post_meta($locality->locality_id, 'locality_status', true);
                $image = wp_get_attachment_image_url(get_post_meta($locality->locality_id,'locality_banner_pic',true),'banner_pic');
                if(empty($image) && $image == ''){
                  $image = wp_get_attachment_image_url(get_option('options_default_locality_pic'),'banner-image');

                    //$banner_image = plugins_url().'/chotu_work/assets/images/default_locality.webp';
                  }
                  if($locality_status =='coming_soon' && wp_get_attachment_image_url(get_post_meta($locality->locality_id,'locality_banner_pic',true),'banner-image') == ''){
                    //$banner_image = plugins_url().'/chotu_work/assets/images/coming_soon_default_locality.webp';
                    $image = wp_get_attachment_image_url(get_option('options_coming_soon_default_locality_pic'),'banner-image');
                  }
                $gated_locality = get_custom_post_meta($locality->locality_id,'locality_address_syntax',true);
                $address_syntax = json_decode($gated_locality,true);
                if(isset($address_syntax['level_1'])){
                  $add .= '_'.$address_syntax['level_1']['level'].'_: %0a';
                  }
                  if(isset($address_syntax['level_2'])){
                    $add .= '_'.$address_syntax['level_2']['level'].'_: %0a';
                  }
                  if(isset($address_syntax['level_3'])){
                    $add .= $address_syntax['level_3']['level'].': %0a';
                  }
                  /* display only 6 localities commented due to ravi's instructions*/
                // if($i < 6){
                   /* echo'<div class="cont-checkbox">
                      <input class="checkbox_input" type="radio" name="locality_id" id="myRadio-1"  value="'.$locality->locality_id.'" data-title="'.strtoupper(get_the_title($locality->locality_id)).'" data-address="'.$add.'"> <div class="image_radio">
                      <label class="lable_for_check" for="myRadio-1">
                        <img
                          src="'.wp_get_attachment_image_url(get_post_meta($locality->locality_id,'locality_banner_pic',true),'banner_pic').'"/>
                        <span class="cover-checkbox">
                          <svg viewBox="0 0 12 10">
                            <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                          </svg>
                        </span>
                        <div class="info">'.strtoupper(get_the_title($locality->locality_id)).'</div>
                      </label>
                    </div>
                  </div>';*/
                  echo '
                    <div class="col-6">
                    <div class="cont-checkbox">
                      <input class="checkbox_input" type="radio" name="locality_id" id="myRadio-'.$i.'" value="'.$locality->locality_id.'" data-title="'.strtoupper(get_the_title($locality->locality_id)).'" data-address="'.$add.'">
                      <label class="lable_for_check" for="myRadio-'.$i.'">
                        <img src="'.$image.'" >
                        <span class="cover-checkbox">
                          <svg viewBox="0 0 12 10">
                            <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                          </svg>
                        </span>
                        <div class="info">'.strtoupper(get_the_title($locality->locality_id)).'</div>
                      </label>
                    </div>
                    </div>
                    ';


                    // echo ' <div class="col-6"><label class="radio-img"><input type="radio" name="locality_id" value="'.$locality->locality_id.'" data-title="'.strtoupper(get_the_title($locality->locality_id)).'" data-address="'.$add.'"> <div class="image_radio"><img class="img-fluid" src="'.wp_get_attachment_image_url(get_post_meta($locality->locality_id,'locality_banner_pic',true),'banner_pic').'" alt="image" style="aspect-ratio:3/1; object-fit:cover"> <div class="caption_r"><p class="mx-auto caption">'.strtoupper(get_the_title($locality->locality_id)).'</p></div></div></label></div>';
                // }
                $i++;
              }
        //      echo '<input type="radio" name="locality_id" value="'.$locality->locality_id.'" id="locality_'.$locality->locality_id.'">&nbsp;&nbsp;'.get_the_title($locality->locality_id).'&nbsp;&nbsp;';
            }
          }
    ?>
    </div>
</div>
</div>
  </div>
  <br/>
    <h5 class="">
      Top Brands I deliver (choose locality ↑)
    </h5>
    <!-- cards -->
        <?php
        $box_start = '<div class="carousel overflow-hidden hide"><div><div class="bottom-0 position-absolute" style=""></div>';// '';
       $box_end = '</div></div>'; //</div></div>
        echo $box_start;
       echo chotu_work_make_brand_card($brands,'','','',$curauth->ID,'disabled');
        echo $box_end;?>

</div>
<?php
    }else{
       $wp_query->set_404();
      status_header( 404 );
      get_template_part( 404 );
      exit();
    }
get_footer();