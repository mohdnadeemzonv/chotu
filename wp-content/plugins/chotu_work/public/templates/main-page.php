<?php
/*
 * Template Name: Main Site Page 
 * description: >-
  Page template without sidebar
 */
get_header();
echo '<link rel="manifest" href="'.get_template_directory_uri().'/manifest.json">';
?>

    <div class="text-center mb-4 text-white home_bg_img">
      <div class="home_page_baner">
     <?php //echo get_option('blogdescription'); ?>
    </div>
</div>
<?php 
 do_action('chotu_after_main_content');
$page_id = get_the_ID();
global $wpdb;
$join_as_captain_image = wp_get_attachment_url(get_option('options_add_captain_image'));
$join_as_captain_heading = get_option('options_add_captain_heading');
$join_as_captain_description = get_option('options_add_captain_description');
$home_page_description = apply_filters('the_content', get_post_meta($page_id,'home_page_description',true));;
//check lat-long of the page from cookie. if not set, get from IP.
 if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
    $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));

        $lat = $location['lat'];
        $lon = $location['lon'];
    }else{

        $lat = $_COOKIE['lat'];
        $lon = $_COOKIE['lon'];
    }
    $bg_color = '#FAFFDC';
    $lat_long = $lat.','.$lon;
    
    echo '</div>';
    echo '<div class="container">'.$home_page_description.'<p class="caption text-center">
        to find localities near you, please <a class="allow" href="javascript:void(0)" onclick="getLocation()">allow</a> location access
        <span id="demo"></span>
        <div id="locality_list"></div> <a href="#brands_list" class="pull-right caption text-decoration-none text-primary">What can we buy?</a>';
    echo do_shortcode('[show_localities title="Where can we buy from?" brand_id="" captain_id="" lat_long="'.$lat_long.'"]');


echo '<div id="brands_list"></div><a href="#locality_list"  class="pull-right caption text-decoration-none text-primary">Where can we buy from?</a>';
echo do_shortcode('[show_brands title="What can we buy?" locality_id="" captain_id="" lat_long="'.$lat_long.'"]');
$link = home_url().'/join';
$share_text = chotu_work_card_whatsapp_share('captain',$link);
$footer_text = '<p class=""><small class="caption">'.$join_as_captain_description.'</small></p>';
echo '<h5>Become a captain</h5>';
echo chotu_work_make_card($join_as_captain_image,$join_as_captain_heading,$footer_text,'JOIN',$link,'',$share_text,'');
// echo $html;
the_content();
do_action('chotu_work_home_after_content');
echo '</div>';
get_footer();

if(!empty($posts)){
    foreach ($posts as $key => $post) {
        // code...
    }
}
