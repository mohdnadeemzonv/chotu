<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/public
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_work_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_work_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_work_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_work-public.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_work_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_work_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_work-public.min.js', array( 'jquery' ), $this->version, false );

	}
	

/*
	if user tries to login from a network site (brand), we push him to login at central site. After the login at central site, the user should be redirected to the respective newtork site. The following three functions handles this.
*/

/*
	Login modal popup on the header of the central site. A user cannot sign-in from the network site, s/he has to go to the central header to login. this is the only way to login.
*/
	public function chotu_append_login_modal_footer(){
	
		require_once plugin_dir_path( __FILE__ ) . 'partials/chotu_modal_login.php';
	}

	/* set actual location in cookie on clicking allow on the home-page */

	public function get_object_by_actual_location(){
		if(isset($_POST['latitude']) && isset($_POST['longitude'])){
			unset($_COOKIE['lat']);
			unset($_COOKIE['lon']);
			// Clear the cache.
			if ( function_exists( 'rocket_clean_domain' ) ) {
				rocket_clean_domain();
			}
			setcookie('lat', $_POST['latitude'], time() + (31536000), "/");
			setcookie('lon', $_POST['longitude'], time() + (31536000), "/");
		}
  		wp_die();	
	}
}