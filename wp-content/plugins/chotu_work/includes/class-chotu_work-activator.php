<?php
/**
 * Fired during plugin activation
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Chotu_work
 * @subpackage Chotu_work/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_work_Activator {
	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		$chotu_orders = "chotu_orders";
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS $chotu_orders (
		  id int(9) NOT NULL AUTO_INCREMENT,
		  captain_ID int(9) NOT NULL,
		  customer_ID int(9) NOT NULL,
		  brand_ID int(9) NOT NULL,
		  order_ID int(55) NOT NULL,
		  locality_id int(55) NOT NULL,
		  order_date date NOT NULL,
		  order_status varchar(55) DEFAULT '' NOT NULL,
		  mongo_status varchar(55) DEFAULT '' NOT NULL,
		  order_total float(15,2) NOT NULL,
		  PRIMARY KEY  (id)
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
		$chotu_master_locations = "chotu_master_locations";
		$query = "CREATE TABLE IF NOT EXISTS $chotu_master_locations (
		  location_ID int NOT NULL AUTO_INCREMENT,
		  reference_ID int(9) NOT NULL,
		  reference_type varchar(150) NOT NULL,
		  lat_long point NOT NULL,
		  SPATIAL INDEX(lat_long),
		  status int NOT NULL DEFAULT '1',
		  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  PRIMARY KEY  (location_id)
		) $charset_collate;";
		dbDelta( $query );
		
		$chotu_captain_brand_table = "chotu_captain_brand";
		$chotu_captain_brand = "CREATE TABLE IF NOT EXISTS $chotu_captain_brand_table (
		  id int NOT NULL AUTO_INCREMENT,
		  captain_ID int(9) NOT NULL,
		  brand_id int(9) NOT NULL,
		  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $chotu_captain_brand );

		$chotu_captain_locality_table = "chotu_captain_locality";
		$chotu_captain_locality = "CREATE TABLE IF NOT EXISTS $chotu_captain_locality_table (
		  id int NOT NULL AUTO_INCREMENT,
		  captain_ID int(9) NOT NULL,
		  locality_id int(9) NOT NULL,
		  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $chotu_captain_locality );
	}

}
