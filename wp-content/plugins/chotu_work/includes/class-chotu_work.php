<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Chotu_work
 * @subpackage Chotu_work/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_work {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Chotu_work_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CHOTU_WORK_VERSION' ) ) {
			$this->version = CHOTU_WORK_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'chotu_work';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Chotu_work_Loader. Orchestrates the hooks of the plugin.
	 * - Chotu_work_i18n. Defines internationalization functionality.
	 * - Chotu_work_Admin. Defines all hooks for the admin area.
	 * - Chotu_work_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		global $wpdb;
		$blog_id = get_current_blog_id();
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-chotu_work-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-chotu_work-i18n.php';
		require_once plugin_dir_path( dirname( __FILE__ ) )  . 'includes/class-chotu_work_template.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-chotu_work-admin.php';
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		if($blog_id == 1){
    		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/brand/chotu_work_brand_settings.php';
    		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Api/chotu_work_custom_api_endpoints.php';
		}
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/admin_functions.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'functions.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/public_functions.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api/chotu_work_register_rest_routes.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api/chotu_work_captain_apis.php';
		
		
		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-chotu_work-public.php';

		$this->loader = new Chotu_work_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Chotu_work_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Chotu_work_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		global $wpdb;
		$blog_id = get_current_blog_id();
		$plugin_admin = new Chotu_work_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		if($blog_id == 1){
			$this->loader->add_action( 'add_meta_boxes_locality', $plugin_admin,'chotu_meta_box_for_locality' );
			$this->loader->add_action( 'save_post_locality', $plugin_admin,'chotu_meta_box_for_locality_meta_box_save', 10, 2 );
		}
		
		$this->loader->add_action( 'wp_insert_site', $plugin_admin,'chotu_blog_update_option');
		// $this->loader->add_action( 'wp_update_site', $plugin_admin,'chotu_wp_update_site_meta',10,2 );
		$this->loader->add_action( 'edit_user_profile', $plugin_admin, 'chotu_work_user_profile_field');
		$this->loader->add_action( 'show_user_profile', $plugin_admin, 'chotu_work_user_profile_field');
		
		$this->loader->add_action( 'personal_options_update', $plugin_admin,'chotu_save_extra_user_profile_fields' ); // edit profile as logged-in user
		$this->loader->add_action( 'edit_user_profile_update', $plugin_admin,'chotu_save_extra_user_profile_fields' ); // edit profile as wp-admin
		// $this->loader->add_action( 'user_profile_update_errors', $plugin_admin,'chotu_save_extra_user_profile_fields_errors', 10, 3 );

		// $this->loader->add_action( 'save_post_locality', $plugin_admin,'chotu_set_post_default_category', 10,3 );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		global $wpdb;
		$blog_id = get_current_blog_id();
		$plugin_public = new Chotu_work_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		// $this->loader->add_filter( 'wp_nav_menu_items',$plugin_public, 'chotu_add_loginout_link', 10, 2 );
		// $this->loader->add_action( 'init',$plugin_public, 'chotu_wp_redirect_login' );
		// $this->loader->add_filter( 'login_redirect', $plugin_public, 'chotu_sc_after_login_redirection' );
		// $this->loader->add_filter( 'wp', $plugin_public, 'chotu_sc_capture_before_login_page_url' );
		// $this->loader->add_action( 'woocommerce_before_checkout_form', $plugin_public, 'chotu_woocommerce_checkout_login_form_links' );
		

		$this->loader->add_action('wp_ajax_nopriv_get_object_by_actual_location',$plugin_public,'get_object_by_actual_location');
		$this->loader->add_action('wp_ajax_get_object_by_actual_location',$plugin_public,'get_object_by_actual_location');
		if($blog_id == 1){
			$this->loader->add_action( 'wp_footer', $plugin_public, 'chotu_append_login_modal_footer');
		}
		// $this->loader->add_filter( 'post_type_link', $plugin_public,'chotu_remove_team_slug', 10, 3 );
		// $this->loader->add_action( 'pre_get_posts',  $plugin_public,'chotu_parse_request' );

		

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Chotu_work_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
