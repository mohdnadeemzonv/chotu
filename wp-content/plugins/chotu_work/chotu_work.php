<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.zonvoir.com
 * @since             1.0.0
 * @package           Chotu_work
 *
 * @wordpress-plugin
 * Plugin Name:       Chotu Work 
 * Plugin URI:        www.zonvoir.com
 * Description:       This is a central site plugin, which creates the localities, captains and sends referral links to the brand sites.
 * Version:           2.1.9

 * Author:            Mohd Nadeem
 * Author URI:        www.zonvoir.com
 * License:           GPL-2.0+
 * License URI:        www.zonvoir.com
 * Text Domain:       chotu_work
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
/* version change 29 oct 2021*/
define( 'CHOTU_WORK_VERSION', '2.1.9' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-chotu_work-activator.php
 */
function activate_chotu_work() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chotu_work-activator.php';
	Chotu_work_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-chotu_work-deactivator.php
 */
function deactivate_chotu_work() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chotu_work-deactivator.php';
	Chotu_work_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_chotu_work' );
register_deactivation_hook( __FILE__, 'deactivate_chotu_work' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-chotu_work.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_chotu_work() {

	$plugin = new Chotu_work();
	$plugin->run();

}
run_chotu_work();
