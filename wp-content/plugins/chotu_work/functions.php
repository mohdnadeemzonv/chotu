<?php
//set default timezone for central website.
//date_default_timezone_set('Asia/Kolkata');
/*print a variable, Nadeem's fav function*/
function dd($data){
  echo "<pre>";
  print_r($data);
  echo "</pre>";
  die;
}

/*retrieve blog_ID from blogname*/
function get_blog_id_by_blog_path($domain,$path = '/'){
  global $wpdb;
  $blog_id = $wpdb->get_row("SELECT blog_id FROM wp_blogs WHERE `path`='$path'");
  if($blog_id){
    return $blog_id->blog_id;
  }else{
    return false;
  }
}

/*get order/product details from a particular brand. Order/Product are posts in the network site*/
function get_brand_post_meta($post_id,$meta_key,$blog_id){
  global $wpdb;
  $wp_postmeta = $wpdb->base_prefix.$blog_id.'_postmeta';
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND post_id = '".$post_id ."')"); 
}

function load_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_media_files' );

/*update a post (Product/Order) in the blog_ID*/
function update_custom_post_meta($post_id,$blog_id,$meta_key,$meta_value){
  global $wpdb;
  $postmeta = $wpdb->base_prefix.$blog_id.'_postmeta';
  $exist = $wpdb->get_var("SELECT meta_key FROM $postmeta WHERE meta_key = '$meta_key' AND post_id='$post_id'");

  if(isset($exist)){
   $wpdb->query($wpdb->prepare("UPDATE $postmeta SET meta_value='$meta_value' WHERE meta_key='$meta_key' AND post_id=$post_id"));
 }else{
  $wpdb->insert( $postmeta, array('post_id' => $post_id, 'meta_key' => $meta_key, 'meta_value' => $meta_value));

}
return true;
}

/* get custom brand logo url*/
function get_custom_logo_url($blog_id){
  $image_url = '';
  $switched_blog = false;

  if ( is_multisite() && ! empty( $blog_id ) && (int) $blog_id !== get_current_blog_id() ) {
    switch_to_blog( $blog_id );
    $switched_blog = true;
  }
  $custom_logo_id = get_theme_mod( 'custom_logo' );

    // We have a logo. Logo is go.
  if ( $custom_logo_id ) {
    $image = wp_get_attachment_image_src( $custom_logo_id , 'thumb' );
    $image_url = $image[0];
  }

  if ( $switched_blog ) {
    restore_current_blog();
  }

  return $image_url ;
}

/* Return ALL localities without any filter */
function get_localities($post_type = ''){
  global $wpdb;
  $wp_posts = 'wp_posts';
  return $wpdb->get_results("SELECT * FROM $wp_posts WHERE post_type='$post_type' AND post_status='publish'");
}

/* Return all locality details by the ID */
function get_localitity_by_id($post_type = '',$post_id){
  global $wpdb;
  $wp_posts = 'wp_posts';
  return $wpdb->get_row("SELECT * FROM $wp_posts WHERE post_type='$post_type' AND ID='$post_id'");

}

/* when updating the custom chotu_master_locations table, this function checks if the given location/captain/brand exists or not */
function chotu_checkExist($reference_id,$reference_type)
{
  global $wpdb;
  $chotu_master_locations = 'chotu_master_locations';
  $rowcount = $wpdb->get_var("SELECT COUNT(*) FROM $chotu_master_locations WHERE reference_id = '$reference_id' AND reference_type='$reference_type'");
  return $rowcount;
}

/* From custom locations table, retrieves the respective object (locality, brand, captain) for a given lat_long and radius*/
function get_object_by_location($lat_long,$reference_type,$radius){
  global $wpdb;
  $lat_long = str_replace(",", ' ', $lat_long);
  $chotu_master_locations = 'chotu_master_locations';
  return $wpdb->get_results("SELECT * , ST_AsText(`lat_long`), ST_Distance(`lat_long`,ST_GeomFromText( 'POINT(".$lat_long.")' ))* 111.139 as `distance` FROM $chotu_master_locations HAVING `distance` < '$radius' AND reference_type='$reference_type' order by `distance` asc");
}

/*PERSISTENT LOGIN for customers on the front-end*/
add_filter('auth_cookie_expiration', 'chotu_persistent_expiration_filter', 99, 3);
function chotu_persistent_expiration_filter($seconds, $captain_id, $remember){
  $remember = true;
    //if "remember me" is checked;
  if ( $remember ) {
        //WP defaults to 2 weeks;
        $expiration = 365*24*60*60; //UPDATE HERE;
      } else {
        //WP defaults to 48 hrs/2 days;
        $expiration = 2*24*60*60; //UPDATE HERE;
      }

    //http://en.wikipedia.org/wiki/Year_2038_problem
      if ( PHP_INT_MAX - time() < $expiration ) {
        //Fix to a little bit earlier!
        $expiration =  PHP_INT_MAX - time() - 5;
      }

      return $expiration;
    }

    /*change slug for any post type. Here, we change locality slug from /locality to /l */
    function chotu_change_slug( $args, $post_type ) {
     /* item post type slug */   
     if ( 'locality' === $post_type ) {
      $args['rewrite']['slug'] = 'l';
    }
    return $args;
  }
  add_filter( 'register_post_type_args', 'chotu_change_slug', 10, 2 );

  /*check length of pin code for captain and locality custom fields*/
  add_filter('acf/validate_value/name=locality_pin_code', 'chotu_acf_validate_value', 10, 4);
  add_filter('acf/validate_value/name=captain_pin_code', 'chotu_acf_validate_value', 10, 4);

  function chotu_acf_validate_value( $valid, $value, $field, $input ){
  // bail early if value is already invalid
    if( !$valid ) {
      return $valid;
    }
    if ( strlen($value) != 6 ) {

      $valid = 'Must be 6 digits.';
    } 
  // return
    return $valid;
  }

  /* shows all the geography_locality as option in a search select box*/
  function chotu_get_locality_select_input($selected_id){
    $posts = get_posts(array(
      'post_type'   => 'locality',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'fields' => 'ids'
    ));
    if(!empty($posts)){
      foreach($posts as $locality){
        if(get_post_meta($locality,"geo_locality",true) != ''){
          if(isset($selected_id) && $selected_id ==  $locality){
            echo '<option value="'.$locality.'" selected>'.get_post_meta($locality,"geo_locality",true).'</option>';
          }else{
            echo '<option value="'.$locality.'">'.get_post_meta($locality,"geo_locality",true).'</option>';
          }
        }
      }
    }
  }

  /*rewites the author page with the template we have in public/template*/
  add_filter ('template_include', 'chotu_redirect_page_template');
  function chotu_redirect_page_template ($template) {
   $file = '';

   if ( is_author() ) {
        $file   = 'captain.php'; // the name of your custom template
        $find[] = $file;
        $find[] = 'chotu_work/public/templates/' . $file; // name of folder it could be in, in user's theme
      } 
      if ( $file ) {
        $template       = locate_template( array_unique( $find ) );
        if ( ! $template ) { 
            // if not found in theme, will use your plugin version
          $template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/public/templates/' . $file;
        }
      }
      return $template;
    }


    /* Filter the single_template with our custom function*/
    add_filter('single_template', 'chotu_work_locality_post_template');
    function chotu_work_locality_post_template($single) {
      global $post;
      /* Checks for single template by post type */
      if ( $post->post_type == 'locality' ) {
        $file   = 'single-locality.php';
        if ( file_exists( plugin_dir_path( __FILE__ ) ) . '/public/templates/' . $file ) {
          return plugin_dir_path( __FILE__ ) . '/public/templates/' . $file;
        }
      }

      return $single;

    }

    /* Retrieves all the category-subcategory concat as an option in a search-select field*/
    function chotu_get_taxonomy_options($taxonomy,$selected){
      $brand_category =  get_terms( array(
        'taxonomy' => $taxonomy,
        'hide_empty' => false,
      ) );
      if(!empty($brand_category)){
        foreach ($brand_category as $key => $value) {
          if(is_array($selected)){
            if(in_array($value->term_id, $selected)){
              echo '<option value="'.$value->term_id.'" selected>'.$value->name.'</option>';
            }else{
              echo '<option value="'.$value->term_id.'">'.$value->name.'</option>';
            }
          }else{
            if($value->term_id == $selected){
              echo '<option value="'.$value->term_id.'" selected>'.$value->name.'</option>';
            }else{
              echo '<option value="'.$value->term_id.'">'.$value->name.'</option>';
            }
          }
        }
        
      }
    }

    /*updates the custom table chotu_captain_brand with the captain_ID & brands */
    function update_captain_brand_data($captain_id,$brands){
      global $wpdb;
      $b_values = implode(",", $brands);
      $chotu_captain_brand = 'chotu_captain_brand';
      $wpdb->query("DELETE FROM $chotu_captain_brand WHERE captain_ID='$captain_id'" );
      foreach ($brands as $key => $brand) {
        $wpdb->query("INSERT INTO $chotu_captain_brand (brand_id, captain_ID) VALUES ('$brand', '$captain_id')" );
      }
      return true;
    }

    /*updates the custom table chotu_captain_locality with the captain_ID & localities*/
    function update_captain_locality_data($captain_id,$localities){
      global $wpdb;
      $chotu_captain_locality = 'chotu_captain_locality';
      $wpdb->query("DELETE FROM $chotu_captain_locality WHERE captain_ID='$captain_id'" );
      foreach ($localities as $key => $locality) {
        $wpdb->query("INSERT INTO $chotu_captain_locality (locality_id, captain_ID) VALUES ('$locality', '$captain_id')" );
      }
      return true;
    }

    /* delete ALL brand or locality data for a particular captain*/
    function delete_captain_data($captain_id,$type){
      global $wpdb;
      $chotu_captain_brand = 'chotu_captain_brand';
      $chotu_captain_locality = 'chotu_captain_locality';
      if($type =='brand'){
        $wpdb->query("DELETE FROM $chotu_captain_brand WHERE  captain_ID='$captain_id'" );
      }
      else if($type =='locality'){
        $wpdb->query("DELETE FROM $chotu_captain_locality WHERE  captain_ID='$captain_id'" );
      }
    }


    /*For creating a brand card, this function makes the delivery_date, time_left_to_order and sorts the brand cards by the time_left_to_order (low to high)*/
    function chotu_work_make_brand_card($brands,$carousel_start,$end_carousel,$locality_id,$captain_id,$class){
      if(get_post_type() == 'locality' && is_singular() == 'locality'){
        $locality_id = get_the_ID();
      }
      
  //is_singular
      $add_brand_image = wp_get_attachment_url(get_option('options_add_brand_image'));
      $add_brand_heading = get_option('options_add_brand_heading');
      $add_brand_description = get_option('options_add_brand_description');
      $banner_image = '';
      $html ='';
      if(!empty($brands)){
        $sort_brand = array();
        $i=0;
        foreach ($brands as $key => $brand) {
          $delivery_order_cycle = get_delivery_date_and_cut_off_time($brand->brand_id,'','');
          if(isset($delivery_order_cycle->delivery_date)){
            $sort_brand[$i]['brand_id'] = $brand->brand_id;
            $sort_brand[$i]['delivery_date'] = $delivery_order_cycle->delivery_date;
            $sort_brand[$i]['order_before_time'] = $delivery_order_cycle->order_before_time;
            $i++;
          }
        }
        if(!empty($sort_brand)) {
          array_multisort(array_column($sort_brand, 'order_before_time'), SORT_ASC, $sort_brand);
          foreach ($sort_brand as $kes => $brand) {
           $html .= $carousel_start;
           $blog = get_blog_details($brand['brand_id']);
           $blogdescription = get_brand_option($brand['brand_id'],'blogdescription');
            // $blog_desc = strlen($blogdescription) > 50 ? substr($blogdescription,0,50).'...' : $blogdescription.'...';
           $blog_desc = htmlspecialchars_decode(get_site_meta($brand['brand_id'],'brand_card_promo_text','single'));
           $button_text = 'View';
           $url='';
           if(isset($locality_id) && $locality_id !='' || $captain_id){
            $url = display_brands_with_url_param($brand['brand_id'],$locality_id,$captain_id);
          }else{
            $url = $blog->siteurl;
          }
          
          $banner_pic_of_brand = unserialize(get_banner_pic_of_brand($brand['brand_id'],'banner_pic'));
          if(!empty($banner_pic_of_brand)){
            $banner_image = $banner_pic_of_brand['banner_pic'];
          }
          $delivery_order_cycle = get_delivery_date_and_cut_off_time($brand['brand_id'],'','');
          $delivery_date = '';
          if(!empty($delivery_order_cycle)){
            $delivery_date = $delivery_order_cycle->delivery_date;
            $order_before_time = $delivery_order_cycle->order_before_time;
          }

          $footer_text ='<p class="" style="text-overflow: ellipsis;-webkit-line-clamp: 1;white-space: nowrap;overflow: hidden;width: 100%;"><small class="caption">'.$blog_desc.'<br /></p><p class="order_before_time_card" data-card="'.$order_before_time.'"><img src="'.plugins_url().'/chotu_work/assets/images/local_shipping_black.svg" alt="">  '.date('j M, D',strtotime($delivery_date)).' | <img src="'.plugins_url().'/chotu_work/assets/images/timer_black.svg" alt=""> <span class="timer"> </span></small></p>';
          $share_text = chotu_work_card_whatsapp_share('brand',$url);
          $html .=chotu_work_make_card($banner_image,$blog->blogname,$footer_text,$button_text,$url,$class,$share_text,'');
          //echo '<div class="captain"><h3><a href="'.$blog->siteurl.'">'.$blog->blogname.'</a></h3>';
          $html .=$end_carousel;
          if( $kes == array_key_last($sort_brand)) {
            $link = home_url().'/recommend-a-brand/';
            $share_text = chotu_work_card_whatsapp_share('brand',$link);
            $footer_text = '<p class="" style="text-overflow: ellipsis;"><small class="caption">'.$add_brand_description.'</small></p>';
            $html .= chotu_work_make_card($add_brand_image,$add_brand_heading,$footer_text,'REFER',$link,'',$share_text,'');
          }
        }
      }else{
        
        $link = home_url().'/recommend-a-brand/';
        $share_text = chotu_work_card_whatsapp_share('brand',$link);
        $footer_text = '<p class=""><small class="caption">'.$add_brand_description.'</small></p>';
        $html .= chotu_work_make_card($add_brand_image,$add_brand_heading,$footer_text,'REFER',$link,'',$share_text,'');
      }  
    }
    return $html; 
  }

  /* Ala instagram posts, this is ONE card-maker in the entire portal. Earlier, we wanted to have share & fav button, hence they are retained.*/
function chotu_work_make_card($banner_image,$header_text,$footer,$button_text,$button_url,$class,$share_text,$favorite){//width: 25rem;
  $html = '';
  if($banner_image == ''){
    $banner_image = home_url().'/wp-content/uploads/2021/07/other-small.jpg';
  }
  $share_text ='';
  if($button_text == ''){
    $button_text = 'click';
  }
  $html .='<div class="card mb-4 mx-auto share_brand_btn"><a class="share_inactive" href="https://api.whatsapp.com/send/?phone&text='.$share_text.':"><img src="'.plugins_url().'/chotu_work/assets/images/share_black.svg"class="position-absolute share-button-banner end-0 border-0 filter_blue p-3" alt=""></a><a class="url_param" href="'.$button_url.'"><div class="image_container"><img class="card-img-top-cl" src="'.$banner_image.'" alt="..."></div></a><div class="card-body"><div class="wrap_text_btn_cl"><h6 class="card-title">'.strtoupper(substr($header_text,0,40)).'</h6><a href="'.$button_url.'" style="display:none;" class="button btn btn-primary btn-sm border-0  end-0  '.$class.'">'.$button_text.'</a></div>
  '.$footer.'</div></div>';
  return $html;
}

/*retrieve the banner pic of the brand*/
function get_banner_pic_of_brand($brand_id,$key_name){
  global $wpdb;
  $wp_options = $wpdb->base_prefix.$brand_id.'_options';
  return $wpdb->get_var("SELECT option_value FROM $wp_options WHERE `option_value` LIKE '%$key_name%' ORDER by `option_id` DESC" );
}

/*retrieve the specific option of the brand*/
function get_brand_option($brand_id,$option){
  global $wpdb;
  $wp_options = $wpdb->base_prefix.$brand_id.'_options';
  return $wpdb->get_var("SELECT option_value FROM $wp_options WHERE option_name ='$option'");
  
}

/*short code to show all localities. 1 out of 3 params needs to be given: captain_id, brand_id, lat_long*/
add_shortcode('show_localities','chotu_work_show_localities');
function chotu_work_show_localities($attrs){
  $html ='';
  $box_start='';
  $box_end='';
  if(isset($attrs['title'])){
    $title = $attrs['title'];
  }
  $brand_id_set =0; $captain_id_set =0; $lat_long_set =0;
  $locality_id ='';
  $captain_id = '';
  $lat_long = '';
  $localities='';
  $add_locality_image = wp_get_attachment_url(get_option('options_add_locality_image'));
  $add_locality_heading = get_option('options_add_locality_heading');
  $add_locality_description = get_option('options_add_locality_description');
  if(isset($attrs['brand_id']) && $attrs['brand_id'] !=''){
    $brand_id = $attrs['brand_id'];
    $localities = get_localities_by_brand($brand_id);
    $brand_id_set = 1;
  }
  if(isset($attrs['captain_id']) && $attrs['captain_id'] !=''){
    $captain_id = $attrs['captain_id'];
    $localities = get_locality_by_captain($captain_id);
    $captain_id_set = 1;
  }
  if(isset($attrs['lat_long']) && $attrs['lat_long'] !=''){
    $lat_long = $attrs['lat_long'];
    $lat_long_set = 1;
    $location_radius =  get_option('options_location_radius');
    $localities =  get_object_by_location($lat_long,'locality',$location_radius);
  }

  if($brand_id_set+$captain_id_set+$lat_long_set > 1){
    echo '<div id="message" class="error"><p>Something went wrong.</p></div>';
    return ;
  }
  $bg_color = '#FAFFDC';
  $html .='<h5>'.$title.'</h5>';
     // $brand .='<div class="carousel overflow-hidden" data-flickity="{"freeScroll":true, "autoPlay":false}">';

  if(!empty($localities)){
    $i=0;
    foreach ($localities as $key => $locality) {
      if(isset($locality->reference_ID)){
        $locality_id = $locality->reference_ID;
      }else{
        $locality_id = $locality->locality_id;
      }
      $s_locality = get_post($locality_id);

      if(!empty($s_locality)){
        if($s_locality->post_status == 'publish'){
          $locality_status = get_custom_post_meta($locality_id,'locality_status');
          $url_link = get_permalink($locality_id);
          $footer_text = '';
          $locality_card_promo_text = get_custom_post_meta($locality_id,'locality_card_promo_text');
          $hide = '';
          $button = '';
          if($i > 4){
            $hide = 'hide';
            $button .= '<div class="text-center load_more"><a href="javascript:void(0)" class="button btn btn-primary end-0 mb-3" id="load_more">load more</a></div>';
          }
          $box_start = '<div class="carousel overflow-hidden '.$hide.'">
            <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 " style="background: '.$bg_color.'; width:100%; "></div>';// '';
            $box_end = '</div></div>';
            $footer_text = '<p class=""><small class="caption">'. $locality_card_promo_text.'</small></p>';
            $button_text = 'View';
            
            $attachment_id = get_custom_post_meta($locality_id,'locality_banner_pic');
            $banner_image = wp_get_attachment_image_url($attachment_id,'banner-image');
            if(empty($banner_image) && $banner_image == ''){
              $banner_image = wp_get_attachment_image_url(get_option('options_default_locality_pic'),'banner-image');

                //$banner_image = plugins_url().'/chotu_work/assets/images/default_locality.webp';
            }
            if($locality_status =='coming_soon' && wp_get_attachment_image_url($attachment_id,'banner-image') == ''){
                //$banner_image = plugins_url().'/chotu_work/assets/images/coming_soon_default_locality.webp';
              $banner_image = wp_get_attachment_image_url(get_option('options_coming_soon_default_locality_pic'),'banner-image');
            }
            if($locality_status == 'coming_soon'){
              $button_text = 'REFER';
              $footer_text = '<p class=""><small class="caption">'.$locality_card_promo_text.'</small></p>';
              $url_link = $url_link;
            }
            if($locality_status != 'paused'){
              $favorite = get_favorites_button($locality_id, 0);
              $share_text = chotu_work_card_whatsapp_share('locality',get_permalink( $locality_id));
              
              
                // $html .='<div class="carousel-item"><div class="row"><div class="col-xs-4 mb-3">';
              $html .=$box_start;
              $html .= chotu_work_make_card($banner_image,$s_locality->post_title,$footer_text,$button_text,$url_link,'',$share_text,$favorite); 
              $html .=$box_end;
                // $html .='</div></div></div>';
              if( $key  === array_key_last($localities)) {
                $link = home_url().'/pull';
                $share_text = chotu_work_card_whatsapp_share('locality',$link);
                $html .=$box_start;
                $class = '';
                $footer_text = '<p class=""><small class="caption">'.$add_locality_description.'</small></p>';
                $html .= chotu_work_make_card($add_locality_image,$add_locality_heading,$footer_text,'ADD',$link,$class,$share_text,'');
                $html .=$box_end;
              } 
            }
            $i++;
          }
        }
        
      }
    }else{
      $link = home_url().'/pull';
      $html .=$box_start;
           $share_text = chotu_work_card_whatsapp_share('locality',$link); //come from wp_options
           $class = '';
           $footer_text = '<p class=""><small class="caption">'.$add_locality_description.'</small></p>';
           $html .= chotu_work_make_card($add_locality_image,$add_locality_heading,$footer_text,'ADD',$link,$class,$share_text,'');
           $html .=$box_end;

         }
         if(isset($button)){
          $html .=$button;
        }
        // $brand .= '</div>';
        return $html;
      }

      /*short code to show all brands. 1 out of 3 params needs to be given: captain_id, locality_id, lat_long*/
      add_shortcode('show_brands','chotu_work_show_brands');
      function chotu_work_show_brands($attrs){
        //dd($attrs);
        if(isset($attrs['title'])){
          $title = $attrs['title'];
        }
        $add_brand_image = wp_get_attachment_url(get_option('options_add_brand_image'));
        $add_brand_heading = get_option('options_add_brand_heading');
        $add_brand_description = get_option('options_add_brand_description');
        $locality_id_set =0; $captain_id_set =0; $lat_long_set =0;
        $locality_id='';
        $captain_id = '';
        $lat_long = '';
        $captains='';

        if(isset($attrs['locality_id']) && $attrs['locality_id'] !=''){
          $locality_id = $attrs['locality_id'];
          $captains = get_captains_by_localities($locality_id);
          $locality_id_set = 1;
        }
        if(isset($attrs['captain_id']) && $attrs['captain_id'] !=''){
          $captain_id = $attrs['captain_id'];
          $captains = $captain_id;
          $captain_id_set = 1;
        }
        if(isset($attrs['lat_long']) && $attrs['lat_long'] !=''){
          $lat_long = $attrs['lat_long'];
          $lat_long_set = 1;
          $lat_long = $lat_long;

        }
        if($locality_id_set+$captain_id_set+$lat_long_set > 1){
          echo '<div id="message" class="error"><p>Something went wrong.</p></div>';
          return ;
        }
        $bg_color = '';
        $html  = '';
        $html .='<h5>'.$title.'</h5>';
        
        $captain_id = '';
        $delivery_date='';
        $super_captain = get_users( array( 'role__in' => array( 'super-captain' ) ) );
        if(!empty($super_captain)){
          $captain_id =$super_captain[0]->ID;
        //display_brands_with_url_param($brand->brand_id,$locality_id,$captain_id);
        }
        $box_start = '<div class="carousel overflow-hidden">
      <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 " style="background: '.$bg_color.'; width:100%; "></div>';// '';
       $box_end = '</div></div>'; //
       if(empty($captains) || $captains ==''){
        $location_radius =  get_option('options_location_radius')*5;
        $locality_by_lat_long = get_object_by_location($lat_long,'locality',$location_radius);
        $locality_id_array = chotu_work_change_key($locality_by_lat_long,'reference_ID','locality_id');
        $locality_geo = get_geography_by_locality($locality_id_array);
        $brands = get_brand_by_geography($locality_geo);
        if(!empty($brands)){
          $html .= $box_start;
          $html .= chotu_work_make_brand_card($brands,'','',$locality_id,$captain_id,'');
          $html .=  $box_end; 
        }else{
          $link = home_url().'/recommend-a-brand/';
          $share_text = chotu_work_card_whatsapp_share('brand',$link);
          $footer_text = '<p class=""><small class="caption">'.$add_brand_description.'</small></p>';
          $html .= chotu_work_make_card($add_brand_image,$add_brand_heading,$footer_text,'REFER',$link,'',$share_text,'');
        }
      }else{
        $captain_brands = get_brands_by_captains($captains);
        $locality_id_array = array(array('locality_id' => $locality_id));
        $locality_geo = get_geography_by_locality($locality_id_array);
        $geo_brands = get_brand_by_geography($locality_geo);
        $brands = array_merge($captain_brands,$geo_brands);
        if(!empty($brands)){
         $html .=  $box_start;
         $html .= chotu_work_make_brand_card($brands,'','',$locality_id,$captain_id,'');
         $html .=  $box_end;     
       }
       else{
        $link = home_url().'/recommend-a-brand/';
        $share_text = chotu_work_card_whatsapp_share('brand',$link);
        $footer_text = '<p class=""><small class="caption">'.$add_brand_description.'</small></p>';
        $html .= chotu_work_make_card($add_brand_image,$add_brand_heading,$footer_text,'REFER',$link,'',$share_text,'');
      }
    }
    return $html;
  }

  /*short code to show all captains. 1 out of 3 params needs to be given: brand_id, locality_id, lat_long*/
  add_shortcode('show_captains','chotu_work_show_captains');
  function chotu_work_show_captains($attrs){
    $bg_color = '';
    $html ='';
    $locality_id_set =0; $brand_id_set =0; $lat_long_set =0;
    $locality_id='';
    $captain_id = '';
    $lat_long = '';
    $captains='';
    $add_captain_image = wp_get_attachment_url(get_option('options_add_captain_image'));
    $add_captain_heading = get_option('options_add_captain_heading');
    $add_captain_description = get_option('options_add_captain_description');
    if(isset($attrs['title'])){
      $title = $attrs['title'];
    }
    
    if(isset($attrs['locality_id']) && $attrs['locality_id'] !=''){
      $locality_id = $attrs['locality_id'];
      $captains = get_captains_by_localities($locality_id);
      $locality_id_set = 1;
    }
    if(isset($attrs['brand_id']) && $attrs['brand_id'] !=''){
      $brand_id = $attrs['brand_id'];
      $captains = get_captain_by_brand($brand_id);
      $brand_id_set = 1;
    }
    if(isset($attrs['lat_long']) && $attrs['lat_long'] !=''){
      $lat_long = $attrs['lat_long'];
      $lat_long_set = 1;
      $location_radius =  get_option('options_location_radius');
      $captains = get_object_by_location($lat_long,'captain',$location_radius);
    }

    if($locality_id_set+$brand_id_set+$lat_long_set > 1){
      echo '<div id="message" class="error"><p>Something went wrong.</p></div>';
      return ;
    }
    $captain_id = '';
    $delivery_date='';
    $super_captain = get_users( array( 'role__in' => array( 'super-captain' ) ) );
    if(!empty($super_captain)){
      $captain_id =$super_captain[0]->ID;
        //display_brands_with_url_param($brand->brand_id,$locality_id,$captain_id);
    }
    $box_start = '<h5 class="">'.$title.'</h5><div class="carousel overflow-hidden ">
      <div style=" width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 " style="background: '.$bg_color.'; width:100%; "></div>';// '';
      $box_end = '</div></div>';
      $html .=  $box_start;
      
      foreach ($captains as $key => $captain) {
        $captain_ID  = $captain->captain_ID;
        $user = get_user_by( 'id', $captain->captain_ID );
        $captain_card_promo_text = get_the_author_meta('captain_card_promo_text',$captain_ID );
        if(isset($user) && $user !='' && in_array('captain',$user->roles)){
          $attachment_id = get_the_author_meta('captain_banner_pic',$captain_ID );
    // $captain_rating = get_the_author_meta('captain_rating',$captain_ID );
          $order_count = get_order_count_by_captain($captain_ID);
          $banner_image = get_custom_attachment_image($attachment_id);
          $footer_text = '<p class=""><small class="caption">'.$captain_card_promo_text.'</small></p>';
          if(empty($banner_image) && $banner_image == ''){
      //$banner_image = plugins_url().'/chotu_work/assets/images/teamscreen.png';
            $banner_image = get_option('options_coming_soon_default_brand_pic');
          }

          $url = get_author_posts_url($captain_ID).'?l='.$locality_id;
          $share_text = chotu_work_card_whatsapp_share('captain',$url);
          $html .=chotu_work_make_card($banner_image,$user->display_name,$footer_text,'View',$url,'',$share_text,'');
          if( $key  === array_key_last($captains)) {
            $link = home_url().'/join';
            $share_text = chotu_work_card_whatsapp_share('captain',$link);
            $footer_text = '<p class=""><small class="caption">'.$add_captain_description.'</small></p>';
            $html .= chotu_work_make_card($add_captain_image,$add_captain_heading,$footer_text,'JOIN',$link,'',$share_text,'');
            
          }

        }
        
      }
      $html .=  $box_end;
      return  $html;
    }

    /*show a different menu for logged and logged-out users*/
    function chotu_work_wp_nav_menu_args( $args = '' ) {
      if( is_user_logged_in() ) { 
        $args['menu'] = 'logged-in';
      } else { 
        $args['menu'] = 'logged-out';
      } 
      return $args;
    }
    add_filter( 'wp_nav_menu_args', 'chotu_work_wp_nav_menu_args' );

    /*remove the black admin bar for logged in users, show for administrators only*/
    add_action('after_setup_theme', 'chotu_work_remove_admin_bar');
    function chotu_work_remove_admin_bar() {
      if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
      }
    }
    add_filter( 'logout_redirect', function() {
      return esc_url( home_url() );
    } );

    /*Programatically populate the OG:meta tags for different pages*/
    add_action('wp_head','chotu_work_og_meta_details');
    function chotu_work_og_meta_details(){
      $blog_id = get_current_blog_id();
      if ( is_single() && 'locality' == get_post_type()  && ! is_front_page()) {
        $locality_id = get_the_ID();
        $location = explode(",", get_post_meta($locality_id,'locality_lat_long',true)) ;

        ?>
        <title><?php echo the_title()?>  &#8211; chotu</title>
        <meta property="og:title" content="<?php echo the_title()?>"/>
        <meta property="og:description" content="<?php echo get_post_meta($locality_id,'locality_description',true);?>-let’s buy together"/>
        <meta property="og:url" content="<?php echo get_permalink($locality_id)?>"/>
        <meta property="og:image" content="<?php echo wp_get_attachment_image_url(get_post_meta($locality_id,'locality_banner_pic',true),'full');?>"/>
        <meta property="place:location:longitude" content = "<?php echo $location[0];?>">
        <meta property="place:location:latitude" content = "<?php echo $location[1] ?>">

      <?php }
      if(is_author()){
        global $wp;
        $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
        $captain_id = $author->ID;
        if(in_array('captain',$author->roles)){
          $current_url = home_url( add_query_arg( array(), $wp->request ) );
          $location = explode(",", get_user_meta($captain_id,'captain_lat_long',true));
          ?>
          <title><?php echo get_user_meta($captain_id,'first_name',true)?>  &#8211; chotu</title>
          <meta property="og:title" content="<?php echo get_user_meta($captain_id,'first_name',true)?>"/>
          <meta property="og:description" content="<?php echo $author->user_description;?>"/>
          <meta property="og:url" content="<?php echo $current_url?>"/>
          <meta property="og:image" content="<?php echo wp_get_attachment_image_url(get_user_meta($captain_id,'captain_banner_pic',true),'full');?>"/>
          <meta property="place:location:longitude" content = "<?php echo $location[0];?>">
          <meta property="place:location:latitude" content = "<?php echo $location[1] ?>">
        <?php }

      }
      if(is_front_page() || is_home()){ if($blog_id == 1){?>
        <meta property="og:title" content="<?php echo get_field('og_heading',get_the_ID());?>"/>
        <meta property="og:description" content="<?php echo get_field('og_description',get_the_ID());?>"/>
        <meta property="og:url" content="<?php echo home_url();?>"/>
        <meta property="og:image" content="<?php echo wp_get_attachment_image_url(get_post_meta(get_the_ID(),'og_image',true),'thumbnail');?>"/>
      <?php } }
    }

    /*change key name. When getting reference_ID from chotu_custom_locations, we need to change this to brand_ID, captain_ID or locality_ID. this functions precisely does this.*/
    function chotu_work_change_key( $array, $old_key, $new_key ) {
    // $data =  new stdClass();
      $data = array();
      foreach ($array as $key => $value) {
        $obj = [];
        $obj[$new_key] = $value->$old_key; 
        $data[$key] = (object) $obj;
      }
      return $data;
    } 
    add_action('wp_logout','chotu_work_auto_redirect_after_logout');

    /*after logout, redirect the user to the home page*/
    function chotu_work_auto_redirect_after_logout(){
      wp_safe_redirect( home_url() );
      exit;
    }

    /*change menu on logout*/
    function chotu_work_logout($items){
      foreach($items as $item){
        if( $item->title == "Logout"){
         $item->url = $item->url . "&_wpnonce=" . wp_create_nonce( 'log-out' );
       }
     }
     return $items;
   }
   add_filter('wp_nav_menu_objects', 'chotu_work_logout');

/*
function get_short_captain_by_order_and_rating($captains){
  foreach ($captains as $key => $captain) {
    $captain_ID ='';
    if(isset($captain->reference_ID)){
      $captain_ID = $captain->reference_ID;
    }else{
        $captain_ID = $captain->captain_ID;
    }
    // $captain_rating = get_the_author_meta('captain_rating',$captain_ID );
    //$order_count = get_order_count_by_captain($captain_ID);
    $captains[$key]->captain_rating=get_the_author_meta('captain_rating',$captain_ID );;
    $captains[$key]->order_count=get_order_count_by_captain($captain_ID);

  }
  array_multisort(array_column($captains, 'order_count'), SORT_DESC, $captains);
  return $captains;
}
if(function_exists('get_delivery_date_and_cut_off_time')){
    function get_delivery_date_and_cut_off_time($brand_id,$locality_id,$captain_id){//$brand_id,$locality_id,$captain_id
    global $wpdb;
    $blogmeta = $wpdb->base_prefix.'blogmeta';
    $next_delivery_date = date('Y-m-d H:i');
    $order_before_time_sql = "(SELECT CONCAT(meta_value,' ',(SELECT meta_value from wp_blogmeta WHERE  meta_key = 'brand_cut_off_time' AND  `blog_id` = '$brand_id')) as date_and_time from wp_blogmeta WHERE meta_key = 'brand_order_cycle' AND  `blog_id` = '$brand_id' HAVING date_and_time >= '$next_delivery_date' order by date_and_time ASC limit 1)";
  return $wpdb->get_row("SELECT DATE_ADD($order_before_time_sql, INTERVAL (SELECT meta_value from wp_blogmeta WHERE  meta_key = 'brand_lead_time' AND  `blog_id` = '$brand_id') DAY) as delivery_date,$order_before_time_sql as order_before_time");
  }
}*/

add_action('acf/init', 'my_acf_op_init');

function my_acf_op_init() {

    // Check function exists.
  if( function_exists('acf_add_options_page') ) {

        // Register options page.
    $option_page = acf_add_options_page(array(
      'page_title'    => __('Central Site Settings'),
      'menu_title'    => __('Central Site Settings'),
      'menu_slug'     => 'central-site-settings',
      'capability'    => 'edit_posts',
      'redirect'      => false
    ));
       /*  add_menu_page( 
        __( 'Global Product Categories', 'chotu' ),
          'Global Product Categories',
          'manage_options',
          'edit-tags.php?taxonomy=pr_cat&post_type=product_category',
          '',
          'dashicons-universal-access-alt',
          6
        );*/
      }
    }

    /*return share button text in card - not used in card but in locality & captain pages*/
    function chotu_work_card_whatsapp_share($share_type,$share_link){
      $share_text = '';
      if($share_type == 'captain'){
        $share_text = get_option('options_captain_share_text');
      }
      if($share_type == 'brand'){
        $share_text = get_option('options_brand_share_text');
      }
      if($share_type == 'locality'){
        $share_text = get_option('options_locality_share_text');
      }
      return $share_text;
    }

    /*add delivery date to PDF invoice*/
    add_action('wpo_wcpdf_after_order_data','chotu_work_add_expected_date_in_pdf_invoice',10,2);

    function chotu_work_add_expected_date_in_pdf_invoice($type,$order){
      $blog_id = get_current_blog_id();
      if(isset($_GET['order_ids'])){?>
        <tr class="expected-delivery-date">
          <th><?php _e( 'Delivery Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
          <td><?php echo date('d-M, D',strtotime(get_post_meta($_GET['order_ids'],'date_of_delivery',true)));?></td>
        </tr>
      <?php }
    }

  add_action('wpo_wcpdf_before_order_details','chotu_work_wpo_wcpdf_before_order_details',10,2);
  function chotu_work_wpo_wcpdf_before_order_details($type,$order){
      $order_id  = $order->get_id();
      $captain_id = get_post_meta($order_id,'captain_id',true);
      $location_table_data = get_location_object_data_by_type(get_post_meta($order_id,'locality_id',true),'locality');
      $user = new WP_User($captain_id);
      $locality = get_localitity_by_id('locality',get_post_meta($order_id,'locality_id',true));
      $locality_name = $locality->post_title ?  $locality->post_title: '';
      $locality_address = $location_table_data->full_address ?  $location_table_data->full_address: '';
      $user_name = $user->display_name ? $user->display_name :'';
      $level_label_1 =  get_post_meta($order_id,'level_label_1',true);
      $level_label_2 =  get_post_meta($order_id,'level_label_2',true);
      $level_label_3 =  get_post_meta($order_id,'level_label_3',true);
      ?>
      <h2 style="color:#96588a;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
      Customer Details
      </h2>
    <div style="margin-bottom:40px">
     <table cellspacing="0" cellpadding="6" border="1" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;width:100%;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">  
        <tbody>
           <tr>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
                <strong>Address: </strong>
              </td>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
                <?php if($level_label_1 != 'N/A' && $level_label_1!=''){
                    echo '<strong>'.$level_label_1 .'</strong>: '. get_post_meta($order_id,'level_1',true).'<br />';
                   }
                   if($level_label_2 != 'N/A' && $level_label_2!=''){
                    echo '<strong>'.$level_label_2 .'</strong>: '. get_post_meta($order_id,'level_2',true).'<br />';
                   }
                   if($level_label_3 != 'N/A' && $level_label_3!=''){
                    echo '<strong>'.$level_label_3 .'</strong>: '. get_post_meta($order_id,'level_3',true);
                   }?>    
              </td>
           </tr>
            <tr>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
                <strong>Locality Name: </strong>
              </td>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
                 <?php echo $locality_name;?>   
              </td>
           </tr>
             <tr>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
                <strong>Locality Address: </strong>
              </td>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
                 <?php echo $locality_address;?>   
              </td>
           </tr>
            <tr>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
                <strong>Delivery Date: </strong>
              </td>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
                 <?php echo date('j M, D',strtotime(get_post_meta($order_id,'date_of_delivery',true)));?>   
              </td>
           </tr>
            <tr>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
                <strong>Captain Name: </strong>
              </td>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
                 <?php echo $user_name;?>   
              </td>
           </tr>
           <tr>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
                <strong>Captain Phone Number: </strong>
              </td>
              <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
                 <?php echo get_custom_user_meta($captain_id,'phone_number');?>   
              </td>
           </tr>
          
        </tbody>
       
     </table>
  </div>
    <?php }

    /* add shortcode for show favorite locality as card in my favorite page*/

    add_shortcode('show_favorites','chotu_work_show_favorite_by_logged_in_user');
    function chotu_work_show_favorite_by_logged_in_user(){
      $favourites = get_user_favorites($user_id = null, $site_id = 0, $filters = null);
      if(is_user_logged_in()){
        $bg_color = '#FAFFDC';
        if(!empty($favourites)){
          $box_start = '<div class="carousel overflow-hidden ">
            <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 " style="background: '.$bg_color.'; width:100%; "></div>';// '';
            $box_end = '</div></div>';
            foreach ($favourites as $key => $locality_id) {
              $s_locality = get_post($locality_id);
              $footer_text = '<p class=""><small class="caption">'. get_custom_post_meta($locality_id,'locality_description').'</small></p>';

              $button_text = 'View';
              $attachment_id = get_custom_post_meta($locality_id,'locality_banner_pic');
              $banner_image = wp_get_attachment_image_url($attachment_id,'banner-image');
              $share_text = chotu_work_card_whatsapp_share('locality',get_permalink( $locality_id));
              echo $box_start;
              echo chotu_work_make_card($banner_image,$s_locality->post_title,$footer_text,$button_text,get_permalink( $locality_id),'',$share_text,'');
              echo $box_end;
            }
          }else{
            echo '<p class="caption text-center">Add favorites by clicking on &#10084;.</p><hr>';
          }
        }else{
          echo '<p class="caption text-center">Please <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="h-100 login font-family-inter font-weight-normal">login</a> to add favorites.</p><hr>';
        }
      }

      /* remove edit lock in localty post type*/
// function chotu_work_remove_post_locked() {
//     $current_post_type = get_current_screen()->post_type;   

//     // Disable locking for page, post and some custom post type
//     $post_types_arr = array(
//         'locality',
//     );

//     if(in_array($current_post_type, $post_types_arr)) {
//         add_filter( 'show_post_locked_dialog', '__return_false' );
//         add_filter( 'wp_check_post_lock_window', '__return_false' );
//         wp_deregister_script('heartbeat');
//     }
// }

// add_action('load-edit.php', 'chotu_work_remove_post_locked');
// add_action('load-post.php', 'chotu_work_remove_post_locked');
      /* for session */
      function chotu_work_curl_before_request($curlhandle){
        session_write_close();
      }
      add_action( 'requests-curl.before_request','chotu_work_curl_before_request', 9999 );
      /* this function return brand meta value by meta key*/
      function get_site_meta_of_taxonomy($blog_id,$meta_key){
        global $wpdb;
        return $wpdb->get_results("SELECT meta_value as cat FROM `wp_blogmeta` WHERE `blog_id` = $blog_id AND `meta_key` LIKE '%$meta_key%'");
      }
      /* this function update brand meta value by meta key*/
      function delete_blog_meta_category($blog_id,$cats,$meta_key){
        global $wpdb;
        
        if(!empty($cats)){
          $cats = implode(",",$cats);
        }
        return $wpdb->get_var("DELETE FROM `wp_blogmeta` WHERE `blog_id` = $blog_id AND `meta_value` IN($cats) AND `meta_key` LIKE '%$meta_key%'");
      }

      function get_user_meta_geography($user_id){
        global $wpdb;
        return $wpdb->get_results("SELECT meta_value as cat FROM `wp_usermeta` WHERE `user_id` = $user_id AND `meta_key` LIKE '%geography_%'");
      }

      function delete_user_meta_geography($user_id,$geography){
        global $wpdb;
        if(!empty($geography)){
          $geography = implode(",",$geography);
        }
        return $wpdb->get_var("DELETE FROM `wp_usermeta` WHERE `user_id` = $user_id AND `meta_value` IN($geography)");
      }

      add_action( 'template_redirect', 'remove_wpseo' );

      /**
       * Removes output from Yoast SEO on the frontend for a specific post, page or custom post type.
       */
      function remove_wpseo() {
         if(is_author()){
        global $wp;
        $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
        $captain_id = $author->ID;
        if(in_array('captain',$author->roles)){
              $front_end = YoastSEO()->classes->get( Yoast\WP\SEO\Integrations\Front_End_Integration::class );

              remove_action( 'wpseo_head', [ $front_end, 'present_head' ], -9999 );
          }
      }

}

function redirect_admin( $redirect){
  $redirect = 'https://dev.chotu.shop/nadeemshop/cart/';
}

add_filter( 'digits_login_redirect', 'redirect_admin',);