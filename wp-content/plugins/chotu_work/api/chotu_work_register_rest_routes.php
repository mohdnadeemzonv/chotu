<?php
add_action('rest_api_init', 'chotu_work_add_api_routes');
function chotu_work_add_api_routes($plugin_name){
	register_rest_route('chotu_work/v1', 'all_brand', array(
		'methods' => 'get',
		'callback' => 'get_captain_active_brand',
	));

	register_rest_route('chotu_work/v1', 'all_locality', array(
		'methods' => 'get',
		'callback' => 'get_captain_active_locality',
	));

	register_rest_route('chotu_work/v1', 'captain_brand', array(
		'methods' => 'get',
		'callback' => 'get_brand_served_by_captain',
	));

	register_rest_route('chotu_work/v1', 'captain_locality', array(
		'methods' => 'get',
		'callback' => 'get_locality_served_by_captain',
	));
}


?>