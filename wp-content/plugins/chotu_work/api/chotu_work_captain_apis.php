<?php
function wp_authenticate_by_phone_number($phone_number){
	$captains = get_users(
            array(
                'role' => 'captain',
                'meta_query' => array(
                    array(
                        'key' => 'phone_number',
                        'value' => $phone_number,
                        'compare' => '=='
                    )
                )
            )
        );
	if(!empty($captains)){
		return $captains[0];
	}
	new WP_Error( 'authentication_failed', __( '<strong>Error</strong>: Invalid username, email address or incorrect password.' ) );
}
function get_captain_active_brand(){
	$sites = get_sites(array('public'  => 1));
	wp_send_json(array('data'=>$sites));
}
function get_captain_active_locality(){
	$args = array('meta_query' => array(
			array(
				'key' => 'locality_status',
				'value' => 'active'
			)
		),
		'post_type'=>'locality',
		'post_status'=>'publish',
		'posts_per_page'=> -1
	);
    $localities = get_posts($args);
    wp_send_json(array('data'=>$localities));
}
function get_brand_served_by_captain($request){
	$captain_id = get_current_user_id();
	if(!isset($captain_id) || $captain_id == 0){
		return check_request_authentication();
	}
	$brand_ids = get_brands_by_captains($captain_id);
	if(!empty($brand_ids)){
		$brand_ids = implode(",",array_column($brand_ids, 'brand_id'));
		$sites = get_sites(array('site__in'=> $brand_ids,'public'  => 1));
		$return = array(
		    'data'  => $sites,
		);
		wp_send_json($return);
	}
	wp_send_json(array('data'=>''));
	
}
function get_locality_served_by_captain(){
	$captain_id = get_current_user_id();
	if(!isset($captain_id) || $captain_id == 0){
		return check_request_authentication();
	}
	$locality_ids = get_locality_by_captain($captain_id);
	if(!empty($locality_ids)){
		$locality_ids = array_column($locality_ids, 'locality_id');
		$args = array('post_type'=>'locality','post__in' => $locality_ids,'post_status'=>'publish','posts_per_page'=> -1);
      $localities = get_posts($args);
      $return = array(
		    'data'  => $localities,
		);
		wp_send_json($return);
	}
	wp_send_json(array('data'=>''));
}
function check_request_authentication(){
	return new WP_Error(
                'authentication_failed',
                __('You are not authenticated to access this route', 'wp-api-jwt-auth'),
                array(
                    'status' => 403,
                )
            );
}

?>