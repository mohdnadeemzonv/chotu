(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	/* shows the update locality button in checkout page on selecting a locality. */
	 $(document).on('change','input[name="locality_id"]', function(e) {
	 	$('#update_locality_id').removeClass('hide');
	 });
	 
	 /* Sets the locality_ID into the session through AJAX*/
	 $(document).on('click','#update_locality_id', function(e) {
	 	e.preventDefault();
	 	var locality_id = jQuery('input[name="locality_id"]:checked').val();
	 	if(locality_id == ''){
	 		return false;
	 	}
	 	$.ajax({
	 		type : "POST",
	 		url : chotu_shop.ajaxurl,
	 		data : {'locality_id':locality_id,action: "set_session_of_locality"},
	 		success: function(response) {
	 			//jQuery("#locality_select").modal("hide");
		     	location.reload();
		     	console.log(response);
		     }
		 });

	 });

})( jQuery );
