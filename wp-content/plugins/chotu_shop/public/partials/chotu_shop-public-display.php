<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_shop
 * @subpackage Chotu_shop/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
