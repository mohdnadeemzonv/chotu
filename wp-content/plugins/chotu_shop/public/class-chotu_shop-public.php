<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_shop
 * @subpackage Chotu_shop/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Chotu_shop
 * @subpackage Chotu_shop/public
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_shop_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_shop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_shop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_shop-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_shop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_shop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_shop-public.js', array( 'jquery' ), $this->version, false );

	}
	/* Send order to mongo using API */
	public function chotu_shop_send_success_order_data_to_api($order_id){
		global $wpdb;
		$blog_id = get_current_blog_id();
		$order = new WC_Order( $order_id );
		$user_id = $order->user_id;
		$total = $order->get_total();
		$order_status = $order->get_status();
		$phone = get_user_meta($user_id,'phone_number',true);
		if(!isset($phone)){
			$phone = get_custom_user_meta($user_id,'phone_number',true);
		}
		//$captain_id = get_post_meta($order_id,'captain_id',true);
		$locality_id = get_post_meta($order_id,'locality_id',true);
		$new_order_id = str_replace(' ', '', $blog_id.'_'.$order_id);
		$order_data=array();
		$order_data['order'] = $order->get_data();
		$order_data['order_unique_id'] = $new_order_id;
	//$order_data['captain_image_url'] = $captain_image_url;
	//$order_data['vendor_logo_url'] = $logo;
		$order_data['shop_id'] = $blog_id;
		$order_data['order_id'] = $order_id;
		$order_data['order_status'] = $order_status;
	//$order_data['vendor_name'] = $suffix;
	// $order_data['pin'] = get_pin_details_by_order_team_id($coupon_id,$coupon_code[0]);
		$i=0;
		foreach ($order->get_items() as $item_key => $item_values){
			$order_data['order'][$i]['item_data'] = $item_values->get_data();
			$i++;
		}
		$order_data['order']['postmeta'] = get_post_meta($order_id);
		$response = post_data_curl(json_encode($order_data),$order_id);
		if($response == false){
			update_post_meta($order_id,'mongo_api_status','failed');
		}
	}

	/* send order status to mongo */
	function chotu_shop_send_success_order_status_to_api($order_id){
		global $wpdb;
		$blog_id = get_current_blog_id();
		$order = new WC_Order( $order_id );
		$user_id = $order->user_id;
		$order_status = $order->get_status();
		$new_order_id = str_replace(' ', '', $blog_id.'_'.$order_id);
		$order_data['shop_id'] = $blog_id;
		$order_data['order_unique_id'] = $new_order_id;
		$order_data['order_status'] = $order_status;
		$response = post_data_curl(json_encode($order_data),$order_id);
	}

 /* Set locality in session on the checkout page */
	public function chotu_shop_set_session_of_locality(){
		$locality_id = $_POST['locality_id'];
		if(isset($_POST['locality_id'])){
			$_SESSION['l'] ='';
			unset($_SESSION['l']);
			$inactive = 86400;
			$_SESSION['expired'] = time() + $inactive;
			$_SESSION['l'] = $locality_id;
		}
		wp_die();
	}

	/* Send order status to mongo database using API */
	public	function chotu_shop_update_customer_woocommerce_order($order_id) {
		global $wpdb;
		if (!$order_id) {
			return;
		}
		$this->chotu_shop_send_success_order_status_to_api($order_id);
	}

	/* Send payment status to mongo database using API */
	public function chotu_shop_update_payment_complete($order_id){
		$order = wc_get_order( $order_id );
		$this->chotu_shop_send_success_order_status_to_api($order_id);
	}

	/* If URL param has locality included, set locality in session. */
	public function chotu_shop_check_url_coming_locality(){
		if(isset($_GET['l'])){
		$locality = explode(",", get_option('options_shop_localities'));
		    if(in_array($_GET['l'], $locality)){
		    	$inactive = 86400;
				$_SESSION['expired'] = time() + $inactive;
		    	$_SESSION['l'] = $_GET['l'];
		    }else{
		    	$_SESSION['l'] = '';
		    	unset($_SESSION['l']);
		    }
		  }
	}
}