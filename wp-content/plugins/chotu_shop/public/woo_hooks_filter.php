<?php

/* Display brand name in every product details page  */
add_action( 'woocommerce_product_meta_end', 'chotu_shop_after_add_to_cart_btn' );
function chotu_shop_after_add_to_cart_btn(){
  $brand_name = get_brand_name_or_brand_id(get_the_ID(),'name');
  $brand_id = get_brand_name_or_brand_id(get_the_ID(),'id');
  $before = '';
  if(isset($brand_name) && $brand_name !=''){
    $before = '<p>By: <em><a href="'.home_url().'/brand/?id='.$brand_id.'">'.$brand_name.'</a></em></p>';
  }
  echo $before;
}
add_filter( 'woocommerce_loop_add_to_cart_link', 'chotu_shop_before_after_btn', 10, 3 );

/* Display brand name in every product card from product loop on the shop page */
function chotu_shop_before_after_btn( $add_to_cart_html, $product, $args ){
  $brand = get_brand_name_or_brand_id($product->id,'name');
  $brand_id = get_brand_name_or_brand_id(get_the_ID(),'id');
  $before = '';
  if(isset($brand) && $brand !=''){
    $before = '<p>By: <em><a href="'.home_url().'/brand/?id='.$brand_id.'">'.$brand.'</a></em></p>';
  }
   // Some text or HTML here
  $after = ''; // Add some text or HTML here as well
  return $before . $add_to_cart_html . $after;
}
/*UNSETs the default fields on the checkout page in woocommerce*/
add_filter( 'woocommerce_checkout_fields' , 'chotu_work_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function chotu_work_override_checkout_fields( $fields ) {
 // unset($fields['billing']['billing_first_name']);
 unset($fields['billing']['billing_last_name']);
 unset($fields['billing']['billing_company']);
 unset($fields['billing']['billing_address_1']);
 unset($fields['billing']['billing_address_2']);
 unset($fields['billing']['billing_city']);
 unset($fields['billing']['billing_postcode']);
 unset($fields['billing']['billing_state']);
 unset($fields['billing']['billing_country']);
 // unset($fields['billing']['billing_email']);
 // unset($fields['billing']['billing_phone']);

 unset($fields['shipping']['shipping_first_name']);
 unset($fields['shipping']['shipping_last_name']);
 unset($fields['shipping']['shipping_company']);
 unset($fields['shipping']['shipping_address_1']);
 unset($fields['shipping']['shipping_address_2']);
 unset($fields['shipping']['shipping_city']);
 unset($fields['shipping']['shipping_postcode']);
 unset($fields['shipping']['shipping_state']);
 // unset($fields['shipping']['shipping_email']);
 unset($fields['shipping']['shipping_phone']);
  $fields['billing']['billing_first_name']['label'] = 'Name';
 return $fields;
}

/*CREATEs new fields on the checkout page in woocommerce*/
add_filter( 'woocommerce_after_checkout_billing_form' , 'chotu_shop_override_checkout_fields' );
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );

// Add locality address_syntax in the checkout page. 
function chotu_shop_override_checkout_fields( $checkout ) {
  if(isset($_SESSION['l'])){
     $url = LOCALITY_SITE_URL.'locality/'.$_SESSION['l'];
      $locality = get_api_request_from_server($url);
      if($locality == false){
        $locality = array();
      }
      $locality_address_syntax = isset($locality->metaval->locality_address_syntax[0]) ? $locality->metaval->locality_address_syntax[0] : '';
  $address_syntax = json_decode($locality_address_syntax,true);
  if(!empty($address_syntax)){
    $i=1;
    foreach ($address_syntax as $key => $value) {
      $name = strtolower(str_replace(' ', '_', $value['level']));
      $name = strtolower(str_replace('.', '', $name));
      if(trim($value['type']) == 'text'){
         woocommerce_form_field( 'level_'.$i, array(
            'type'  => 'text',
            'required' => true,
            'placeholder' => $value['level'],
            'class' => array('custom-field form-row-wide'),
            'label' => ucfirst($value['level']),                           
        ), $checkout->get_value(  'level_'.$i) );
      }
        if(trim($value['type']) == 'option'){
          $option = explode(',', $value['options']);
          $options = convert_index_array_to_associative($option);
          woocommerce_form_field( 'level_'.$i, array(
            'type'          => 'select',
            'class'         => array($name),
            'label'         =>__(ucfirst($value['level']), 'woocommerce'),
            'required'    => true,
            'options'     => $options,
            ),$checkout->get_value('level_'.$i ));
          }
          $i++;
        }
        woocommerce_form_field( 'billing_country', array(
            'type'  => 'hidden',
            'required' => true,
            'placeholder' => '',
            'class' => array('custom-field form-row-wide'),                          
        ), $checkout->get_value('IN') );

      }
    }
  }

/*show the email field as optional, rename the label*/ 
/*START*/ 
add_filter( 'woocommerce_billing_fields', 'chotu_woo_billing_email_optional_field');
add_filter( 'woocommerce_shipping_fields', 'chotu_woo_shipping_email_optional_field');

function chotu_woo_billing_email_optional_field( $fields ) {
    $fields['billing_email']['required'] = false;
    $fields['billing_email']['label'] = 'Email to get invoice';
    $fields['billing_phone']['value'] = '2';
    return $fields;
}
function chotu_woo_shipping_email_optional_field( $fields ) {
    $fields['shipping_email']['required'] = false;
    $fields['shipping_email']['label'] = 'Email to get invoice';
    return $fields;
}
/*END*/ 

/* Display captain details after the checkout fields*/  
add_action( 'woocommerce_after_checkout_billing_form', 'chotu_shop_display_extra_fields_after_billing_address' , 10, 1 );
function chotu_shop_display_extra_fields_after_billing_address() {
  if(isset($_SESSION['l'])){
    global $wpdb;
    $user_phone = '';
    $user_name = '';
    $blog_id = get_current_blog_id();
    if(is_user_logged_in()){
      $user_id = get_current_user_id();
      $user_phone = get_user_meta($user_id,'digits_phone_no', true);
      $user_name = get_user_meta($user_id,'nickname',true);
    }
      $url = LOCALITY_SITE_URL.'locality/'.$_SESSION['l'];
      $locality = get_api_request_from_server($url);
      if($locality == false){
        $locality = array();
      }
      $locality_name =  isset($locality->title->rendered) ? strtoupper($locality->title->rendered) :'';
      $locality_address = isset($locality->metaval->locality_address[0]) ? $locality->metaval->locality_address[0] : '';
      $locality_pincode = isset($locality->metaval->locality_pincode[0]) ? $locality->metaval->locality_pincode[0] : '';
      echo '<input type="hidden" name="user_phone_number" id="user_phone_number" value="'.$user_phone.'"><input type="hidden" name="user_name" id="user_name" value="'.$user_name.'"><h2 class="woocommerce-order-details__title">'.$locality_name.'</h2><table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
      <tfoot>
          <tr>
              <th scope="row">Locality Address :</th>
              <td><span class="woocommerce-delivery-date date">'.$locality_address.', '.$locality_pincode.'</span></td>
            </tr>
            <tr>
              <th scope="row">Shop Address :</th>
              <td>'.get_option('woocommerce_store_address').' '.get_option('woocommerce_store_address_2').', '.get_option('woocommerce_store_city').', '.get_option('woocommerce_store_postcode').'</td>
            </tr>
        </tfoot>
    </table>';
  }
}

/* Add the locality_address_syntax to the order_ID as meta data*/  
add_action( 'woocommerce_checkout_update_order_meta', 'chotu_shop_field_update_order_meta' );
function chotu_shop_field_update_order_meta( $order_id ) {
   $order = wc_get_order( $order_id );
   $total = $order->get_total();
    update_post_meta($order_id,'level_label_1',$_POST['level_label_1']);
    if(!empty($_POST['level_label_2'])){
       update_post_meta($order_id,'level_label_2',$_POST['level_label_2']);
    }else{
       update_post_meta($order_id,'level_label_2','N/A');
    }
    if(!empty($_POST['level_label_3'])){
       update_post_meta($order_id,'level_label_3',$_POST['level_label_3']);
    }else{
       update_post_meta($order_id,'level_label_3','N/A');
    }
    /* update values */
    update_post_meta($order_id,'level_1',$_POST['level_1']);
    if(!empty($_POST['level_2'])){
       update_post_meta($order_id,'level_2',$_POST['level_2']);
    }else{
       update_post_meta($order_id,'level_2','N/A');
    }
    if(!empty($_POST['level_3'])){
       update_post_meta($order_id,'level_3',$_POST['level_3']);
    }else{
       update_post_meta($order_id,'level_3','N/A');
    }
    if($_SESSION['l']){
      update_post_meta( $order_id, 'locality_id', $_SESSION['l']);
    }
   
    // if(isset($_POST['customer_billing_phone'])){
    //   update_post_meta( $order_id, 'customer_billing_phone', $_POST['customer_billing_phone']);
    // }
    
 }

/*convert index array into associative*/
function convert_index_array_to_associative($options){
  if(is_array($options)){
    $aa = array();
    foreach ($options as $key => $option) {
       $aa[$option] = $option;
    }
    return $aa;
  }
}

/* remove sidebar from storefront theme for every page */
function chotu_shop_remove_storefront_sidebar() {
    remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
    add_filter( 'body_class', function( $classes ) {
        return array_merge( $classes, array( 'page-template-template-fullwidth page-template-template-fullwidth-php ' ) );
    } );
}
add_action( 'get_header', 'chotu_shop_remove_storefront_sidebar' );

/* Add phone number and email fields in woocommerce setting genaral tab section*/
add_filter( 'woocommerce_general_settings', 'chotu_shop_store_contact_settings', 10, 1 );
function chotu_shop_store_contact_settings( $settings) {
  /**
   * Check the current section is what we want
   **/
  $new_settings =
      array(
        array(
          'title' => __( 'Store Contact', 'woocommerce' ),
          'type'  => 'title',
          'desc'  => __( 'This is where your business is located.', 'woocommerce' ),
          'id'    => 'store_contact',
        ),
        array(
          'title' => __( 'Store Phone Number', 'woocommerce' ),
          'type'  => 'title',
          'desc'  => __( 'This is contact number visible for customers .', 'woocommerce' ),
          'id'    => 'store_phone_number',
          'css'      => 'min-width:50px;',
          'type'  => 'number',
          'desc_tip' => true,
        ),
        array(
          'title' => __( 'Store Email', 'woocommerce' ),
          'type'  => 'title',
          'desc'  => __( 'This is email visible for customers .', 'woocommerce' ),
          'css'      => 'min-width:50px;',
          'id'    => 'store_email',
          'type'  => 'email',
          'desc_tip' => true,
        ),
        array(
          'type' => 'sectionend',
          'id'   => 'store_contact',
        ),
      );
    $settings =  array_merge($new_settings,$settings);
    return $settings;
}
/*validate checkout page phone number for length check 10 digit */
add_action('woocommerce_checkout_process', 'chotu_shop_validate_billing_phone');
function chotu_shop_validate_billing_phone() {
    $is_correct = preg_match('/^[0-9]{10,10}$/', $_POST['billing_phone']);
    if ( $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'The Phone field should be <strong>10 digits Only</strong>.' ), 'error' );
    }
}

/*show notification on top of home page*/
  /*
  if L is in shop_localities
{
don't show any popup, 
on checkout page, show the locality name & address_syntax | option to edit (popup)
}

if L is in universal_localities
{
Show a green notification on homescreen, "Welcome! Thanks for visiting us. We do not yet serve your locality, we have taken your request. Please visit your local shop here:" <a locality_shop>
}

If Ls is NOT in universal_localities
{
Show a green notification on homescreen, "Welcome! Thanks for visiting us. We are unable to identify your address. Please request to add your locality here: <a add_locality_link>
}
  */
add_action('storefront_content_top','chotu_shop_display_next_delivery_date');
function chotu_shop_display_next_delivery_date(){
  if ( !class_exists( 'woocommerce' ) ) { return false; }

  if(isset($_GET['l'])){
    $locality = explode(",", get_option('options_shop_localities'));
    if(!in_array($_GET['l'], $locality)){
     $locality = get_shop_localities_details_by_api($_GET['l']);
     if(!empty($locality) || $locality !=null){
      $universal_locality_text = get_option('options_universal_locality_text').'<a href="'.$locality[0]->link.'">'.$locality[0]->title->rendered.'</a>';
      wc_print_notice( __($universal_locality_text, "woocommerce"), "success" );
    }else{
      $universal_locality_text = get_option('options_not_in_universal_locality_text');
      wc_print_notice( __($universal_locality_text, "woocommerce"), "success" );
      
    }
  }else{
    $locality = get_shop_localities_details_by_api($_GET['l']);
    if(isset($locality[0]->title->rendered)){
      wc_print_notice( __('Welcome '.$locality[0]->title->rendered, "woocommerce"), "success" );
    }
  }
}

}
add_action('woocommerce_before_checkout_form','chotu_shop_woocommerce_before_checkout_form');
function chotu_shop_woocommerce_before_checkout_form(){
  $localities = get_shop_localities_details_by_api('');
  ?>
  <div class="cont-bg">
    <div class="cont-main">
      <div class="custom-row">
       <?php 
       if(!empty($localities)){
        // echo "nadeem".$_SESSION['l'];
        foreach ($localities as $key => $locality) {
          $selected = '';
          if(isset($_SESSION['l']) && ($locality->id == trim($_SESSION['l']))){
            $selected = 'checked';
          }
          if($locality->status == 'publish'){

            ?>
            <div class="col">
              <div class="cont-checkbox">
                <input class="checkbox_input" type="radio" name="locality_id" id="myRadio-<?php echo $key?>" <?php echo $selected;?> value="<?php echo $locality->id;?>" data-title="<?php echo $locality->title->rendered;?>">
                <label class="lable_for_check" for="myRadio-<?php echo $key?>">
                  <img src="<?php echo $locality->metaval->post_attachement;?>">
                  <span class="cover-checkbox">
                    <svg viewBox="0 0 12 10">
                      <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                    </svg>
                  </span>
                  <div class="info"><?php echo $locality->title->rendered;?></div>
                </label>
              </div>
            </div>
            <?php 
          }
        }
      }
      ?>
      <button type="button" class="btn btn-primary" id="update_locality_id">Update Locality</button>
    </div>
  </div>
</div>
<?php
}


/* In checkout page, if locality not selected disable place order button */
add_filter('woocommerce_order_button_html', 'chotu_shop_inactive_order_button_html' );
function chotu_shop_inactive_order_button_html( $button ) {
    // HERE define your targeted shipping class
    $found = false;
    if(!isset($_SESSION['l'])){
      $found = true;
    }

    // If found we replace the button by an inactive greyed one
    if( $found ) {
        $style = 'style="background:Silver !important; color:white !important; cursor: not-allowed !important;text-align: center;"';
        $button_text = apply_filters( 'woocommerce_order_button_text', __( 'Place order', 'woocommerce' ) );
        $button = '<a class="button" '.$style.'>' . $button_text . '</a>';
    }
    return $button;
}
/* shop page, cart page and checkout page title override by custom field option.*/
//add_filter( 'woocommerce_page_title', 'chotu_shop_woocommerce_page_title');
add_action( 'storefront_page', 'chotu_shop_woocommerce_page_title' );
function chotu_shop_woocommerce_page_title() {
  if(get_the_title() == 'Cart' ){
    ?>
    <header class="entry-header page_header">
      <?php
      storefront_post_thumbnail( 'full' );
      echo '<h1 class="entry-title">'.get_option('options_cart_text').'</h1>';
      ?>
    </header><!-- .entry-header -->
    <?php
  }
  if(get_the_title() == 'Checkout' ){
    ?>
    <header class="entry-header page_header">
      <?php
      storefront_post_thumbnail( 'full' );
      echo '<h1 class="entry-title">'.get_option('options_checkout_text').'</h1>';
      ?>
    </header><!-- .entry-header -->
    <?php
    /* Show coupon div only if the locality is set in the session */
    if(isset($_SESSION['l'])){
    echo '<style>.woocommerce-form-coupon-toggle, form.checkout{display: block !important;}</style>';
  }
  }
 
}

// Display brnad name on cart and checkout page.
add_filter( 'woocommerce_get_item_data', 'chotu_shop_get_item_data' , 25, 2 );
function chotu_shop_get_item_data ( $cart_data, $cart_item ) {
    if( ! empty( $cart_item['product_id'] ) ){
        $cart_data[] = array(
            'name'    => __( "By", "chotu"),
            'value' => '<div>'.get_brand_name_or_brand_id($cart_item['product_id'],'name').'</div>'
        );
    }
    return $cart_data;
}

/* sort cart and checkout item by brand name by alphabetical order in ascending*/
add_action( 'woocommerce_cart_loaded_from_session', 'chotu_shop_sort_cart_items_by_brand', 100 );
function chotu_shop_sort_cart_items_by_brand() {
    $items_to_sort = $cart_contents = array(); // Initializing

    // Loop through cart items
    foreach ( WC()->cart->cart_contents as $item_key => $cart_item ) {
        $brand  = get_brand_name_or_brand_id($cart_item['product_id'],'name');
        $items_to_sort[ $item_key ] = $brand;
    }
    ksort( $items_to_sort );
    // Loop through sorted items key
    foreach ( $items_to_sort as $cart_item_key => $store_name ) {
        $cart_contents[ $cart_item_key ] = WC()->cart->cart_contents[ $cart_item_key ];
    }
    // Set sorted items as cart contents
    WC()->cart->cart_contents = $cart_contents;
}

/* Add shope header in every page */
add_action('storefront_before_content','chotu_shop_add_shop_text_header_for_all_pages');
function chotu_shop_add_shop_text_header_for_all_pages(){
    echo '<div class="header_second"><h3 class="header_text">'.get_bloginfo('name').'</h3></div>';
    if (is_shop()){
      echo '<div class="address_text">';
      echo '<span>'.get_option('woocommerce_store_address').' '.get_option('woocommerce_store_address_2').', '.get_option('woocommerce_store_city').'</span>';
        echo get_option('options_shop_intro');
      echo '</div>';
    }
    
}

/* Hide shop page title form shop page*/
add_filter('woocommerce_show_page_title', 'chotu_shop_hide_shop_page_title');
function chotu_shop_hide_shop_page_title($title) {
   if (is_shop()) $title = false;
   return $title;
}


/*The three icons in woocommerce footer are to be rearranged and reassigned to: search, home_URL & cart*/
add_filter('storefront_handheld_footer_bar_links','chotu_shop_change_my_account_menu_link_in_footer',10,1);
function chotu_shop_change_my_account_menu_link_in_footer($links){
  if(!empty($links)){
    foreach ($links as $key => $link) {
      if($key == 'my-account'){
        //$links[$key]['priority']=20;
        $links[$key]['callback']='chotu_storefront_handheld_footer_bar_account_link';
      }
      if($key == 'search'){
        //$links[$key]['priority']=10;
      }
    }
  }
  krsort($links);
return $links;
}
/* change mobile menu footer for add home icon */
function chotu_storefront_handheld_footer_bar_account_link(){
  echo'<style>.storefront-handheld-footer-bar ul li.my-account>a::before {
      content: "" !important;
      background-image: url("https://img.icons8.com/ios-filled/30/fdcb9e/home.png");
      background-position-x: center;
      background-repeat: repeat-y;
      background-size: contain;
      margin-bottom: 10px;
      margin-top: 9px;
  }</style>';
  $href =home_url();
  echo '<a href="'.$href.'">My Account </a>';
} 

/* Which template to call when a brand page is selected. */
add_filter( 'page_template', 'chotu_shop_brand_page_template_loaded' );
function chotu_shop_brand_page_template_loaded( $template ) {
    if( 'brand-page.php' == basename( $template ) )
        $template = plugin_dir_path( __DIR__ ) . 'templates/brand-page.php.php';
    return $template;
}

/* after successfully regieter on new user add some update in user meta */
add_action('user_register','update_user_site_regiter_details_and_cap',10,1);
function update_user_site_regiter_details_and_cap($user_id){
  global $wpdb;
  $blog_id = get_current_blog_id();
  $blog_name = get_bloginfo('name');
  update_user_meta($user_id,'new_user_register_from',$blog_id.'_'.$blog_name);
  update_meta_key('wp_'.$blog_id.'_capabilities','wp_capabilities');
  update_meta_key('wp_'.$blog_id.'_user_level','wp_user_level');
}

add_filter('storefront_credit_links_output','remove_store_front_theme_link_footer',10,1);
function remove_store_front_theme_link_footer($footer){
  $footer ='';
  return $footer;
}
add_filter('storefront_copyright_text','remove_store_front_shop_name_link_footer',10,1);
function remove_store_front_shop_name_link_footer($footer){
  $content = '&copy; chotu ' . gmdate( 'Y' );
  return $content;
}
add_action('storefront_after_footer','add_whatsapp_floating_icon');
function add_whatsapp_floating_icon(){
  $phone_number = get_option('store_phone_number');
  if(preg_match('/^[0-9]{10}+$/', $phone_number)){
    $phone_number = '91'.$phone_number;
  }
  $achor = '<a href="?login=true" onclick="jQuery(this).digits_login_modal(jQuery(this));return false;" attr-disclick="1" class="digits-login-modal" type="1"><img src="'.plugin_dir_path(__DIR__).'assets/images/whatsapp.png"></a>';
  if(is_user_logged_in()){
    $achor = '<a href="https://wa.me/'.$phone_number.'?text='.home_url($_SERVER['REQUEST_URI']).'" target="_blank"><img src="'.plugin_dir_path(__DIR__).'assets/images/whatsapp.png"></a>';
  }
  echo '<div class="floating_icon">'.$achor.'</div>';
}


