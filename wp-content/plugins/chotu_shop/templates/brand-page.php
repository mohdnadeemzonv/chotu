<?php
/* Template Name: Brand Page under a shop*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="container">
			<h2 class="entry-title"><?php echo get_brand_name_by_brand_id($_GET['id']);?></h2>
			<ul class="products">
			<?php
			if(isset($_GET['id'])){
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => 12,
					'meta_query'    => array(
	                    array(
	                        'key'       => 'brand',
	                        'value'     => $_GET['id'].'_',
	                        'compare'   => 'like',
	                    ),
	                ),
					);
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();
						wc_get_template_part( 'content', 'product' );
					endwhile;
				} else {
					echo __( 'No products found' );
				}
				wp_reset_postdata();
			}
			?>
		</ul>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
