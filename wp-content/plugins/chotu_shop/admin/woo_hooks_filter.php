<?php

// Display brand (custom field) as an additional field for products
add_action('woocommerce_product_options_general_product_data', 'chotu_shop_woocommerce_product_custom_fields');
function chotu_shop_woocommerce_product_custom_fields() {
    global $product_object;

    echo '<div class=" product_custom_field ">';

    // Custom Product Text Field
    woocommerce_wp_text_input( array( 
        'id'          => 'brand',
        'label'       => __('By:', 'woocommerce'),
        'placeholder' => '',
        'desc_tip'    => 'true' // <== Not needed as you don't use a description
    ) );

    echo '</div>';
}

// Save admin product custom setting field(s) values
add_action('woocommerce_process_product_meta', 'chotu_shop_woocommerce_product_custom_fields_save');
function chotu_shop_woocommerce_product_custom_fields_save( $product_id ) {
    if ( isset($_POST['brand']) ) {
        update_post_meta( $product_id,'brand', sanitize_text_field( $_POST['brand'] ) );
    }
}

// Add brand to multiple products in quick edit (admin function)
add_action( 'woocommerce_product_quick_edit_end', function(){

    /*
    Notes:
    Take a look at the name of the text field, '_custom_field_demo', that is the name of the custom field, basically its just a post meta
    The value of the text field is blank, it is intentional
    */

    ?>
    <div class="brand_fields" bis_skin_checked="1">
		
			<br class="clear">
			<label>
				<span class="title">Made By</span>
				<span class="input-text-wrap">
					<input type="text" name="brand" class="text wc_input_brand brand" placeholder="By:" value="">
				</span>
			</label>
			<br class="clear">
		</div>
    <?php

} );

/* Update product meta key name brand in database */
add_action('woocommerce_product_quick_edit_save', function($product){
    $post_id = $product->id;
    if ( isset( $_REQUEST['brand'] ) ) {
        $brand = trim(esc_attr( $_REQUEST['brand'] ));
        // Do sanitation and Validation here
        update_post_meta( $post_id, 'brand', wc_clean( $brand ) );
    }

}, 10, 1);

// Add brand to multiple products in quick edit (admin function) - set with actual value
add_action( 'manage_product_posts_custom_column', function($column,$post_id){

switch ( $column ) {
    case 'name' :

        ?>
        <div class="hidden brand_inline" id="brand_inline_<?php echo $post_id; ?>">
            <div id="brand"><?php echo get_post_meta($post_id,'brand',true); ?></div>
        </div>
        <?php

        break;

    default :
        break;
}

}, 99, 2);

/* add a menu in admin menu name "Chotu Shop" by creating option page*/
add_action('acf/init', 'chotu_shop_acf_op_init');
function chotu_shop_acf_op_init() {
    // Check function exists.
  if( function_exists('acf_add_options_page') ) {

        // Register options page.
    $option_page = acf_add_options_page(array(
      'page_title'    => __('Chotu Shop'),
      'menu_title'    => __('Chotu Shop'),
      'menu_slug'     => 'chotu-shop',
      'capability'    => 'edit_posts',
      'icon_url' => 'dashicons-universal-access-alt',
      'redirect'      => false,
      'position' => 7
    ));
      }
    }

    /* add brand page template in database and assign to page */
    add_action('admin_init','chotu_shop_add_brand_template_and_assign');
    function chotu_shop_add_brand_template_and_assign() {
       global $user_ID;
       $new_post = array(
        'post_title' => 'Brand',
        'post_content' => '',
        'post_status' => 'publish',
        'post_date' => date('Y-m-d H:i:s'),
        'post_author' => $user_ID,
        'post_type' => 'page',
        'post_category' => array(0)
    );
       if ( 0 === post_exists( 'Brand' ) ) {
          $post_id = wp_insert_post($new_post);
          if (!$post_id) {
            wp_die('Error creating template page');
        } else {
            update_post_meta($post_id, '_wp_page_template', 'brand-page.php');
        }
    }
}
?>