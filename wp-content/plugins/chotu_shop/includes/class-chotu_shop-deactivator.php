<?php

/**
 * Fired during plugin deactivation
 *
 * @link       zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_shop
 * @subpackage Chotu_shop/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Chotu_shop
 * @subpackage Chotu_shop/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_shop_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
