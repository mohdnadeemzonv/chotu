<?php

/**
 * Fired during plugin activation
 *
 * @link       zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_shop
 * @subpackage Chotu_shop/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Chotu_shop
 * @subpackage Chotu_shop/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_shop_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
