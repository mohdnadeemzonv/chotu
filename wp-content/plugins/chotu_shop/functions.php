<?php
/* Nadeem's favorite function, just prints the text */
if(!function_exists('dd')){
	function dd($data){
		echo "<pre>";
		print_r($data);
		echo "<pre>";
		die;
	}
}

/* Add AJAX url in header for calling AJAX in Javascript*/

add_action('wp_head','chotu_shop_add_ajax_url_header');
function chotu_shop_add_ajax_url_header(){?>
	<script id='chotu_shop_ajax_script'>
var chotu_shop = <?php echo json_encode(array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));?>
</script>
<?php }

/* Generate JWT token for API call. To call central site APIs */
add_action('init','chotu_shop_action_on_init');
function chotu_shop_action_on_init(){
	$token = get_option('token');
	if($token ==''){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => TOKEN_GENERATTION_URL,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_HTTPHEADER => array(),
		));

		$response = curl_exec($curl);
		$result = json_decode($response);
		if(!empty($result)){
			update_option('token',$result->token);
		}
		curl_close($curl);
	}
	if (!session_id()) {
	    session_start();
	}
	
}

/*API Credentials to post to MongoDB*/
function post_data_curl($data,$order_id){
		$curl = curl_init();
		$auth = base64_encode(AMBASSDOR_API_USERNAME_PASSWORD);
		curl_setopt_array($curl, array(
			CURLOPT_URL => AMBASSDOR_API_URL,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>$data,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic $auth",
				"Content-Type: application/json"
			),
		));
		$response = curl_exec($curl);
		$result = json_decode($response);
		if(isset($result->_id)){
			update_post_meta($order_id,'mongo_api_status','success');
			return true;
		}else{
			update_post_meta($order_id,'mongo_api_status','failed');
			return true;
		}
		curl_close($curl);
		return false;
	}

/* API Call to central site: pass the url for which api you call and return the response*/
function get_api_request_from_server($url){
	$curl = curl_init();
	$token = get_option('token');
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
	    'Authorization: Bearer '. $token
	  ),
	));
$response = curl_exec($curl);
curl_close($curl);
$result = json_decode($response);
  if (isset($result->data->status)) {
  	update_option('token','');
    get_api_request_from_server($url);
}else{
	return $result;
}

}

/* Return Locality details array from API call and param locality id */
/* Input: locality_ID */
/* Output: converts the JSON details of the locality into an array*/
function get_shop_localities_details_by_api($locality_id){
	$locality = get_option('options_shop_localities');
	if($locality_id){
		$locality = $locality_id;
	}
	$url = LOCALITY_SITE_URL.'locality?include='.$locality;
	$localities = get_api_request_from_server($url);
	if($localities == false){
		$localities = array();
	}
	return $localities;
}

/* Get brand name by exploding brand meta value */
/* Brand is stored as BrandID_BrandName as product meta. This function splits it by "_" and returns the brand_name. */
function get_brand_name_or_brand_id($product_id,$type){
	$brand_value = get_post_meta($product_id,'brand',true);
	if(isset($brand_value)){
		$brand_array = explode("_",$brand_value);
		if($type == 'id'){
			return $brand_array[0];
		}else{
			return $brand_array[1];
		}
	}
}

/* To display the brand name in the brand page under a shop, we extract brand name by brand id */
function get_brand_name_by_brand_id($brand_id){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$wp_post_meta = $wpdb->base_prefix.$blog_id.'_postmeta';
	if(!is_multisite()){
		$wp_post_meta = $wpdb->base_prefix.'postmeta';
	}
	$brand_id = "'%".$brand_id."_%'";
	$brand_value = $wpdb->get_var("SELECT `meta_value` FROM $wp_post_meta WHERE `meta_key`= 'brand' AND `meta_value` LIKE $brand_id");
	if(isset($brand_value)){
		$brand_array = explode("_",$brand_value);
			return $brand_array[1];
	}
}
/*
Retrieve post meta of central site from a network site (brand) for a user (captain or buyer)
*/
if(!function_exists('get_custom_user_meta')){
	function get_custom_user_meta($user_id,$meta_key){
  global $wpdb;
  $wp_postmeta = "wp_usermeta";
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND user_id = '".$user_id ."')"); 
}
}

/* update usermeta key */
if(!function_exists('update_meta_key')){
	function update_meta_key( $old_key=null, $new_key=null ){
    global $wpdb;
    $table_name = $wpdb->base_prefix."postmeta";
    $wpdb->query($wpdb->prepare("UPDATE $table_name SET `meta_key`='$new_key' WHERE `userid`=$userid AND `meta_key` = '$old_key'"));
}
}
