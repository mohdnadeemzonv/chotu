<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              zonvoir.com
 * @since             1.0.0
 * @package           Chotu_shop
 *
 * @wordpress-plugin
 * Plugin Name:       Chotu Shop
 * Plugin URI:        chotu.com
 * Description:       This plugin retrive all the localities from chotu.com and shop owner can choose the localities which he want to serve. also after successfull order order data will send to mongo.
 * Version:           1.0.0
 * Author:            Mohd Nadeem
 * Author URI:        zonvoir.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       chotu_shop
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CHOTU_SHOP_VERSION', '1.0.0' );
define('TOKEN_GENERATTION_URL','https://chotu.com/wp-json/jwt-auth/v1/token?username=nadeem&password=mybest@chotu');
define('LOCALITY_SITE_URL', 'https://chotu.com/wp-json/wp/v2/');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-chotu_shop-activator.php
 */
function activate_chotu_shop() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chotu_shop-activator.php';
	Chotu_shop_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-chotu_shop-deactivator.php
 */
function deactivate_chotu_shop() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chotu_shop-deactivator.php';
	Chotu_shop_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_chotu_shop' );
register_deactivation_hook( __FILE__, 'deactivate_chotu_shop' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-chotu_shop.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_chotu_shop() {

	$plugin = new Chotu_shop();
	$plugin->run();

}
run_chotu_shop();
