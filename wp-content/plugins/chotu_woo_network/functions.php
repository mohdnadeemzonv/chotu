<?php 
/*show locality banner pic on the brand home page*/
add_action('storefront_before_content','chotu_woo_network_add_captain_banner_image_header');
function chotu_woo_network_add_captain_banner_image_header(){
global $wpdb;
    $url='';
    //echo '<a href="'.network_home_url(0).'">Home</a>';
    $blog_id = get_current_blog_id();
    if( is_shop() ){
      if (isset($_SESSION['l']) && isset($_SESSION['c']) && isset($_SESSION['d'])){
        if($_SESSION['l'] !=''){
          $title = get_localitity_by_id('locality',$_SESSION['l']);
          $horizontal_pic = get_custom_post_meta($_SESSION['l'],'locality_banner_pic');
          if(isset($horizontal_pic) && $horizontal_pic !=''){
            $url = get_custom_attachment_image($horizontal_pic,'banner-image');
          }else{
            $url = home_url().'/wp-content/uploads/2021/07/9e69a59c38ca00c68311d9ecf19eb078.jpg';
          }
          //echo '<div id="header-image" '.$_SESSION['l'].'>'.$title->post_title.'<img width="100%" height="150px"  alt="" class="header-image" src="'.$url.'">Delivered only to: <strong>'.$title->post_title.'</strong></div>';
          $blog_id = get_current_blog_id();
          $delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,'','');
         
          } 
        }
    }
    $banner_image ='';
    $banner_pic_of_brand = unserialize(get_banner_pic_of_brand($blog_id,'banner_pic'));

    if(!empty($banner_pic_of_brand)){
        $banner_image = $banner_pic_of_brand['banner_pic'];
      }
      $url = $banner_image;
      //$url = home_url().'/wp-content/uploads/2021/07/9e69a59c38ca00c68311d9ecf19eb078.jpg';
      echo '<div id="header-image"><img width="100%"  alt="" class="header-image" src="'.$url.'"></div>';
}

/*show next delivery date as marquee on the brand home page*/
add_action('storefront_content_top','chotu_woo_network_display_next_delivery_date');
function chotu_woo_network_display_next_delivery_date(){
  if ( !class_exists( 'woocommerce' ) ) { return false; }
  global $wpdb;
    $title ='';
    $blog_id = get_current_blog_id();
    $delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,'','');
   
    $delivery_date = '';
    $share_url = '';
    if(!empty($delivery_order_cycle)){
      $delivery_date = $delivery_order_cycle->delivery_date;
      $order_before_time = $delivery_order_cycle->order_before_time;
      if(isset($_SESSION['l']) &&  $_SESSION['l']!=''){
        $locality = get_localitity_by_id('locality',$_SESSION['l']);
      }
      $blogdescription = get_brand_option($blog_id,'blogdescription');
      //echo '<a href="javascript:;" data-toggle="modal" data-target="#locality_select" class="btn login font-family-inter font-weight-normal btn-primary">Change Locality</a>';

      echo '<marquee width="100%" direction="left">'. $title.' Next Delivery Date: '.date('j M, D',strtotime($delivery_date)).'</marquee><p class="caption">'.$blogdescription.'</caption>';
   }
}

/*UNSETs the default fields on the checkout page in woocommerce*/
add_filter( 'woocommerce_checkout_fields' , 'chotu_work_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function chotu_work_override_checkout_fields( $fields ) {
 // unset($fields['billing']['billing_first_name']);
 unset($fields['billing']['billing_last_name']);
 unset($fields['billing']['billing_company']);
 unset($fields['billing']['billing_address_1']);
 unset($fields['billing']['billing_address_2']);
 unset($fields['billing']['billing_city']);
 unset($fields['billing']['billing_postcode']);
 unset($fields['billing']['billing_state']);
 unset($fields['billing']['billing_country']);
 // unset($fields['billing']['billing_email']);
 // unset($fields['billing']['billing_phone']);

 unset($fields['shipping']['shipping_first_name']);
 unset($fields['shipping']['shipping_last_name']);
 unset($fields['shipping']['shipping_company']);
 unset($fields['shipping']['shipping_address_1']);
 unset($fields['shipping']['shipping_address_2']);
 unset($fields['shipping']['shipping_city']);
 unset($fields['shipping']['shipping_postcode']);
 unset($fields['shipping']['shipping_state']);
 // unset($fields['shipping']['shipping_email']);
 unset($fields['shipping']['shipping_phone']);
  $fields['billing']['billing_first_name']['label'] = 'Name';
 return $fields;
}

/*CREATEs new fields on the checkout page in woocommerce*/
add_filter( 'woocommerce_after_checkout_billing_form' , 'chotu_woo_network_override_checkout_fields' );
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
// Our hooked in function - $fields is passed via the filter!
function chotu_woo_network_override_checkout_fields( $checkout ) {
  if(isset($_SESSION['c'])){
  $gated_locality = get_custom_post_meta($_SESSION['l'],'locality_address_syntax',true);
  $address_syntax = json_decode($gated_locality,true);
  if(!empty($address_syntax)){
    $i=1;
    foreach ($address_syntax as $key => $value) {
      $name = strtolower(str_replace(' ', '_', $value['level']));
      $name = strtolower(str_replace('.', '', $name));
      woocommerce_form_field( 'level_label_'.$i, array(
            'type'  => 'hidden',
            'placeholder' => $value['level'],
            'class' => array('custom-field form-row-wide'),
            'label' => ucfirst($value['level']),                           
            'default' => ucfirst($value['level']),                           
        ), $checkout->get_value( $value['level']) );
      if(trim($value['type']) == 'text'){
         woocommerce_form_field( 'level_'.$i, array(
            'type'  => 'text',
            'required' => true,
            'placeholder' => $value['level'],
            'class' => array('custom-field form-row-wide'),
            'label' => ucfirst($value['level']),                           
        ), $checkout->get_value(  'level_'.$i) );
      }
        if(trim($value['type']) == 'option'){
          $option = explode(',', $value['options']);
          $options = convert_index_array_to_associative($option);
          woocommerce_form_field( 'level_'.$i, array(
            'type'          => 'select',
            'class'         => array($name),
            'label'         =>__(ucfirst($value['level']), 'woocommerce'),
            'required'    => true,
            'options'     => $options,
            ),$checkout->get_value('level_'.$i ));
          }
          $i++;
        }
        woocommerce_form_field( 'billing_country', array(
            'type'  => 'hidden',
            'required' => true,
            'placeholder' => '',
            'class' => array('custom-field form-row-wide'),                          
        ), $checkout->get_value('IN') );

      }
    }
  }

/*show the email field as optional, rename the label*/ 
/*START*/ 
add_filter( 'woocommerce_billing_fields', 'chotu_woo_billing_email_optional_field');
add_filter( 'woocommerce_shipping_fields', 'chotu_woo_shipping_email_optional_field');

function chotu_woo_billing_email_optional_field( $fields ) {
    $fields['billing_email']['required'] = false;
    $fields['billing_email']['label'] = 'Email to get invoice';
    $fields['billing_phone']['value'] = '2';
    return $fields;
}
function chotu_woo_shipping_email_optional_field( $fields ) {
    $fields['shipping_email']['required'] = false;
    $fields['shipping_email']['label'] = 'Email to get invoice';
    return $fields;
}
/*END*/ 

/*display captain details after the checkout fields*/  
add_action( 'woocommerce_after_checkout_billing_form', 'chotu_woo_network_display_extra_fields_after_billing_address' , 10, 1 );
function chotu_woo_network_display_extra_fields_after_billing_address() {
  if(isset($_SESSION['l'])){
    global $wpdb;
    $user_phone = '';
    $user_name = '';
    $blog_id = get_current_blog_id();
    if(is_user_logged_in()){
      $user_id = get_current_user_id();
      $user_phone = get_custom_user_meta($user_id,'digits_phone_no');
      $user_name = get_custom_user_meta($user_id,'nickname');
    }
    $user = new WP_User($_SESSION['c']);
    $phone_number = get_custom_user_meta($_SESSION['c'],'phone_number');
    $location_table_data = get_location_object_data_by_type($_SESSION['l'],'locality');
    $captain_table_data = get_location_object_data_by_type($_SESSION['c'],'captain');
    $locality = get_localitity_by_id('locality',$_SESSION['l']);
    if(!empty($location_table_data)){
      $locality_name =  isset($locality->post_title) ? strtoupper($locality->post_title) :'';
      echo '<input type="hidden" name="user_phone_number" id="user_phone_number" value="'.$user_phone.'"><input type="hidden" name="user_name" id="user_name" value="'.$user_name.'"><h2 class="woocommerce-order-details__title">'.$locality_name.'</h2><table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
      <tfoot>
          <tr>
              <th scope="row">Locality Address :</th>
              <td><span class="woocommerce-delivery-date date">'.$location_table_data->full_address.', '.$location_table_data->pincode.'</span></td>
            </tr>
            <tr>
              <th scope="row">Pickup Address :</th>
              <td>'.$captain_table_data->full_address.', '.$captain_table_data->pincode.'</td>
            </tr>
            <tr>
              <th scope="row">Delivered By :</th>
              <td><span class="captain-pickup-address address_cl captain_name" title="'.$user->display_name. '/'. $phone_number.'">'.$user->display_name.' / '. $phone_number.'</span></td>
            </tr>
        </tfoot>
    </table>';
    }
  }
}

/*Add the locality_address_syntax to the order_ID as meta data*/  
add_action( 'woocommerce_checkout_update_order_meta', 'chotu_woo_network_field_update_order_meta' );
function chotu_woo_network_field_update_order_meta( $order_id ) {
  
   $order = wc_get_order( $order_id );
   $total = $order->get_total();
 
    update_post_meta($order_id,'level_label_1',$_POST['level_label_1']);
    if(!empty($_POST['level_label_2'])){
       update_post_meta($order_id,'level_label_2',$_POST['level_label_2']);
    }else{
       update_post_meta($order_id,'level_label_2','N/A');
    }
    if(!empty($_POST['level_label_3'])){
       update_post_meta($order_id,'level_label_3',$_POST['level_label_3']);
    }else{
       update_post_meta($order_id,'level_label_3','N/A');
    }
    /* update values */
    update_post_meta($order_id,'level_1',$_POST['level_1']);
    if(!empty($_POST['level_2'])){
       update_post_meta($order_id,'level_2',$_POST['level_2']);
    }else{
       update_post_meta($order_id,'level_2','N/A');
    }
    if(!empty($_POST['level_3'])){
       update_post_meta($order_id,'level_3',$_POST['level_3']);
    }else{
       update_post_meta($order_id,'level_3','N/A');
    }
    if($_SESSION['l']){
      update_post_meta( $order_id, 'locality_id', $_SESSION['l']);
    }
    if($_SESSION['c']){
      update_post_meta( $order_id, 'captain_id', $_SESSION['c']);
    }
    if($_SESSION['d']){
      update_post_meta( $order_id, 'date_of_delivery', $_SESSION['d']);
    }
    // if(isset($_POST['customer_billing_phone'])){
    //   update_post_meta( $order_id, 'customer_billing_phone', $_POST['customer_billing_phone']);
    // }
    
 }

/*ORDER RECD PAGE: whatsapp share text along with good for me, god for locality_name*/  
add_action('woocommerce_before_thankyou','chotu_woo_network_share_button_on_order_recieve_page');
function chotu_woo_network_share_button_on_order_recieve_page($order_id){
  $current_blog_id = get_current_blog_id();
  $data = array();
  $blog = get_blog_details($current_blog_id);
  $captain_id = get_post_meta($order_id,'captain_id',true);
  $locality_id = get_post_meta($order_id,'locality_id',true);
  $date_of_delivery = get_post_meta($order_id,'date_of_delivery',true);
  $locality = get_localitity_by_id('locality',$locality_id);
  $data['og_title'] = $locality->post_title;
  $data['og_description'] = $blog->blogname.', '.date('j M, D',strtotime($date_of_delivery)).', let\'s buy together on chotu';
  $banner_image ='';
  $longUrl='';
  $banner_pic_of_brand = unserialize(get_banner_pic_of_brand($current_blog_id,'banner_pic'));
  if(!empty($banner_pic_of_brand)){
    $banner_image = $banner_pic_of_brand['banner_pic'];
  }
  $data['og_image_attachment_id'] = $banner_image ? $banner_image : network_home_url(0).'/wp-content/uploads/2021/08/Webp.net-resizeimage.png';
  $longUrl = home_url($current_blog_id).'/?l='.$locality_id.'&c='.$captain_id.'&d='.date('Ymd',strtotime($date_of_delivery));
  if(is_user_logged_in()){
    $longUrl =$longUrl.'&mref='.do_shortcode('[mycred_affiliate_id]');
  }
  
  //.'
  // $url = shorten_URL($longUrl,$data);
  // $url = $longUrl;
  $url = urlencode($longUrl);
   $whatsapp_text = 'Friends, as '.$locality->post_title.', let\'s buy together from '.$blog->blogname.'. They will deliver on '.date('j M, D',strtotime($date_of_delivery)).'. I just bought now, you also can buy by clicking here : '.$url;
  
  
  echo '<h5 class="good_forme_text"><em>good for me, good for</em> <strong>'.$locality->post_title.'</strong></h5><p class="delivery_text_order_page"> chotu is delivering '.$blog->blogname.' on '.date('j M, D',strtotime($date_of_delivery)).'. Share it with your friends in '.$locality->post_title.' right now.</p>';
  // if(is_user_logged_in()){
  //   echo __('You might win some cashback too:)');
  // }else{
  //   echo __('Next time, login  to buy and share. you might win some cashback :)');
  // }
    echo '<a class="btn btn-success share_on_whatsapp" href="https://api.whatsapp.com/send/?phone&text='.$whatsapp_text.'" target="_blank">
    Share on  <span class="icon_whatsapp"><img class="filter_white" src="'.plugins_url().'/chotu_woo_network/assets/images/whatsapp-icon.svg"></span></a>';//<img src="https://chotu.app/lcp/wp-content/plugins/community_level_sale/public/templates/img/whatsapp.png">
}
   
add_action('woocommerce_after_order_details','chotu_woo_network_display_delivery_details_continue_shopping');
function chotu_woo_network_display_delivery_details_continue_shopping($order){
  
  global $woocommerce, $post;
  
  $order_id = $order->get_id();
  $captain_id = get_post_meta($order_id,'captain_id',true);
  $locality_id = get_post_meta($order_id,'locality_id',true);
  $locality = get_localitity_by_id('locality',$locality_id);
  $date_of_delivery = get_post_meta($order_id,'date_of_delivery',true);
  $user = new WP_User($captain_id);
  $user_role = unserialize(get_custom_user_meta($captain_id,'wp_capabilities'));
  $phone_number = get_custom_user_meta($captain_id,'phone_number');
  $captain_table_data = get_location_object_data_by_type($captain_id,'captain');
  echo '<h2 class="woocommerce-order-details__title">Delivery Details</h2><table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
    <tfoot>
        <tr>
            <th scope="row">Delivery date:</th>
            <td><span class="woocommerce-delivery-date date">'.date('j M, D',strtotime($date_of_delivery)).'</span></td>
          </tr>
          <tr>
            <th scope="row">Captain Name:</th>
            <td>'.$user->display_name .'</td>
          </tr>
          <tr>
            <th scope="row">Captain phone#:</th>
            <td><span class="woocommerce-captain-phone">'.$phone_number.'</span></td>
          </tr>
          <tr>
            <th scope="row">Captain pickup address:</th>
            <td><span class="captain-pickup-address">'. $captain_table_data->full_address.', '.$captain_table_data->pincode.'</span></td>
          </tr>
      </tfoot>
  </table>';
  if(array_key_exists('super-captain', $user_role)){
    echo '<header class="entry-header"><h1 class="entry-title"><a href="'.$locality->guid.'">Continue Shopping</a></h1></header>';
  }else{
      echo '<header class="entry-header"><h1 class="entry-title"><a href="'.network_home_url(0).'c/'.$user->user_nicename.'/?l='.$locality_id.'">Continue Shopping</a></h1></header>';
  }
  
  //echo do_shortcode('[card_by_captain locality_id="'.$locality_id.'" captain_id="'.$captain_id.'" ]');
}

/*retrieves the user_role details. Super-Captain is assumed to be only ONE*/  
function chotu_woo_network_get_users_details_by_user_role($user_role){
  global $wpdb;
  $wp_postmeta = "wp_postmeta";
  if($user_role == 'super-captain'){
      return $wpdb->get_row("SELECT wp_users.ID, wp_users.user_nicename FROM wp_users INNER JOIN wp_usermeta 
  ON wp_users.ID = wp_usermeta.user_id WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%$user_role%' ORDER BY wp_users.user_nicename"); 
  }
  return $wpdb->get_results("SELECT wp_users.ID, wp_users.user_nicename FROM wp_users INNER JOIN wp_usermeta 
ON wp_users.ID = wp_usermeta.user_id WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%$user_role%' ORDER BY wp_users.user_nicename"); 
}

// define the woocommerce_coupon_is_valid for the given captain ID.
add_filter( 'woocommerce_coupon_is_valid', 'chotu_woo_network_filter_woocommerce_coupon_is_valid', 10, 2 );
function chotu_woo_network_filter_woocommerce_coupon_is_valid( $valid, $instance ) { 
    // make filter magic happen here... 
    $coupon_post_obj = get_page_by_title($instance->code, OBJECT, 'shop_coupon');
    if(!empty($coupon_post_obj)){
        $captain_id = get_post_meta($coupon_post_obj->ID,'captain_id',true);
        if($_SESSION['c'] != $captain_id){
          if($captain_id == 0){
             $valid = 1;
            }else{
              $valid = 0;
            }
        }
      return $valid;
  }
}
         
/*convert index array into associative*/
function convert_index_array_to_associative($options){
  if(is_array($options)){
    $aa = array();
    foreach ($options as $key => $option) {
       $aa[$option] = $option;
    }
    return $aa;
  }
}

/*checks if the session has expired or not*/
add_action('init', 'chotu_woo_network_start_session', 1);
function chotu_woo_network_start_session() {
if(!session_id()) {
  session_start();
}
if(isset($_SESSION['expired'])){
  if(time() > $_SESSION['expired'])
    {  
      $_SESSION['l'] ='';
      $_SESSION['c'] ='';
      $_SESSION['d'] ='';
      $_SESSION['expired'] = '';
    }
}
}

/*VALIDATES if the given combination of L,C,D are valid or not. Is the date valid? Is the captain mapped to locality and the brand? If any one of these conditions is not met, this function returns false*/
function chotu_woo_network_check_url_param_validation($locality_id,$captain_id,$date_of_delivery){
  global $wpdb;
  $blog_id = get_current_blog_id();
  if(isset($locality_id) && isset($captain_id) && isset($date_of_delivery)){
    $delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,$locality_id,$captain_id);
    $super_captain = chotu_woo_network_get_users_details_by_user_role('super-captain');
    $captain_brand = chotu_woo_network_check_captain_brand_mapping($blog_id,$captain_id);
    $captain_locality = chotu_woo_network_check_captain_locality_mapping($captain_id,$locality_id);
    if(!empty($delivery_order_cycle)){
      if(date('Ymd',strtotime($delivery_order_cycle->delivery_date)) != $date_of_delivery){
        return false;
      }
    }
    if(!empty($super_captain)){
        if($super_captain->ID == $captain_id){
          if(chotu_woo_network_check_locality_exist($locality_id) < 1){
            return false;
        }else{
          return true;
        }
      }
    }

  if($captain_brand < 1  || $captain_locality < 1 ){
    return false;
  }
  }
  return true;
}

/*are the brand and captain mapped to each other or not?*/
function chotu_woo_network_check_captain_brand_mapping($blog_id,$captain_id){
  global $wpdb;
  $captain_brand = "chotu_captain_brand";
  return $wpdb->get_var("SELECT COUNT(*) FROM $captain_brand WHERE brand_id = '$blog_id' AND captain_ID='$captain_id'");
}

/*are the captain and locality mapped to each other or not?*/
function chotu_woo_network_check_captain_locality_mapping($captain_id,$locality_id){
  global $wpdb;
  $chotu_captain_locality = "chotu_captain_locality";
  return $wpdb->get_var("SELECT COUNT(*) FROM $chotu_captain_locality WHERE locality_id = '$locality_id' AND captain_ID='$captain_id'");
}

/*check whether the locality exists or not*/
function chotu_woo_network_check_locality_exist($locality_id){
  global $wpdb;
  $wp_posts = "wp_posts";
  return $wpdb->get_var("SELECT COUNT(*) FROM $wp_posts WHERE ID = '$locality_id' AND post_type='locality'");
}

/*The three icons in woocommerce are to be rearranged and reassigned to: search, home_URL & cart*/
add_filter('storefront_handheld_footer_bar_links','chotu_woo_network_change_my_account_menu_link_in_footer',10,1);
function chotu_woo_network_change_my_account_menu_link_in_footer($links){
  if(!empty($links)){
    foreach ($links as $key => $link) {
      if($key == 'my-account'){
        $links[$key]['priority']=20;
        $links[$key]['callback']='chotu_storefront_handheld_footer_bar_account_link';
      }
      if($key == 'search'){
        $links[$key]['priority']=10;
      }
    }
  }
  krsort($links);
return $links;
}
function chotu_storefront_handheld_footer_bar_account_link(){
  echo'<style>.storefront-handheld-footer-bar ul li.my-account>a::before {
      content: "" !important;
      background-image: url("'.get_site_url(0).'/wp-content/plugins/chotu_woo_network/assets/images/chotu-dark-new.png");
      background-position-x: center;
      background-repeat: repeat-y;
      background-size: contain;
      margin-bottom: 10px;
  }</style>';
  $href ='';
  if(isset($_SESSION['c'])){
    $user = chotu_woonetwork_get_user_by_id($_SESSION['c']);
    $user_role = unserialize(get_custom_user_meta($_SESSION['c'],'wp_capabilities'));
    if(isset($_SESSION['l']) && ($_SESSION['l'] !='')){
      $locality = get_localitity_by_id('locality',$_SESSION['l']);
      $locality_name = $locality->post_title ?  $locality->post_title: '';
      if(array_key_exists('captain', $user_role)){
        $href = network_home_url(0).'c/'.$user->user_nicename.'/?l='.$_SESSION['l'];
      }else{
        $href = network_home_url(0).'l/'.$locality->post_name;
      }
    }else{
      $href = network_home_url(0).'c/'.$user->user_nicename.'/';
    }
  }else{
    if(isset($_SESSION['l'])){
      $locality = get_localitity_by_id('locality',$_SESSION['l']);
      $href = network_home_url(0).'l/'.$locality->post_name;
    }else{
      $href = network_home_url(0);
    }
  }

  echo '<a href="'.$href.'">My Account </a>';
} 

/*customise the list of payment gateways on checkout*/
add_filter('woocommerce_available_payment_gateways','chotu_woo_network_filter_gateways',1);
function chotu_woo_network_filter_gateways($gateways){
    global $woocommerce;
    $blog_id = get_current_blog_id();
    if(isset($_SESSION['l']) && $_SESSION['l'] !='' && isset($_SESSION['c']) && $_SESSION['c'] !=''){
      //dd('sdsd');
      $l_mycred_balance = get_custom_post_meta($_SESSION['l'],'locality_payment_type_mycred_balance');
      $l_payment_type_cod = get_custom_post_meta($_SESSION['l'],'locality_payment_type_cod');
      $l_prepaid_razorpay = get_custom_post_meta($_SESSION['l'],'locality_payment_type_prepaid_razorpay');

      $c_mycred_balance = get_custom_user_meta($_SESSION['c'],'payment_type_mycred_balance');
      $c_payment_type_cod = get_custom_user_meta($_SESSION['c'],'payment_type_cod');
      $c_prepaid_razorpay = get_custom_user_meta($_SESSION['c'],'payment_type_prepaid_razorpay');
        if($l_mycred_balance == 0 || $c_mycred_balance == 0){
          unset($gateways['mycred']);
        }
        if($l_prepaid_razorpay == 0 || $c_prepaid_razorpay == 0){
          unset($gateways['razorpay']);
        }
        if($l_payment_type_cod == 0 || $c_payment_type_cod == 0){
          unset($gateways['cod']);
        }
    }
    //  dd($gateways);
    return $gateways;
}

/*Firebase Dynamic Link FDL*/
function shorten_URL($longUrl,$data) {
  // $longUrl = 'https://roofworth.com/shop1/shop/?coupon_code=2XRKZQIZPGSZ';
  $key = 'AIzaSyACbNOToIp6M2gsweR3smOryIy3gLkL9GY';
  $url = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=' . $key;
  $datas = array(
     "dynamicLinkInfo" => array(
        "domainUriPrefix" =>"https://testpin.chotu.shop",
        "link" => $longUrl,
        "socialMetaTagInfo" => array(
          "socialTitle" => $data['og_title'],
          "socialDescription" => $data['og_description'],
          "socialImageLink" => $data['og_image_attachment_id']
        )
     ),
        "suffix"=> array(
        "option"=> "SHORT"
      )   
  );
 
  $headers = array('Content-Type: application/json');

  $ch = curl_init ();
  curl_setopt ( $ch, CURLOPT_URL, $url );
  curl_setopt ( $ch, CURLOPT_POST, true );
  curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode($datas) );

  $data = curl_exec ( $ch );
  curl_close ( $ch );

  $short_url = json_decode($data);
  if(isset($short_url->error)){
      return $short_url->error->message;
  } else {
      return $short_url->shortLink;
  }

}


/*Check if the address_syntax L1 & L2 are set or not*/
add_action('woocommerce_checkout_process', 'chotu_woo_check_if_selected');

function chotu_woo_check_if_selected() {

  // you can add any custom validations here
  if ( empty( $_POST['level_1'] ) ){
    wc_add_notice( 'Please select required fields.', 'error' );
  }
  if ( empty( $_POST['level_2'] ) && isset($_POST['level_2'])){
    wc_add_notice( 'Please select required fields.', 'error' );
  }
    
}

/*change the storefront footer copyright text to BLANK*/
add_filter('storefront_credit_links_output','chotu_woo_change_footer_copyright_text',10,1);
function chotu_woo_change_footer_copyright_text($links_output){
  $links_output ='';
  return $links_output;
}

/*Add payment options heading in checkout page*/
add_action('woocommerce_review_order_before_payment','chotu_woo_woocommerce_review_order_before_payment');
function chotu_woo_woocommerce_review_order_before_payment(){
  echo '<h3>' .__('Payment Options', 'woocommerce').'</h3>';
}

/*Force the wooCommerce site to have shop as the home page*/
add_action( 'woocommerce_settings_saved', 'chotu_woo_change_default_home_page',10,1);
function chotu_woo_change_default_home_page($network_wide){
  $shop_page_id = woocommerce_get_page_id( 'shop' );
  update_option('page_on_front',$shop_page_id );
}

/*Remove sorting by category etc., on shop page*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/*retrieve the option of the main site*/
function get_option_main_site($option){
  global $wpdb;
  $wp_options = $wpdb->base_prefix.'options';
  return $wpdb->get_var("SELECT option_value FROM $wp_options WHERE option_name ='$option'");
}

/*Programatically populate the OG:meta tags for different pages*/

add_action('wp_head','chotu_woo_network_og_meta_details');
function chotu_woo_network_og_meta_details(){
  $blog_id = get_current_blog_id();
  $banner_image='';
  $title='';
  $banner_pic_of_brand = unserialize(get_banner_pic_of_brand($blog_id,'banner_pic'));
  if(!empty($banner_pic_of_brand)){
    $banner_image = $banner_pic_of_brand['banner_pic'];
  }
  if(is_product_category()){
     $current_category = get_queried_object();
     $thumbnail_id = get_woocommerce_term_meta( $current_category->term_id, 'thumbnail_id', true );
     $banner_image = wp_get_attachment_url( $thumbnail_id );
     $title = $current_category->name;
     //dd($banner_image);
  }
  if(is_product()){
    $thumbnail_id = get_post_meta(get_the_ID(), 'thumbnail_id', true );
    $banner_image = wp_get_attachment_url( $thumbnail_id );
    $title = get_the_title();
  }
 
  ?>
      <meta property="og:title" content="<?php echo bloginfo( 'name' ).' - '.$title;?>"/>
      <meta property="og:description" content="<?php echo bloginfo( 'description' );?>"/>
      <meta property="og:url" content="<?php echo home_url();?>"/>

      <meta property="og:image" content="<?php echo $banner_image;?>"/>
      <script id='idehweb-lwp-js-extra'>
var chotu_woo_network = <?php echo json_encode(array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));?>
</script>
<?php }
add_action( 'woocommerce_email_customer_details', 'chotu_woo_network_email_order_details', 10, 4 );
function chotu_woo_network_email_order_details( $order, $sent_to_admin, $plain_text, $email ) {
   if( $sent_to_admin ): // For admin emails notification
   $order_id  = $order->get_id();
    $captain_id = get_post_meta($order_id,'captain_id',true);
    $location_table_data = get_location_object_data_by_type(get_post_meta($order_id,'locality_id',true),'locality');
    $user = new WP_User($captain_id);
    $locality = get_localitity_by_id('locality',get_post_meta($order_id,'locality_id',true));
    $locality_name = $locality->post_title ?  $locality->post_title: '';
    $locality_address = $location_table_data->full_address ?  $location_table_data->full_address: '';
    $user_name = $user->display_name ? $user->display_name :'';
    $level_label_1 =  get_post_meta($order_id,'level_label_1',true);
    $level_label_2 =  get_post_meta($order_id,'level_label_2',true);
    $level_label_3 =  get_post_meta($order_id,'level_label_3',true);
    ?>
    <h2 style="color:#96588a;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
      Customer Details
    </h2>
  <div style="margin-bottom:40px">
   <table cellspacing="0" cellpadding="6" border="1" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;width:100%;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">  
      <tbody>
         <tr>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
              <strong>Address: </strong>
            </td>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
              <?php if($level_label_1 != 'N/A' && $level_label_1!=''){
                  echo '<strong>'.$level_label_1 .'</strong>: '. get_post_meta($order_id,'level_1',true).'<br />';
                 }
                 if($level_label_2 != 'N/A' && $level_label_2!=''){
                  echo '<strong>'.$level_label_2 .'</strong>: '. get_post_meta($order_id,'level_2',true).'<br />';
                 }
                 if($level_label_3 != 'N/A' && $level_label_3!=''){
                  echo '<strong>'.$level_label_3 .'</strong>: '. get_post_meta($order_id,'level_3',true);
                 }?>    
            </td>
         </tr>
          <tr>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
              <strong>Locality Name: </strong>
            </td>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
               <?php echo $locality_name;?>   
            </td>
         </tr>
           <tr>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
              <strong>Locality Address: </strong>
            </td>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
               <?php echo $locality_address;?>   
            </td>
         </tr>
          <tr>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
              <strong>Delivery Date: </strong>
            </td>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
               <?php echo date('j M, D',strtotime(get_post_meta($order_id,'date_of_delivery',true)));?>   
            </td>
         </tr>
          <tr>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
              <strong>Captain Name: </strong>
            </td>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
               <?php echo $user_name;?>   
            </td>
         </tr>
         <tr>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
              <strong>Captain Phone Number: </strong>
            </td>
            <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
               <?php echo get_custom_user_meta($captain_id,'phone_number');?>   
            </td>
         </tr>
        
      </tbody>
     
   </table>
</div>
    <?php
     endif;
}

add_action('woocommerce_checkout_process', 'chotu_woo_network_validate_billing_phone');
function chotu_woo_network_validate_billing_phone() {
    $is_correct = preg_match('/^[0-9]{10,10}$/', $_POST['billing_phone']);
    if ( $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'The Phone field should be <strong>10 digits Only</strong>.' ), 'error' );
    }
}

add_filter('wpseo_opengraph_image', 'category_image');
function category_image($image) {
   global $post;
   if( in_category( 'category1', $post->ID ) ) {
       $image = get_stylesheet_directory_uri().'/images/cat_1_image.jpg';
   } elseif(in_category( 'category2', $post->ID )) {
       $image = get_stylesheet_directory_uri().'/images/cat_2_image.jpg';
   }
   return $image;
}
function chotu_woonetwork_get_user_by_id($user_id){
   global $wpdb;
  $wp_users = "wp_users";
  return $wpdb->get_row("SELECT * FROM $wp_users WHERE ID = ".$user_id); 
}

add_action( 'woocommerce_product_meta_end', 'chotu_woo_network_after_add_to_cart_btn' );

function chotu_woo_network_after_add_to_cart_btn(){
  $made_by = get_post_meta( get_the_ID(),'made_by',true);
  $before = '';
  if(isset($made_by) && $made_by !=''){
    $before = '<p>Made By : <strong>'.$made_by.'</strong></p>';
  }
  echo $before;
}
add_filter( 'woocommerce_loop_add_to_cart_link', 'chotu_woo_network_before_after_btn', 10, 3 );

function chotu_woo_network_before_after_btn( $add_to_cart_html, $product, $args ){

  $made_by = get_post_meta( $product->id,'made_by',true);
  $before = '';
  if(isset($made_by) && $made_by !=''){
    $before = '<p>Made By : <strong>'.$made_by.'</strong></p>';
  }
   // Some text or HTML here
  $after = ''; // Add some text or HTML here as well

  return $before . $add_to_cart_html . $after;
}