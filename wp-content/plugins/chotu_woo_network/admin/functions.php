<?php
/*Disallow virtual and downloadable products for network sites with wooCommerce*/
add_filter( 'product_type_options', function( $options ) {
  if( isset( $options[ 'virtual' ] ) ) {
    unset( $options[ 'virtual' ] );
  }
   // remove "Downloadable" checkbox
  if( isset( $options[ 'downloadable' ] ) ) {
    unset( $options[ 'downloadable' ] );
  }
  return $options;
} );
/*Disallow external products for network sites with wooCommerce*/
add_filter( 'product_type_selector', 'chotu_work_remove_external_product' );

function chotu_work_remove_external_product ( $type ) {
  // Key should be exactly the same as in the class product_type
  unset($type['external']);
  return $type;
}


// Display admin product custom setting field(s)
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');
function woocommerce_product_custom_fields() {
    global $product_object;

    echo '<div class=" product_custom_field ">';

    // Custom Product Text Field
    woocommerce_wp_text_input( array( 
        'id'          => 'made_by',
        'label'       => __('Made By :', 'woocommerce'),
        'placeholder' => '',
        'desc_tip'    => 'true' // <== Not needed as you don't use a description
    ) );

    echo '</div>';
}

// Save admin product custom setting field(s) values
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');
function woocommerce_product_custom_fields_save( $product_id ) {
    if ( isset($_POST['made_by']) ) {
        update_post_meta( $product_id,'made_by', sanitize_text_field( $_POST['made_by'] ) );
    }
}
add_action( 'woocommerce_product_quick_edit_end', function(){

    /*
    Notes:
    Take a look at the name of the text field, '_custom_field_demo', that is the name of the custom field, basically its just a post meta
    The value of the text field is blank, it is intentional
    */

    ?>
    <div class="made_by_fields" bis_skin_checked="1">
		
			<br class="clear">
			<label>
				<span class="title">Made By</span>
				<span class="input-text-wrap">
					<input type="text" name="made_by" class="text wc_input_made_by made_by" placeholder="Made by" value="">
				</span>
			</label>
			<br class="clear">
		</div>
    <?php

} );
add_action('woocommerce_product_quick_edit_save', function($product){
    $post_id = $product->id;

    if ( isset( $_REQUEST['made_by'] ) ) {

        $made_by = trim(esc_attr( $_REQUEST['made_by'] ));

        // Do sanitation and Validation here

        update_post_meta( $post_id, 'made_by', wc_clean( $made_by ) );
    }

}, 10, 1);
add_action( 'manage_product_posts_custom_column', function($column,$post_id){

/*
Notes:
The 99 is just my OCD in action, I just want to make sure this callback gets executed after WooCommerce's
*/

switch ( $column ) {
    case 'name' :

        ?>
        <div class="hidden made_by_inline" id="made_by_inline_<?php echo $post_id; ?>">
            <div id="made_by"><?php echo get_post_meta($post_id,'made_by',true); ?></div>
        </div>
        <?php

        break;

    default :
        break;
}

}, 99, 2);

