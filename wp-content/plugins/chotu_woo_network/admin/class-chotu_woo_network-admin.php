<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/admin
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_woo_network_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;
	public $visited  = array();
	public $data  = array();
	public $parent_tree  = array();

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_woo_network_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_woo_network_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_woo_network-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_woo_network_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_woo_network_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_woo_network-admin.js', array( 'jquery' ), $this->version, false );

	}
	// Add a custom field to Admin coupon settings pages
	
	public function woo_network_add_coupon_captain_id_field() {
	    $captain_id = get_post_meta(get_the_ID(), 'captain_id', true);
	     woocommerce_wp_select( array(
	        'id'      => 'captain_id',
	        'label'   => __( 'Captain ', 'woocommerce' ),
	        'options' =>  $this->get_all_captains(), 
	        'value'   => $captain_id,
	    ) );
		}

	// Save the custom field value from Admin coupon settings pages
	
	public function woo_network_save_coupon_captain_id_field( $post_id, $coupon ) {
	    if( isset( $_POST['captain_id'] ) ) {
	        $coupon->update_meta_data( 'captain_id', sanitize_text_field( $_POST['captain_id'] ) );
	        $coupon->save();
	    }
	}
	public function get_all_captains(){
		$all_captain = chotu_woo_network_get_users_details_by_user_role('captain');
		$options = array('0'=>'');
		if(!empty($all_captain)){
			foreach ($all_captain as $key => $captain) {
				$phone_number = get_custom_user_meta($captain->ID,'phone_number');
				$options[$captain->ID] = $captain->user_nicename.' '.$phone_number;
			}
		}
		return $options;
		}


	/* Add banner pic to the brand website*/
	public function chotu_woo_network_customize_register( $wp_customize ) {
	 // Add Section
	$wp_customize->add_section('display_product_mobile_setting', array(
	      'title'             => __('Product Mobile Layout', 'chotu-theme'), 
	      'priority'          => 70,
	  )); 
	$wp_customize->add_setting( 'display_product_layout', array(
	        'default' =>1,
	        'capability' => 'edit_theme_options',
	        'callback' => 'chotu_woo_network_sanitize_select',
	        'transport'         => 'refresh',
	    ) );
	$wp_customize->add_control('display_product_layout',array(
	        'type'        => 'radio',
	        'priority'    => 10,
	        'section'     => 'display_product_mobile_setting',
	        'label'       => 'Display Product Mobile Layout',
	        'description' => 'Display Product column view in Mobile',
	        'choices' => array(
			    '1' => __( '1' ),
			    '2' => __( '2' ),
			  ),
	      )
	   );
	}
	public function chotu_woo_network_sanitize_select( $input, $setting ) {

	  // Ensure input is a slug.
	  // $input = sanitize_key( $input );

	  // Get list of choices from the control associated with the setting.
	  $choices = $setting->manager->get_control( $setting->id )->choices;

	  // If the input is a valid key, return it; otherwise, return the default.
	  return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}

	public function chotu_woo_network_display_product_mobile_setting(){
		if(get_theme_mod('display_product_layout') == 2){ 
		echo '<style>ul.products li.product {
				width: 46.411765%;
				float: left;
				margin-right: 5.8823529412%;
			}

			ul.products li.product:nth-of-type( 2n ) {
				margin-right: 0;
			}

			@media ( min-width: 768px ) {
				ul.products li.product:nth-of-type( 2n ) {
					margin-right: 5.8823529412%;
				}
			}</style>';
		}
	}
	
	
 
	public function chotu_woo_network_option_group() {
		echo '<div class="options_group">
		<p class=" form-field product_category_field"><label for="product_category">Product Categories</label>';
		echo '<select id="product_category_ajax" name="product_category" class="select short">';
	 	//$this->get_central_site_product_categories();
		echo '</select></div>';
	}

	
	public function chotu_woo_network_save_fields( $id, $post ){
	 
		//if( !empty( $_POST['super_product'] ) ) {
			update_post_meta( $id, 'product_category', $_POST['product_category'] );
		//} else {
		//	delete_post_meta( $id, 'super_product' );
		//}
	 
	}

	/**  start here not used below code */
	public function get_central_site_product_categories($search){
	  global $wpdb;
	  $wp_terms = "wp_terms";
	  $selected='';
	  //$data=array();
	  $i ='';
	  $search = htmlentities($search, ENT_HTML5  , 'UTF-8');
	  $wp_term_taxonomy = "wp_term_taxonomy";
	  $sql = "SELECT * from (SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND name like '%$search%') as temp";
	  $brand_category = $wpdb->get_results($sql);
	  if(!empty($brand_category)){
   //       $tree_string=array();
	        foreach ($brand_category as $key => $value) {
	        	$temp = [];
	        	if($value->parent == 0){
	        		$temp['id'] = $value->term_id;
	        		$temp['text'] = $value->name;
	        		array_push($this->data,$temp);
	        	}else{
	        		$this->getParents($value->name,$value->parent, 0,$value->term_id);
	        	}
	        	if(!in_array($value->term_id,$this->visited)){
		        	$this->getChildrens($this->data[0]['text'],$value->term_id, 0);
		        }
	
	        }
	    }
	    
	    return $this->data;
	    //echo $html;
	}

	public function getParents($name,$parent_id, $level,$term_id) {
		global $wpdb;
		$wp_terms = "wp_terms";
		$wp_term_taxonomy = "wp_term_taxonomy";
		;
		if($parent_id ==0) return $data;
		$brand_category = $wpdb->get_results("SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND wp_term_taxonomy.term_id ='$parent_id'");
		$level++;
		$temp = [];
		if(!empty($brand_category)){
			// $this->data[0]['text'] = $name;
   //          $this->data[0]['id'] = $parent_id;
			foreach ($brand_category as $key => $value) {
				$this->data[$key]['id'] = $term_id;
         		$this->data[$key]['text'] = $value->name.' > '.$name;
         		// array_push($this->data,$temp);
         		if($value->parent !=0){
         			$this->getParents($value->name.' > '.$name,$value->parent,$level,$term_id);
         		}
         		
            	
			}
			//dd($this->data);
			//return $this->data; 
		}                             
	}

	public function getChildrens($name,$parent_id, $level) {
		global $wpdb;
		$wp_terms = "wp_terms";
		$wp_term_taxonomy = "wp_term_taxonomy";
		$brand_category = $wpdb->get_results("SELECT wp_terms.term_id, name,wp_term_taxonomy.parent FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND  wp_term_taxonomy.parent = $parent_id");
		//dd($brand_category);
		// if($level == 1){
		// }
		$level++;
		$temp = [];
		if(!empty($brand_category)){
			// $data[0]['text'] = $name;
   //          $data[0]['id'] = $parent_id;
			foreach ($brand_category as $key => $value) {
				//$key++;
				array_push($this->visited,$value->term_id);
         		$temp['id'] = $value->term_id;
         		$temp['text'] = $name.' > '.$value->name;
         		array_push($this->data,$temp);
            	$this->getChildrens($name.' > '.$value->name,$value->term_id,$level);
			}
		}
		return true;
		
		//$this->data['visisted']=$visited;
		//return $temp;                            
	}

    public function get_global_product_category(){
    	if(isset($_GET['search'])){
    		$cat = $this->get_central_site_product_categories($_GET['search']);
    		$data = array (
	        'results' => $cat      
	      );
    		echo json_encode($data);
    	}
    	 
    
     wp_die();	
    }



    public function chotu_woo_network_asssign_category_to_product($post_id, $post, $update){
    	$product = wc_get_product( $post_id );
    	echo 'dsd'.$_POST['product_category'];
    	$i=0;
    	$this->getParentsCategory($post_id,$_POST['product_category'],$i);
    	if(!empty($this->parent_tree)){
    		$this->chotu_woo_network_insert_product_category($post_id);
    	}
    	
    	
    }


    public function getParentsCategory($post_id,$parent_id,$i) {
		global $wpdb;
		$wp_terms = "wp_terms";
		$wp_term_taxonomy = "wp_term_taxonomy";
		$brand_category = $wpdb->get_results("SELECT wp_terms.term_id, name,wp_term_taxonomy.parent,wp_term_taxonomy.description FROM $wp_terms LEFT JOIN $wp_term_taxonomy ON $wp_terms.term_id = $wp_term_taxonomy.term_id WHERE $wp_term_taxonomy.taxonomy = 'pr_cat' AND wp_term_taxonomy.term_id ='$parent_id'");
		$temp=[];
		if(!empty($brand_category)){
			foreach ($brand_category as $key => $value) {
				$temp = $value;
         		array_push($this->parent_tree,$temp);
         		if($value->parent !=0){
         			$this->getParentsCategory($post_id,$value->parent,$i);
         		}
            	$i++;
			}
		}
		return true;                             
	}

	public function chotu_woo_network_insert_product_category($post_id){
		$cats = array_reverse($this->parent_tree);
		foreach ($cats as $key => $cat) {
			if (!term_exists( $cat->name, 'product_cat' )) {
				$terms = wp_insert_term(
				  $cat->name, // the term 
				  'product_cat', // the taxonomy
				  array(
				    'parent'=> $cat->parent,
				    'description'=> $cat->description,
				  )
				);
			}
			if($key == end($cats)){
				wp_set_object_terms( $post_id, $terms->term_id, 'product_cat' );
			}
		}
		
	}

	/**  end here not used above code */


}