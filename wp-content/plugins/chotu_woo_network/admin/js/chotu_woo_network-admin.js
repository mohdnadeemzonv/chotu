(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 $(document).ready(function() {
	 $("#captain_id").select2( {
		placeholder: "Select Captain",
		allowClear: true
		} );

	 $('#product_category_ajax').select2({
	 		ajax: {
	 			url : ajaxurl,
	 			dataType: 'json',
	 			 data: function (params) {
			      var query = {
			        search: params.term,
			        // captain_ID: $('#user_id').val(),
			        action:"get_global_product_category"
			      }
			      return query;
			    },
	 		}

		});
	});
})( jQuery );
jQuery(function(){
jQuery('#the-list').on('click', '.editinline', function(){

    /**
     * Extract metadata and put it as the value for the custom field form
     */
    inlineEditPost.revert();

    var post_id = jQuery(this).closest('tr').attr('id');

    post_id = post_id.replace("post-", "");

    var $cfd_inline_data = jQuery('#made_by_inline_' + post_id),
        $wc_inline_data = jQuery('#woocommerce_inline_' + post_id );

    jQuery('input[name="made_by"]', '.inline-edit-row').val($cfd_inline_data.find("#made_by").text());
});
});
