<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.zonvoir.com
 * @since             1.0.0
 * @package           Chotu_woo_network
 *
 * @wordpress-plugin
 * Plugin Name:       Chotu Woo Network
 * Plugin URI:        chotu.app
 * Description:       This plugin customises the woocommerce store, a network site. this trains the woo shop to change the checkout address fields.

 * Version:           2.1.3
 * Author:            Mohd Nadeem
 * Author URI:        www.zonvoir.com
 * License:           1.0+
 * License URI:       www.zonvoir.com
 * Text Domain:       chotu_woo_network
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
/* change from 04 jan 2022*/
define( 'CHOTU_WOO_NETWORK_VERSION', '2.1.3' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-chotu_woo_network-activator.php
 */
function activate_chotu_woo_network() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chotu_woo_network-activator.php';
	Chotu_woo_network_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-chotu_woo_network-deactivator.php
 */
function deactivate_chotu_woo_network() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chotu_woo_network-deactivator.php';
	Chotu_woo_network_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_chotu_woo_network' );
register_deactivation_hook( __FILE__, 'deactivate_chotu_woo_network' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-chotu_woo_network.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_chotu_woo_network() {

	$plugin = new Chotu_woo_network();
	$plugin->run();

}
run_chotu_woo_network();
