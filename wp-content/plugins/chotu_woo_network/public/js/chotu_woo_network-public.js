(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	
	/*reusing the phone login AJAX URL here, as it is not otherwise used. do not confuse as why that ajaxurl is being called here.*/
	
	 $(document).on('click','#update_locality_id', function(e) {
	  e.preventDefault();
	  var locality_id = $('#locality_id').val();
	  if(locality_id == ''){
	  	return false;
	  }

	  $.ajax({
		     type : "POST",
		     url : chotu_woo_network.ajaxurl,
		     data : {'locality_id':locality_id,action: "set_session_of_lcd"},
		     success: function(response) {
		     	jQuery("#locality_select").modal("hide");
		     	location.reload();
		     	console.log(response);
		     }
		});
	  
	});

	/*change the COD label to captain name*/

	 $(document).ready(function() {

		setTimeout(function(){ $('.payment_method_cod label').text('Pay to: '+$('.captain_name').attr('title'));
		if(jQuery('#billing_phone').val() == ''){
			jQuery('#billing_phone').val(jQuery('#user_phone_number').val());
			jQuery('#billing_first_name').val(jQuery('#user_name').val());
		}
	}, 1000);
		jQuery('.storefront-sorting:first').css('display','none');

		if(jQuery('#billing_phone').val() == ''){
			jQuery('#billing_phone').val(jQuery('#user_phone_number').val());
			jQuery('#billing_first_name').val(jQuery('#user_name').val());
		}
	});

	 /*make the header sticky*/
	 $(window).scroll(function () {

		   var scroll = $(window).scrollTop();

		    if (scroll > 2) {

		       $(".site-header").addClass('stick_header_cl')

		    } else {

		      $(".site-header").removeClass('stick_header_cl')

		   }

		});

})( jQuery );
