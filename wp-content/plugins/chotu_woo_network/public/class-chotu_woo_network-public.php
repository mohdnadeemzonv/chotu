<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/public
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_woo_network_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_woo_network_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_woo_network_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_woo_network-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_woo_network_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_woo_network_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_woo_network-public.js', array( 'jquery' ), $this->version, false );

	}
	/*checks whether the URL parameters are coming in the URL or not. It also validates the parameter through: chotu_woo_network_check_url_param_validation*/
	public function chotu_woo_network_check_url_param(){
		global $wp_query;
		global $wpdb;
		$blog_id = get_current_blog_id();
		$delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,'','');
		$next_delivery_date = '';
	    if(!empty($delivery_order_cycle)){
	        $next_delivery_date = $delivery_order_cycle->delivery_date;
	    }
		if($blog_id !=1){
		  if(isset($_GET['l']) && isset($_GET['c']) && isset($_GET['d'])){
		  	 $delivery_date_exist = check_delivery_date_existing($blog_id,date('Y-m-d',strtotime($_GET['d'])));
		  	 if(chotu_woo_network_check_url_param_validation($_GET['l'],$_GET['c'],$_GET['d'])){
				session_unset();
				$inactive = 86400;
				$_SESSION['expired'] = time() + $inactive;
			  	$_SESSION['l'] = $_GET['l'];
				$_SESSION['c'] = $_GET['c'];
				$_SESSION['d'] = $_GET['d'];
				echo '<script>var newURL = location.href.split("?")[0];
				window.history.pushState("object", document.title, newURL)</script>';
			 }else if(date('Y-m-d',strtotime($delivery_date_exist)) != date('Y-m-d',strtotime($next_delivery_date))){
					session_unset();
					$inactive = 86400;
					$_SESSION['expired'] = time() + $inactive;
				  	$_SESSION['l'] = $_GET['l'];
					$_SESSION['c'] = $_GET['c'];
					$_SESSION['d'] = date('Ymd',strtotime($next_delivery_date));
					echo '<script>var newURL = location.href.split("?")[0];
					window.history.pushState("object", document.title, newURL)</script>';
					wc_add_notice( __( 'Your next delivery is '.date('j M, D',strtotime($next_delivery_date)), 'textdomain' ), 'notice' );
			}else{
			  	session_unset();
			    session_destroy(); 
			    wc_add_notice( __( 'The link you clicked is incorrect, please check.', 'textdomain' ), 'error' );
			  }
		  }else{
			  	session_unset();
			    session_destroy();
		}
		}
	}

	public function chotu_woo_network_set_session_of_lcd(){
		global $wpdb;
		$blog_id = get_current_blog_id();
		$locality_id = $_POST['locality_id'];
		$user_id = '';
		$delivery_date='';
		$super_captain = chotu_woo_network_get_users_details_by_user_role('super-captain');
		if(!empty($super_captain)){
			$delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,$locality_id,$super_captain->ID);
			if(!empty($delivery_order_cycle)){
				if(isset($_GET['l']) && isset($_GET['c']) && isset($_GET['d'])){
					$_SESSION['l'] ='';
			        $_SESSION['c'] ='';
			        $_SESSION['d'] ='';
			        $_SESSION['expired'] = '';
				  	
				}elseif(isset($_POST['locality_id'])){
					$_SESSION['l'] ='';
      				$_SESSION['c'] ='';
      				$_SESSION['d'] ='';
      				$_SESSION['expired'] = '';
					$inactive = 86400;
					$_SESSION['expired'] = time() + $inactive;
					$delivery_date = $delivery_order_cycle->delivery_date;
					$_SESSION['l'] = $locality_id;
				  	$_SESSION['c'] = $super_captain->ID;
				  	$_SESSION['d'] = $delivery_date;
				}
			}
		}
		wp_die();
	}

	/* send order to mern stack using api*/
public function chotu_woo_network_send_success_order_data_to_api($order_id){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$current_blog_details = get_blog_details($blog_id);
	$order = new WC_Order( $order_id );
	$user_id = $order->user_id;
	$total = $order->get_total();
	$order_status = $order->get_status();
	$phone = get_custom_user_meta($user_id,'phone_number',true);
	if(!isset($phone)){
		$phone = get_user_meta($user_id,'phone_number',true);
	}
	$captain_id = get_post_meta($order_id,'captain_id',true);
	$locality_id = get_post_meta($order_id,'locality_id',true);
	$horizontal_pic = get_custom_user_meta($captain_id,'captain_banner_pic');
	$captain_image_url = get_custom_attachment_image($horizontal_pic);
	$logo = get_custom_logo_url($blog_id);
	$locality_name = get_localitity_by_id('locality',$locality_id);
	$prefix = str_replace(' ', '', $locality_name->post_title);
	$suffix = str_replace('/', " ", $current_blog_details->path);
	$new_order_id = str_replace(' ', '', $blog_id.'_'.$order_id);
	$order_data=array();
	$order_data['order'] = $order->get_data();
	$order_data['order_unique_id'] = $new_order_id;
	$order_data['captain_image_url'] = $captain_image_url;
	$order_data['vendor_logo_url'] = $logo;
	$order_data['vendor_id'] = $blog_id;
	$order_data['order_id'] = $order_id;
	$order_data['order_status'] = $order_status;
	$order_data['vendor_name'] = $suffix;
	// $order_data['pin'] = get_pin_details_by_order_team_id($coupon_id,$coupon_code[0]);
	$i=0;
	foreach ($order->get_items() as $item_key => $item_values){
    	$order_data['order'][$i]['item_data'] = $item_values->get_data();
    	$i++;
	}
	$order_data['order']['postmeta'] = get_post_meta($order_id);
	// dd($order_data);
	$response = $this->post_data_curl(json_encode($order_data),$order_id);
	if($response == false){
		update_post_meta($order_id,'mongo_api_status','failed');
	}

}

/*API Credentials to post to MongoDB*/
public function post_data_curl($data,$order_id){
	$curl = curl_init();
	$auth = base64_encode(AMBASSDOR_API_USERNAME_PASSWORD);
	curl_setopt_array($curl, array(
		CURLOPT_URL => AMBASSDOR_API_URL,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS =>$data,
		CURLOPT_HTTPHEADER => array(
			"Authorization: Basic $auth",
			"Content-Type: application/json"
		),
	));
	$response = curl_exec($curl);
	$result = json_decode($response);
	if(isset($result->_id)){
		update_post_meta($order_id,'mongo_api_status','success');
		return true;
	}else{
		update_post_meta($order_id,'mongo_api_status','failed');
		return true;
	}
	curl_close($curl);
	return false;
	
}

public	function chotu_woo_network_update_customer_woocommerce_order($order_id) {
	global $wpdb;
	if (!$order_id) {
		return;
	}
	$order = wc_get_order($order_id);
	$user_id = $order->get_user_id() ? $order->get_user_id() : 0;
	$order_date = date('Y-m-d');
	$order_total = $order->get_total();
	$order_status = $order->get_status();
	$blog_id = get_current_blog_id();
	$current_blog_details = get_blog_details($blog_id);
	$captain_id = get_post_meta($order_id,'captain_id',true);
	$locality_id = get_post_meta($order_id,'locality_id',true);
	$mongo_api_status = get_post_meta($order_id,'mongo_api_status',true);
	$vendor_name = str_replace('/', " ", $current_blog_details->path);
	$table = 'chotu_orders';
	$data  = array('captain_ID'=>$captain_id,'customer_ID'=>$user_id,'brand_ID'=>$blog_id, 'order_id'=>$order_id,'locality_id'=>$locality_id, 'order_date'=>$order_date, 'order_status'=>$order_status,'mongo_status'=>$mongo_api_status, 'order_total'=>$order_total);

	$wpdb->insert( $table, $data);
	$this->chotu_woo_network_send_success_order_data_to_api($order_id);
}

/* update order status on chotu_orders table*/
public function chotu_woo_network_update_payment_complete($order_id){
	$order = wc_get_order( $order_id );
	global $wpdb;
	$table_name = 'chotu_orders';
	$order_status = $order->get_status();
	$wpdb->query( $wpdb->prepare("UPDATE $table_name 
		SET order_status = %s 
		WHERE order_id = %s",$order_status, $order_id)
);
	$this->chotu_woo_network_send_success_order_data_to_api($order_id);
}

/**** update status in chotu_orders when admin change the order status from admin dashboard******/
public function chotu_woo_network_update_order_status_to_customer_order($order_id){
	global $wpdb;
	$table_name = 'chotu_orders';
	if (!$order_id) {
		return;
	}
	$order = wc_get_order($order_id);
	$order_status = $order->get_status();
	$wpdb->query( $wpdb->prepare("UPDATE $table_name 
		SET order_status = %s 
		WHERE order_id = %s",$order_status, $order_id)
	);
	$this->chotu_woo_network_send_success_order_data_to_api($order_id);
}
public function chotu_woo_network_add_script_open_modal(){
	global $wpdb;
	$localities= '';
	$captain_localities=array();
	$geography_localities=array();
	$blog_id = get_current_blog_id();
	$brand_captains = get_captain_by_brand($blog_id);
    if(!empty($brand_captains)){
	    $brand_captains = implode(",", array_column($brand_captains, 'captain_ID'));
    	$captain_localities =  get_locality_by_captain($brand_captains);
    }
    $brand_geographies = get_geography_by_brand($blog_id);
	if(!empty($brand_geographies)){
		$geography_localities = get_locality_by_geography($brand_geographies);
	}
	$localities = array_merge($captain_localities,$geography_localities);
	$lat_long='';
	if($blog_id !=1){
		if(is_cart() || is_checkout()){
			if(!isset($_SESSION['l']) || !isset($_SESSION['c']) || !isset($_SESSION['d']) || $_SESSION['l'] ==0 || $_SESSION['c'] ==0 || $_SESSION['d'] ==0){
	?>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function(){
    	jQuery("#locality_select").modal("show");
    	$("#locality_id").select2( {
			placeholder: "Select your locality",
			allowClear: true
			} );
    	
        //jQuery(".locality-modal-toggle").trigger("click");
    });</script>
    <?php 
    $location_table_data = get_location_object_data_by_type($blog_id,'brand');
    //dd($location_table_data);
    if(!empty($location_table_data)){
      $lat_long = trim($location_table_data->lat_long);
    }
    $location_radius = get_option_main_site('options_location_radius');
    if($location_radius =='' && $location_radius ==0){
    	$location_radius = 30;
    }
    //$localities = get_object_by_location($lat_long,'locality',($location_radius*5));

    ?>
    <div id="locality_select" class="modal fade" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content locality_model">

                    <div class="modal-header ">
                        <h6 class="modal-title">
                        <div class="">Please select your locality</div>
                        </h6>
                       <button type="button" class="btn btn-default close_model" data-dismiss="modal">
                           <span class="close_icon" aria-hidden="true">×</span>
                       </button>
                      </div>

            <div class="modal-body">
            	<div class="form-group">
				 <p class="modal-title caption">chotu delivers to the following localities, please choose yours. If you don't find your locality, please <a href="<?php echo network_site_url(0).'add-our-locality'?>">request</a> to add your locality.</p>
				</div>
                    <div class="form-group">
                        <label> Locality Name</label>
				          <select name="locality_id" id="locality_id" class="form-control">
				          	<option value="0">Choose locality</option>
				            <?php
				            if(!empty($localities)){
				              foreach ($localities as $key => $locality) {
				                // $post = get_post($locality->reference_ID);
				                // dd($post)
				                $locality_details = get_localitity_by_id('locality',$locality->ID);
				                
				                if(!empty($locality_details) && $locality_details !=null){
				                	if($locality_details->post_status == 'publish'){
					                	echo '<option value="'.$locality->ID.'">'.$locality_details->post_title.'</option>';
					                }
				                }
				              }
				            }
				            ?>
				          </select>
                    </div>
                    <button type="button" class="btn btn-primary" id="update_locality_id">Update</button>
            </div>
        </div>
    </div>
</div>

	
<?php 
		}
	  }
	 }
	}
}